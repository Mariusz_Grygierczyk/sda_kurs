(function () {
    const playArea = $(".play-area");
    const numberSelector = $(".number-selector");
    const startButton = $(".start-button");

    // let clicked;
    // let previousSquare;
    let squares = [];

    const Square = function(pair){
        this.pair = pair;
        this.pairedSquare = null;
        this.div = $('<div></div>');
        this.div.addClass("element");
        //this.div.html(this.pair);
    };

    // const squareOnClick = function(){
    //     if (!clicked){
    //         clicked = true;
    //         previousSquare = this;
    //         this.div.css('background-image', `url("img/${this.pair}.jpg")`);
    //     }else {
    //         this.div.css('background-image', `url("img/${this.pair}.jpg")`);
    //         if ((this.pair === previousSquare.pair)&&(this !== previousSquare)) {
    //             this.div.css("visibility", "hidden");
    //             previousSquare.div.css("visibility", "hidden");
    //         }
    //         if(this !== previousSquare) {
    //             setTimeout(function(square1, square2){
    //                 square1.div.css('background-image', '');
    //                 square2.div.css('background-image', '');
    //             }, 1000, this, previousSquare);
    //             clicked = false;
    //             previousSquare = null;
    //         }
    //     }
    // };

    const clearAllCallbacks = function () {
        squares.forEach((square) =>{
        square.div.off();
      })
    };
    const resetCallbacks = function() {
        clearAllCallbacks();
        squares.forEach((square) => {
            square.div.click(squareOnFirstClick.bind(square));
        })
    };

    const squareOnFirstClick = function(){
        this.div.css('background-image', `url("img/${this.pair}.jpg")`);
        clearAllCallbacks();
        squares.filter((square) => {
            return square.pair !== this.pair;
        }).forEach((square) =>{
            square.div.click(squareSecondClickWrong.bind(square, this));
        });
        this.pairedSquare.div.click(squareSecondClickRight.bind(this.pairedSquare, this));
    };

    const squareSecondClickWrong = function(previousSquare){
        this.div.css('background-image', `url("img/${this.pair}.jpg")`);
        setTimeout(function(square1, square2){
            square1.div.css('background-image', '');
            square2.div.css('background-image', '');
        }, 1000, this, previousSquare);
        resetCallbacks();
    };

    const squareSecondClickRight = function(previousSquare){
        this.div.css("visibility", "hidden");
        previousSquare.div.css("visibility", "hidden");
        resetCallbacks();
    };



   const start = function(){
       squares = [];
       // clicked = false;
       // previousSquare = null;

       playArea.empty();

       let numberOfPairs = numberSelector.val();
        for(let i = 0; i<numberOfPairs; i++){
            let square1 = new Square(i);
            let square2 = new Square(i);
            square1.pairedSquare = square2;
            square2.pairedSquare = square1;
            squares.push(square1);
            squares.push(square2);
        }

       squares.sort(function() { return 0.5 - Math.random() });
       squares.forEach((square) => {
           playArea.append(square.div);
           square.div.click(squareOnFirstClick.bind(square));
       })

    };

    startButton.click(start);

})();