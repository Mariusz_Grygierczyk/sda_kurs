(function () {
    const playArea = $(".play-area");
    const numberSelector = $(".number-selector");
    const startButton = $(".start-button");

    let clicked;
    let previousSquare;

    const Square = function(pair){
        this.pair = pair;
        this.div = $('<div></div>');
        this.div.addClass("element");
        //this.div.html(this.pair);
    };

    const squareOnClick = function(){
        if (!clicked){
            clicked = true;
            previousSquare = this;
            this.div.css('background-image', `url("img/${this.pair}.jpg")`);
        }else {
            this.div.css('background-image', `url("img/${this.pair}.jpg")`);
            if ((this.pair === previousSquare.pair)&&(this !== previousSquare)) {
                this.div.css("visibility", "hidden");
                previousSquare.div.css("visibility", "hidden");
            }
            if(this !== previousSquare) {
                setTimeout(function(square1, square2){
                    square1.div.css('background-image', '');
                    square2.div.css('background-image', '');
                }, 1000, this, previousSquare);
                clicked = false;
                previousSquare = null;
            }
        }
    };

   const start = function(){
       let squares = [];
       clicked = false;
       previousSquare = null;

       playArea.empty();

       let numberOfPairs = numberSelector.val();
        for(let i = 0; i<numberOfPairs; i++){
            squares.push(new Square(i));
            squares.push(new Square(i));
        }

        while(squares.length>0){
            let square = squares[Math.floor(Math.random() * squares.length)];
            playArea.append(square.div);
            square.div.click(squareOnClick.bind(square));
            let index = squares.indexOf(square);
            if(index !== -1) squares.splice(index, 1);
        }
    };

    startButton.click(start);

})();