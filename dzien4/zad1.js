var userList = [];

var userNameInput = document.getElementsByClassName("user-name")[0];
var userSurnameInput = document.getElementsByClassName("user-surname")[0];
var userAgeInput = document.getElementsByClassName("user-age")[0];
var userAddButton = document.getElementsByClassName("user-add-button")[0];
var filterInput = document.getElementsByClassName("filter-input")[0];
var filterButton = document.getElementsByClassName("filter-button")[0];
var clearListButton = document.getElementsByClassName("clear-list-button")[0];
var showAllButton = document.getElementsByClassName("show-all-button")[0];
var usersContainer = document.getElementsByClassName("users-container")[0];
var filterSelection = document.getElementsByClassName("filter-selection")[0];

var User = function (name, surname, age){
	var that = this;
  this.name = name;
  this.surname = surname;
  this.age = age;
}

function addUser() {
  var name = userNameInput.value;
  var surname = userSurnameInput.value;
  var age = Number.parseInt(userAgeInput.value);
  var user = new User(name, surname, age);
  userList.push(user);
  userNameInput.value = '';
  userSurnameInput.value = '';
  userAgeInput.value = '';
  showAll();
}

function clearUsersList(){
	usersContainer.innerHTML = "Users:"
}

function showAll(){
	usersContainer.innerHTML = "Users:"
	userList.forEach((item) => {usersContainer.innerHTML += "<br>"+item.name + " " + item.surname + " " + item.age; });
}

function showFilteredList(){
	if(filterSelection.value == "more"){
  	var filteredUserList = userList.filter((item) => item.age > Number.parseInt(filterInput.value));
    }else{
    var filteredUserList = userList.filter((item) => item.age < Number.parseInt(filterInput.value));
    }
    
	usersContainer.innerHTML = "Users:"
	filteredUserList.forEach((item) => {usersContainer.innerHTML += "<br>"+item.name + " " + 		item.surname + " " + item.age; });  
}

userAddButton.addEventListener("click", addUser);
showAllButton.addEventListener("click", showAll);
clearListButton.addEventListener("click",clearUsersList);
filterButton.addEventListener("click", showFilteredList);