(function () {
    function CircularBuffer(initialCapacity) {
        this.storage = new Array(initialCapacity + 1);
        this.startIndex = 0;
        this.endIndex = 0;
        this.capacity = initialCapacity;


        this.push = function(element) {
            if (this.isFull()) {
                console.error("buffer is full");
            } else {
                this.storage[this.endIndex] = element;
                this.endIndex = (this.endIndex + 1) % (this.capacity + 1);
            }
        };

        this.pop = function() {
            if (this.isEmpty()) {
                console.error("buffer is empty");
            } else {
                result = this.storage[this.startIndex];
                this.storage[this.startIndex] = null;
                this.startIndex = (this.startIndex + 1) % (this.capacity + 1);
                return result;
            }
        };

        this.isFull = function() {
            return (this.startIndex === (this.endIndex + 1) % (this.capacity + 1));
        };

        this.isEmpty = function() {
            return (this.startIndex === this.endIndex);
        };


    }

    const buffer = new CircularBuffer(8);
    buffer.push(1);
    buffer.push(2);
    console.log(buffer.pop());
    buffer.push(3);
    buffer.push(4);
    console.log(buffer.pop());
    console.log(buffer.pop());
    console.log(buffer.pop());
    console.log("--------")
    for (i = 0; i < 8; i++) buffer.push(i);
    for (i = 0; i < 8; i++) console.log(buffer.pop());
    for (i = 0; i < 9; i++) buffer.push(i);

})();

