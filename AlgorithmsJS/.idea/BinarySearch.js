(function () {

    const itBinarySearch = function (arr, searched) {

        let begin = 0;
        let end = arr.length - 1;
        let mid;
        while(begin !== end){
            mid = Math.ceil((begin + end) / 2);
            if(arr[mid] === searched) return mid;
            if (arr[mid] > searched) end = mid;
            else begin = mid;
        }
        return false;
    }

    const recBinarySearch = function (arr, searched, begin = 0, end = arr.length - 1){

        if (begin === end) return false;
        let mid = Math.ceil((begin + end) / 2);
        if (arr[mid] === searched) return mid;
        else if (arr[mid] > searched) return recBinarySearch(arr, searched, begin, mid);
        else return recBinarySearch(arr, searched, mid, end);
    }



    const itBinarySearch2 = function (arr, searched, comprarator) {

        let begin = 0;
        let end = arr.length - 1;
        let mid = Math.ceil((begin + end) / 2);
        while(comprarator(arr[mid], searched) !== 0){
            if (comprarator(arr[mid], searched) > 0) end = mid;
            else begin = mid;
            mid = Math.ceil((begin + end) / 2);
            if(begin === end) return false;
        }
        return mid;
    }

    const compareInt = function (a, b) {
        return a - b;
    }

    const itBinarySearch3 = function (arr, searched) {

        let begin = 0;
        let end = arr.length - 1;
        let mid = Math.ceil((begin + end) / 2);
        while(arr[mid] !== searched){
            if (arr[mid] > searched) end = mid;
            else begin = mid;
            mid = Math.ceil((begin + end) / 2);
            if(begin === end) return false;
        }
        while (arr[mid - 1] === arr[mid]){
            mid--;
        }
        return mid;
    }

    array = [1,2,2,2,2,3,4,5,6,7];
    console.log(recBinarySearch(array,3));
    console.log(itBinarySearch(array,5));
    console.log(itBinarySearch2(array,2,compareInt));
    console.log(itBinarySearch3(array,2));

})();
