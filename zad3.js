var users = [];
var nameField = document.getElementById("user-name");
var midnameField = document.getElementById("user-midname");
var surnameField = document.getElementById("user-surname");


var User = function(name, midname, surname) {
  var that = this;
  this.name = name;
  this.midname = midname;
  this.surname = surname;
  this.id = name + "_" + surname + Math.floor(Math.random() * 10);
  this.showUser = function(placeholder) {
    placeholder
      .innerHTML = "<br>name: " + that.name + "<br>midname: " + that.midname + "<br>surname: " + that.surname + "<br>id: " + that.id;
  }
  this.addUser = function(parentComponent, usersLists) {
    if (nameField.value && surnameField.value) {
      var content = that.name + " " + that.midname + " " + that.surname;
      var userNode = document.createElement('div');
      userNode.innerHTML = content;
      userNode.id = that.id;
      parentComponent.appendChild(userNode);
      usersLists.push(that);
      nameField.value = '';
      midnameField.value = '';
      surnameField.value = '';
    } else {
      alert('add name and surname');
    }
  }
}


function addUser() {
  var name = nameField.value;
  var midname = midnameField.value;
  var surname = surnameField.value;
  var user = new User(name, midname, surname);
  var userContainer = document.getElementsByClassName("user-container")[0];
  user.addUser(userContainer, users);
}

function showUser(event) {
  var userNode = event.target;
  console.log(userNode.id);
  console.log(users);
  var user = users.find(function(user) {
    return user.id === userNode.id;
  });
  var placeholder = document
    .getElementsByClassName('user-placeholder')[0];
  user.showUser(placeholder);
  saveToLocalStorage(user);

}

function clearPlaceholder() {
  var placeholder = document.getElementsByClassName("user-placeholder")[0];
  placeholder.innerHTML = '';

}

document
  .getElementById("user-add-button")
  .addEventListener("click", addUser);
document
  .getElementById("clear-placeholder-button")
  .addEventListener("click", clearPlaceholder);
document
  .getElementsByClassName("user-container")[0]
  .addEventListener("click", showUser);


function saveToLocalStorage(user) {
  localStorage.setItem('id', user.id);
  localStorage.setItem('name', user.name);
  localStorage.setItem('midname', user.midname);
  localStorage.setItem('surname', user.surname);
}

function loadFromLocalStorage(placeholder) {
  placeholder
    .innerHTML = "<br>name: " +
    localStorage.getItem('name') +
    "<br>midname: " +
    localStorage.getItem('midname') +
    "<br>surname: " +
    localStorage.getItem('surname') +
    "<br>id: " +
    localStorage.getItem('id');
}

function initialize() {
  loadFromLocalStorage(document.getElementsByClassName('user-placeholder')[0]);
}

initialize();
