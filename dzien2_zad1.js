function Product(name, manufacturer){
  var productCount = 0;
  
  this.incrementCount = function(){
    productCount++;
  }
  
  this.initializeProduct = function(name, manufacturer){
    this.name = name;
    this.manufacturer = manufacturer;
    this.incrementCount();
  }
  
  this.display = function(){
    console.log(this.manufacturer + " " + this.name + " " +productCount)
  }
  
  this.checkAvailability = function(){
    return !!productCount;
    
  }
    
}

function ElectronicProduct(name, manufacturer){
  this.initializeProduct(name, manufacturer);
  
  this.displayCode = function(){
    console.log("elektronika # " + this.manufacturer + " # " + this.name);
  }
}
ElectronicProduct.prototype = new Product();

function FoodProduct(name, manufacturer){
  this.initializeProduct(name, manufacturer);
  
  this.displayCode = function(){
    console.log("zywnosc # " + this.manufacturer + " # " + this.name);
  }
}
FoodProduct.prototype = new Product();



var iphone3 = new ElectronicProduct ("iphone3" , "apple");
var iphone4 = new ElectronicProduct ("iphone4" , "apple");
iphone4.display();
iphone4.displayCode();
iphone3.display();
console.log(iphone3.checkAvailability());
