package zad1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

class PersonDatabase {
	private List<Person> people;

	public PersonDatabase(List<Person> people) {
		this.people = new ArrayList<Person>(people);
	}
	
	public void addPerson(Person person){
		people.add(person);
	}
	
	public void printReport(){
		GsonBuilder builder = new GsonBuilder();
		
		Gson gson =  builder.setPrettyPrinting().registerTypeAdapter(Person.class, new PersonSerializer()).registerTypeAdapter(Address.class, new AddressSerializer()).serializeNulls().create();
		
		for(Person p : people){
			System.out.println(gson.toJson(p));
		}
	}
	
	public static void main(String[] args){
		PersonDatabase people = new PersonDatabase(new ArrayList<Person>());
		
		Person person1 = new Person("Marian Kowalski", 50, Arrays.asList("Fanta", 
				"Cola"), new Address("Warszawa", "Mila", 23));
				Person person2 = new Person("Pawl Kukiz", 48, Arrays.asList("Pepsi", 
				"Sprite", "water"), new Address("Warszawa", "3 Maja", 23));
				Person person3 = new Person("Agata Buzek", 37, Arrays.asList("milk"), null);
		
		
		people.addPerson(person1);
		people.addPerson(person2);
		people.addPerson(person3);
		
		people.printReport();
	}
}
