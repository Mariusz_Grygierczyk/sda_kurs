package zad1;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

class AddressSerializer implements JsonSerializer<Address>{

	@Override
	public JsonElement serialize(Address arg0, Type arg1, JsonSerializationContext arg2) {
		JsonObject result = new JsonObject();
		result.add("Miasto", new JsonPrimitive(arg0.getCity()));
		result.add("Ulica", new JsonPrimitive(arg0.getStreet()));
		result.add("numer", new JsonPrimitive(arg0.getNumber()) );	
		return result;
	}
	
}
