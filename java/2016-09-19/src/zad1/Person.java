package zad1;

import java.util.ArrayList;
import java.util.List;

class Person {
	private String name;
	private int age;
	private List<String> orders;
	private Address adress;
	
	public Person(String name, int age, List<String> orders, Address adress) {
		this.name = name;
		this.age = age;
		this.adress = adress;
		orders = new ArrayList<String>(orders);
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	public List<String> getOrders() {
		return orders;
	}

	public Address getAdress() {
		return adress;
	}
	
	public void addOrder(String order){
		orders.add(order);
	}
}
