package zad1;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

class PersonSerializer implements JsonSerializer<Person> {

	@Override
	public JsonElement serialize(Person arg0, Type arg1, JsonSerializationContext arg2) {
		JsonObject result = new JsonObject();
		result.add("Imi�, Nazwisko", new JsonPrimitive(arg0.getName()));
		result.add("Wiek", new JsonPrimitive(arg0.getAge()));
		result.add("Adres", arg2.serialize(arg0.getAdress()) );		
		result.add("Zam�wienia", arg2.serialize(arg0.getOrders()));
		return result;
	}

}
