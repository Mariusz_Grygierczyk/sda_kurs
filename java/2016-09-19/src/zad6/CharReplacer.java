package zad6;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;

public class CharReplacer {

	public static void main(String[] args) {
		
		replaceWithStar("inputFile.txt", "output1.txt");
		toUpperCase("inputFile.txt", "output2.txt");
	}

	
	public static void replaceWithStar(String inputPath, String outputPath){
		char input;
		
		File inFile = new File(inputPath);
		File outFile = new File(outputPath);
		
		try {
			FileReader reader = new FileReader(inFile);
			FileWriter writer = new FileWriter(outFile);
			while(reader.ready()){
				input = (char) reader.read();
				if(input == ' ') input = '*';
				writer.write(input);
			}
			reader.close();
			writer.close();
			
		} catch ( IOException e) {
			System.out.println(e.getClass().getName());
			e.printStackTrace();
		}
		
	}
	
	public static void toUpperCase(String inputPath, String outputPath){
		char input;
		
		File inFile = new File(inputPath);
		File outFile = new File(outputPath);
		
		try {
			FileReader reader = new FileReader(inFile);
			FileWriter writer = new FileWriter(outFile);
			while(reader.ready()){
				input = (char) reader.read();
				if(Character.isLowerCase(input)) input = Character.toUpperCase(input);
				writer.write(input);
			}
			reader.close();
			writer.close();
			
		} catch ( IOException e) {
			System.out.println(e.getClass().getName());
			e.printStackTrace();
		}
	
	}
}
