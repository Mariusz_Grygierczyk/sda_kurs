package zad3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;




public class PersonDatabase {
	private List<Person> people;

	public PersonDatabase(List<Person> people) {
		this.people = new ArrayList<Person>(people);
	}
	
	public void addPerson(Person person){
		people.add(person);
	}
	
	public void printReport() throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		
		for(Person p : people){
			System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(p));			
		}
	}
	
	public void printReportToFile(String filePath) throws JsonProcessingException{
		ObjectMapper mapper = new ObjectMapper();
		FileOutputStream out;
		try {
			out = new FileOutputStream(new File(filePath));
			OutputStreamWriter writer = new OutputStreamWriter(out);
			
				 writer.write(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(people));
			writer.close();
		} catch (IOException e) {
			System.out.println(e.getClass().getName());
			e.printStackTrace();
		} 
		
		
		
	}
	
	public void addPersonFromFile(String filePath){
		ObjectMapper mapper = new ObjectMapper();		
		
			try {
				Person newPerson = mapper.readValue(new File(filePath), Person.class);
				people.add(newPerson);
			} catch (Exception e) {
				System.out.println(e.getClass().getName());
				e.printStackTrace();
			} 
		
			
		
		
	}
	
	public static void main(String[] args) throws JsonProcessingException{
		PersonDatabase people = new PersonDatabase(new ArrayList<Person>());
		
		
		
		Person person1 = new Person("Marian Kowalski", 50, Arrays.asList("Fanta", 
				"Cola"), new Address("Warszawa", "Mila", 23));
				Person person2 = new Person("Pawl Kukiz", 48, Arrays.asList("Pepsi", 
				"Sprite", "water"), new Address("Warszawa", "3 Maja", 23));
				Person person3 = new Person("Agata Buzek", 37, Arrays.asList("milk"), null);
		
		
//		people.addPerson(person1);
//		people.addPerson(person2);
//		people.addPerson(person3);
		
		people.addPersonFromFile("data\\person1.json");	
		people.addPersonFromFile("data\\person2.json");	
		people.addPersonFromFile("data\\person3.json");	
				
		people.printReport();
		people.printReportToFile("data\\report.json");
	}
}
