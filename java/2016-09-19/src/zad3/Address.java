package zad3;

public class Address {
	private String city;
	private String street;
	private int number;
	
	public Address(String city, String street, int number) {
		super();
		this.city = city;
		this.street = street;
		this.number = number;
	}
	
	public Address(){
		
	}
	
	public String getCity() {
		return city;
	}
	public String getStreet() {
		return street;
	}
	public int getNumber() {
		return number;
	}
}
