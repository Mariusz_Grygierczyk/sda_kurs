create table szczegoly_osob (
	osoba_id integer not null,
	data_urodzenia date not null,
	ostatnie_logowanie timestamp not null default current_timestamp,
	aktywny boolean not null default true,
	constraint pk_szczegoly_osob_osoba_id primary key (osoba_id),
	constraint fk_szczegoly_osob_osoba_id foreign key (osoba_id) references osoby(id) 
	  on delete cascade
);

insert into szczegoly_osob(osoba_id, data_urodzenia, ostatnie_logowanie, aktywny) 
     values (1, '1988-01-05', '2014-02-05 16:57:02', true);

insert into szczegoly_osob(osoba_id, data_urodzenia, ostatnie_logowanie, aktywny) 
     values (2, '1975-06-01', '2015-03-25 19:32:51', false);
