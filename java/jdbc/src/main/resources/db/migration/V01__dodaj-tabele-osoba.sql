create table osoby (
    id integer not null,
    imie varchar(20) not null,
    nazwisko varchar(20) not null,
    constraint pk_osoby_id primary key (id)
);
