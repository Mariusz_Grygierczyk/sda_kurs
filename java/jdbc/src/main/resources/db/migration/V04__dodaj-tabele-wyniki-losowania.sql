create table wyniki_losowania (
	id integer not null,
	nazwa varchar(20) not null,
	liczba1 smallint not null,
	liczba2 smallint not null,
	liczba3 smallint not null,
	liczba4 smallint not null,
	liczba5 smallint not null,
	liczba6 smallint not null,
	data timestamp not null default current_timestamp
);

insert into wyniki_losowania(id, nazwa, liczba1, liczba2, liczba3, liczba4, liczba5, liczba6)
     values (0, 'Losowanie 0', 13, 5, 6, 41, 28, 33);
     
create table statystyki_losowan (
	liczba_losowan integer not null default 0
);

insert into statystyki_losowan(liczba_losowan)
     values (1);