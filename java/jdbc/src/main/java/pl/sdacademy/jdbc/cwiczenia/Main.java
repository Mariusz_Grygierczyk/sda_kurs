package pl.sdacademy.jdbc.cwiczenia;

import java.sql.DriverPropertyInfo;
import java.sql.SQLException;

import pl.sdacademy.jdbc.db.ConnectionProvider;
import pl.sdacademy.jdbc.db.DataSourceConnectionProvider;
import pl.sdacademy.jdbc.db.DriverManagerConnectionProvider;
import pl.sdacademy.jdbc.lotto.MaszynaLotto;


@SuppressWarnings("unused")
public class Main {
    
    private final ConnectionProvider connectionProvider;
    private Cw01ProsteZapytanie cw01;
    private Cw02MapowanieTypow cw02;
    private Cw03KontrolowanieLiczbyWynikow cw03;
    private Cw04InsertUpdate cw04;
    private Cw05Transakcje cw05;

    public Main(ConnectionProvider connectionProvider) {
        super();
        this.connectionProvider = connectionProvider;
        this.cw01 = new Cw01ProsteZapytanie(connectionProvider);
        this.cw02 = new Cw02MapowanieTypow(connectionProvider);
        this.cw03 = new Cw03KontrolowanieLiczbyWynikow(connectionProvider);
        this.cw04 = new Cw04InsertUpdate(connectionProvider);
        this.cw05 = new Cw05Transakcje(connectionProvider);
    }

    public static void main(String... args) throws SQLException {
        Main main = new Main(DataSourceConnectionProvider.INSTANCE);
//        main.cw01.pokazInformacjeOBazieDanych();
//        main.cw01.pokazListeOsob();
//        main.cw01.pokazListeOsob("Piotr");
//        main.cw01.pokazListeOsob("Marian' OR '' = '");
//        main.cw01.pobierzAktualnyCzasZBazyDanych();
//        main.cw01.wykonajFunkcjeUpper("to powinno byc duzymi literami");
//        main.cw02.pokazSzczegoloweDaneOsoby(1);
//        main.cw02.pokazSzczegoloweDaneOsoby(2);
//        main.cw02.pokazLiczbeOsob();
//        main.cw03.pokazListeOsob(3);
//        main.cw04.zapiszWynikiLosowania(MaszynaLotto.wylosuj());
//        main.cw04.zwiekszLiczbeLosowan(1);
//        main.cw04.zapiszWynikiLosowan(MaszynaLotto.wylosuj(10));
        main.cw05.zapiszWynikiLosowaniaIZaktualizujStatystyki(MaszynaLotto.wylosuj());
    }
}
