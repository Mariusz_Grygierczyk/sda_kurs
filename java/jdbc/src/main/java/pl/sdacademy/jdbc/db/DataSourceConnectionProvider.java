package pl.sdacademy.jdbc.db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import net.ttddyy.dsproxy.listener.SLF4JLogLevel;
import net.ttddyy.dsproxy.support.ProxyDataSourceBuilder;
import org.postgresql.ds.PGSimpleDataSource;

public enum DataSourceConnectionProvider implements ConnectionProvider {
    INSTANCE;
    
    private DataSource dataSource;
    
    private DataSourceConnectionProvider() {
        // TODO: 0b. Laczenie za pomoca DataSource - utworzyc MysqlDataSource i ustawic odpowiednie parametry polaczenia (url, user, password)
        // TODO: 0c. Laczenie za pomoca DataSource (opcjonalnie) - opakowac otrzymay obiekt DataSource za pomoca ProxyDataSource 
        //           aby podejrzec wysylane do bazy danych polecenia SQL (wraz z parametrami)

        PGSimpleDataSource ds = new PGSimpleDataSource();
        ds.setUrl("jdbc:postgresql://localhost:5432/jdbc");
        ds.setUser("postgres");
        ds.setPassword("admin");


        this.dataSource = ProxyDataSourceBuilder.create(ds)
                                                .logQueryBySlf4j(SLF4JLogLevel.INFO)
                                                .countQuery()
                                                .asJson()
                                                .build();
    }
    
    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}
