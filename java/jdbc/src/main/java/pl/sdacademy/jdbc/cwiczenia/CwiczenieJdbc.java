package pl.sdacademy.jdbc.cwiczenia;

import java.sql.Connection;
import java.sql.SQLException;

import lombok.RequiredArgsConstructor;
import pl.sdacademy.jdbc.db.ConnectionProvider;

@RequiredArgsConstructor
public class CwiczenieJdbc {

    protected final ConnectionProvider connectionProvider;

    public void doWithConnection(ConnectionConsumer callback) {
        // TODO: 1c. Zaimplementowac metode do poprawnego pobierania i zamykania polaczenia z baza danych
        // TODO: 5c. Dodac obslugiwanie transakcji uzywajac Connection::setAutoCommit, Connection::commit i Connection::rollback


        try (Connection connection = connectionProvider.getConnection()){
            connection.setAutoCommit(false);
            try {
                callback.accept(connection);
            } catch (SQLException e) {
                connection.rollback();
                e.printStackTrace();
            }

            connection.commit();
        } catch (SQLException e) {

            e.printStackTrace();
        }
    }
    
    @FunctionalInterface
    public interface ConnectionConsumer {
        void accept(Connection connection) throws SQLException;
    }

}