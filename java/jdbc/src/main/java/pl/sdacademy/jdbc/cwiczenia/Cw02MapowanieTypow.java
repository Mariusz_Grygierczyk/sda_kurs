package pl.sdacademy.jdbc.cwiczenia;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pl.sdacademy.jdbc.db.ConnectionProvider;

public class Cw02MapowanieTypow extends CwiczenieJdbc {

    public Cw02MapowanieTypow(ConnectionProvider connectionProvider) {
        super(connectionProvider);
    }

    public void pokazSzczegoloweDaneOsoby(Integer id) throws SQLException {
        // TODO: 2a. Pobrac informacje szczegolowe osoby (w tabeli `szczegoly_osob`), zmapowac i wyswietlic szczegoly wykorzystujac mapowania ResultSet::set...

        doWithConnection((connection -> {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM osoby, szczegoly_osob WHERE id = osoba_id AND osoba_id = ?");
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                System.out.println(resultSet.getString("imie") + " " + resultSet.getString("nazwisko") + " " + resultSet.getInt("osoba_id") + " " + resultSet.getDate("data_urodzenia") + " " + resultSet.getTime("ostatnie_logowanie")  + " " + resultSet.getBoolean("aktywny"));
            }
            statement.close();
            resultSet.close();
        }));
    }

    public void pokazLiczbeOsob() throws SQLException {
        // TODO: 2b. Pobrac liczbe osob w tabeli `osoby`

        doWithConnection(connection -> {
            PreparedStatement statement = connection.prepareStatement("SELECT COUNT(*) FROM osoby");
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            System.out.println(resultSet.getInt(1));
            statement.close();
            resultSet.close();
        });
    }

}   
