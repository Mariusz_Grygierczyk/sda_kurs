package pl.sdacademy.jdbc.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public enum DriverManagerConnectionProvider implements ConnectionProvider {
    INSTANCE;
    
    public Connection getConnection() throws SQLException {
        // TODO: 0a. Laczenie za pomoca DriverManager::getConnection
       return DriverManager.getConnection("jdbc:postgresql://localhost:5432/jdbc", "postgres", "admin");

    }
}
