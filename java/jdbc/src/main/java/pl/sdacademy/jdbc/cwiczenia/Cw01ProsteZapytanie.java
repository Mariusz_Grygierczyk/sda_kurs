package pl.sdacademy.jdbc.cwiczenia;

import java.sql.*;

import pl.sdacademy.jdbc.db.ConnectionProvider;

public class Cw01ProsteZapytanie extends CwiczenieJdbc {

    public Cw01ProsteZapytanie(ConnectionProvider connectionProvider) {
        super(connectionProvider);
    }

    public void pokazInformacjeOBazieDanych() {
        // TODO: 0d. Wyswietl informacje o bazie danych, sterowniku oraz URL
        try (Connection connection = connectionProvider.getConnection()) {
            System.out.println(connection.getMetaData().getDatabaseProductName());
            System.out.println(connection.getMetaData().getDriverName());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void pokazListeOsob() {
        // TODO: 1a. Zaimplementować zapytanie wyświetlające listę wszystkich osób


        doWithConnection(conn -> {
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from osoby");
            while (resultSet.next()) {
                System.out.println(resultSet.getInt("id") + " " + resultSet.getString("imie") + " " + resultSet.getString("nazwisko"));
            }
        });

    }

    public void pokazListeOsob(String imie) throws SQLException {
        // TODO: 1b. Zaimplementowac zapytanie wyswietlajace dane osob (tabela: osoby) o zadanym imieniu 
        // TODO: 1c. Poprawić implementacje, aby uniknac niebezpieczenstw zwiazanych z SQL Injection 

        doWithConnection(conn -> {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM osoby WHERE imie = ?");
            statement.setString(1, imie);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                System.out.println(resultSet.getInt("id") + " " + resultSet.getString("imie") + " " + resultSet.getString("nazwisko"));
            }
        });

    }


    public void pobierzAktualnyCzasZBazyDanych() throws SQLException {
        // TODO: 1d. Napisać funkcję pobierającą bieżący datę na serwerze bazy danych 
        // Przydatne: CallableStatmement, now()

        doWithConnection(conn -> {
            CallableStatement statement = conn.prepareCall("{ ? = call now()}");
            statement.registerOutParameter(1, Types.TIMESTAMP);
            statement.execute();
            System.out.println(statement.getTimestamp(1));
            statement.close();
        });
    }

    public void wykonajFunkcjeUpper(String wartosc) throws SQLException {
        // TODO: 1e. Napisać funkcję wykonującą funkcję UPPER na przekazanym ciągu znaków
        // Przydatne: UPPER(str) 

        doWithConnection(conn -> {
            CallableStatement statement = conn.prepareCall("SELECT UPPER(?)");
            statement.setString(1, wartosc);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            System.out.println(resultSet.getString(1));
            statement.close();
            resultSet.close();

        });
    }
}   
