package pl.sdacademy.jdbc.cwiczenia;

import java.security.UnresolvedPermission;
import java.sql.*;
import java.util.List;

import pl.sdacademy.jdbc.db.ConnectionProvider;
import pl.sdacademy.jdbc.lotto.WynikiLosowania;

public class Cw04InsertUpdate extends CwiczenieJdbc {

    public Cw04InsertUpdate(ConnectionProvider connectionProvider) {
        super(connectionProvider);
    }

    public void zapiszWynikiLosowania(WynikiLosowania wynikiLosowania) throws SQLException {
        // TODO: 4a. Zapisac wyniki losowania do tabeli `wyniki_losowania` za pomoca PreparedStatement::executeUpdate

        doWithConnection(connection -> {
            PreparedStatement statement = connection.prepareStatement("insert into wyniki_losowania values ((SELECT nextval('numer_losowania_seq')), ?, ?, ?, ?, ?, ?, ?, ?);");

            statement.setString(1, wynikiLosowania.getNazwa());
            statement.setInt(2, wynikiLosowania.getLiczby().remove(0));
            statement.setInt(3, wynikiLosowania.getLiczby().remove(0));
            statement.setInt(4, wynikiLosowania.getLiczby().remove(0));
            statement.setInt(5, wynikiLosowania.getLiczby().remove(0));
            statement.setInt(6, wynikiLosowania.getLiczby().remove(0));
            statement.setInt(7, wynikiLosowania.getLiczby().remove(0));
            statement.setTimestamp(8, Timestamp.from(wynikiLosowania.getData()));
            statement.executeUpdate();
            statement.close();
        });
    }

    public void zapiszWynikiLosowan(List<WynikiLosowania> wynikiLosowan) throws SQLException {
        // TODO: 4b. Zapisac wyniki wszystkich losowan z przekazanego parametru do tabeli `wyniki_losowania` za pomoca PreparedStatement::addBatch/executeBatch

        doWithConnection(connection -> {
            PreparedStatement statement = connection.prepareStatement("insert into wyniki_losowania values ((SELECT nextval('numer_losowania_seq')), ?, ?, ?, ?, ?, ?, ?, ?);");

            for (WynikiLosowania wynik : wynikiLosowan) {
                statement.setString(1, wynik.getNazwa());
                statement.setInt(2, wynik.getLiczby().remove(0));
                statement.setInt(3, wynik.getLiczby().remove(0));
                statement.setInt(4, wynik.getLiczby().remove(0));
                statement.setInt(5, wynik.getLiczby().remove(0));
                statement.setInt(6, wynik.getLiczby().remove(0));
                statement.setInt(7, wynik.getLiczby().remove(0));
                statement.setTimestamp(8, Timestamp.from(wynik.getData()));
                statement.addBatch();
            }
            statement.executeBatch();
            statement.close();
        });

    }

    public void zwiekszLiczbeLosowan(Integer liczba) throws SQLException {
        // TODO: 4c. Zaktualizowac tabele `statystyki_losowan` - zwiekszyc o n liczbe losowan

        doWithConnection(connection -> {
            PreparedStatement getCurrentCountStatement = connection.prepareStatement("SELECT * FROM statystyki_losowan");
            ResultSet currentCountResultSet = getCurrentCountStatement.executeQuery();
            currentCountResultSet.next();
            int currentCount = currentCountResultSet.getInt(1);
            PreparedStatement setNewCountStatement = connection.prepareStatement("UPDATE statystyki_losowan SET liczba_losowan = ?");
            setNewCountStatement.setInt(1, currentCount + liczba);
            setNewCountStatement.executeUpdate();
            getCurrentCountStatement.close();
            setNewCountStatement.close();
            currentCountResultSet.close();;
        });
    }



}   
