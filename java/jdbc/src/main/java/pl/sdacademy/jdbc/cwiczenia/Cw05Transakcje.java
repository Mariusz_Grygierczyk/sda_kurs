package pl.sdacademy.jdbc.cwiczenia;

import java.sql.*;

import pl.sdacademy.jdbc.db.ConnectionProvider;
import pl.sdacademy.jdbc.lotto.WynikiLosowania;

public class Cw05Transakcje extends CwiczenieJdbc {
    
    public Cw05Transakcje(ConnectionProvider connectionProvider) {
        super(connectionProvider);
    }

    public void zapiszWynikiLosowaniaIZaktualizujStatystyki(WynikiLosowania wynikiLosowania) throws SQLException {
        // TODO: 5a. Polaczyc dodawanie wynikow losowania i aktualizacje statystyk w jedna operacje
        //           - w pierwszym kroku podac nieprawidlowa wartosc przy aktualizacji statystyk (np. -2) aby spowodowac blad przy UPDATE
        //             i sprawdzic co sie stalo z INSERT'em do tabeli `wyniki_losowania`
        // TODO: 5b. Jawnie kontrolowac transakcje za pomoca metod Connection::setAutoCommit oraz Connection::commit
        //           - sprawdzic co sie dzieje w przypadku gdy nie wywolamy Connection::commit

        doWithConnection(connection -> {
            incrementCount(connection, 1);
            addNewResult(connection, wynikiLosowania);
        });
    }

    private void addNewResult(Connection connection, WynikiLosowania wynikiLosowania) throws SQLException {
        PreparedStatement insertNewResultStatement = connection.prepareStatement("insert into wyniki_losowania values ((SELECT nextval('numer_losowania_seq')), ?, ?, ?, ?, ?, ?, ?, ?);");

        insertNewResultStatement.setString(1, wynikiLosowania.getNazwa());
        insertNewResultStatement.setInt(2, wynikiLosowania.getLiczby().remove(0));
        insertNewResultStatement.setInt(3, wynikiLosowania.getLiczby().remove(0));
        insertNewResultStatement.setInt(4, wynikiLosowania.getLiczby().remove(0));
        insertNewResultStatement.setInt(5, wynikiLosowania.getLiczby().remove(0));
        insertNewResultStatement.setInt(6, wynikiLosowania.getLiczby().remove(0));
        insertNewResultStatement.setInt(7, wynikiLosowania.getLiczby().remove(0));
        insertNewResultStatement.setTimestamp(8, Timestamp.from(wynikiLosowania.getData()));
        insertNewResultStatement.executeUpdate();
        insertNewResultStatement.close();
    }

//    private int getNextDrawNumber(Connection connection) throws SQLException{
//        Statement getNextDrawStatement = connection.createStatement();
//        ResultSet getNextDrawNumberResultSet =
//    }

    private void incrementCount(Connection connection, int count) throws SQLException {
        PreparedStatement getCurrentCountStatement = connection.prepareStatement("SELECT * FROM statystyki_losowan");
        ResultSet currentCountResultSet = getCurrentCountStatement.executeQuery();
        currentCountResultSet.next();
        int currentCount = currentCountResultSet.getInt(1);
        PreparedStatement setNewCountStatement = connection.prepareStatement("UPDATE statystyki_losowan SET liczba_losowan = ?");
        setNewCountStatement.setInt(1, currentCount + count);
        setNewCountStatement.executeUpdate();
        getCurrentCountStatement.close();
        setNewCountStatement.close();
        currentCountResultSet.close();
    }

}   
