package pl.sdacademy.jdbc.lotto;

import java.time.Instant;
import java.util.List;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class WynikiLosowania {
    
    private final Integer numer;
    private final String nazwa;
    private final List<Integer> liczby;
    private final Instant data; 

}
