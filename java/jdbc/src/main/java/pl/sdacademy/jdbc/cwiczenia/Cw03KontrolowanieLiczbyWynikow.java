package pl.sdacademy.jdbc.cwiczenia;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pl.sdacademy.jdbc.db.ConnectionProvider;

public class Cw03KontrolowanieLiczbyWynikow extends CwiczenieJdbc {
    
    public Cw03KontrolowanieLiczbyWynikow(ConnectionProvider connectionProvider) {
        super(connectionProvider);
    }

    public void pokazListeOsob(Integer max) throws SQLException {
        // TODO: 3a. Stworzyc zapytanie ktore pobierz liste osob ograniczona maksymalnie do `max` wynikow uzywajac PreparedStatement::setMaxRows
        // TODO: 3b. Poprawic zapytanie tak, aby ograniczenie liczby rekordow dzialalo po stronie bazy danych

//        doWithConnection(connection -> {
//            PreparedStatement statement = connection.prepareStatement("SELECT * FROM osoby");
//            statement.setMaxRows(max);
//            ResultSet resultSet = statement.executeQuery();
//            while (resultSet.next()){
//                System.out.println(resultSet.getInt("id") + " " + resultSet.getString("imie") + " " + resultSet.getString("nazwisko"));
//            }
//            statement.close();
//            resultSet.close();
//        });

        doWithConnection(connection -> {
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM osoby LIMIT ?");
            statement.setInt(1, max);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()){
                System.out.println(resultSet.getInt("id") + " " + resultSet.getString("imie") + " " + resultSet.getString("nazwisko"));
            }
            statement.close();
            resultSet.close();
        });
    }

}   
