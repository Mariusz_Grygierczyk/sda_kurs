package com.sda.decorator;


public interface Pizza {

    public String getDescription();

    public double getCost();
}
