package sda.com.sequence;

import java.util.Arrays;

public class LongestSequence {

	public static void main(String[] args) {
		int[] result = findLongestGrowingSubsequence(new int[]{2,3,-6,1,2,3,-8,4,-2,1});
		System.out.println(Arrays.toString(result));
		result = findLargestGrowingSubsequence(new int[]{2,3,-6,1,2,3,-8,4,-2,1});
		System.out.println(Arrays.toString(result));
	}

	public static int[] findLongestGrowingSubsequence(int[] arr){
		int[] result = new int[]{};
		int begin = 0, end = 0;

		for(int i = 1; i < arr.length; i++){
			if(arr[i] > arr[i-1]) {
				end = i;
			}
			else {
				if (end - begin > result.length){
					result = new int[end - begin];
					System.arraycopy(arr, begin, result, 0, end - begin);
				}
				begin = i;
			}

		}

		return result;
	}
	
	public static int[] findLargestGrowingSubsequence(int[] arr){
		int[] result = new int[]{};
		int[] tmp = new int[]{};
		int begin = 0, end = 0;
		

		for(int i = 1; i < arr.length; i++){
			if(arr[i] > arr[i-1]) {
				end = i;
			}
			else {
				tmp = new int[end - begin];
				System.arraycopy(arr, begin, tmp, 0, end - begin);
				
				if (Arrays.stream(tmp).sum() > Arrays.stream(result).sum()){
					result = tmp;
				}
				begin = i;
			}

		}

		return result;
	}
}
