package sda.com.observer;

import java.util.Scanner;

public class ObserverDemo {
	public static void main(String[] args){
		TemperatureObserver observer = new TemperatureObserver();
		
		observer.addObserver(new TemperatureDisplay());
		observer.addObserver(new ACControler());
		observer.addObserver(new HeatingControler());
		
		Scanner input = new Scanner(System.in);
		
		while (true){
			System.out.println("podaj nowa temperature");
			observer.setTemperature(input.nextInt());
			System.out.println();
		}
		
		
		
	}
}
