package sda.com.observer;

public class HeatingControler implements Observer {
	private boolean heatingWorking = false;
	@Override
	public void notify(int i) {
		if(!heatingWorking && i < 17){
			heatingWorking = true;
			System.out.println("wlaczam ogrzewanie");
		}
		
		if(heatingWorking && i > 22){
			heatingWorking = false;
			System.out.println("wylaczam ogrzewanie");
		}

	}

}
