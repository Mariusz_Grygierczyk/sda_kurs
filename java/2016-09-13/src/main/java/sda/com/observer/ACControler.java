package sda.com.observer;

public class ACControler implements Observer{
	private boolean acWorking = false;
	
	@Override
	public void notify(int i) {
		if(!acWorking && i > 24){
			acWorking = true;
			System.out.println("wlaczam klimatyzacje");
		}
		
		if(acWorking && i < 20){
			acWorking = false;
			System.out.println("wylaczam klimatyzacje");
		}
		
	}
}
