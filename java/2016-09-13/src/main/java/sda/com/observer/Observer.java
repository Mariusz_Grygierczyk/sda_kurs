package sda.com.observer;

public interface Observer {
	public void notify(int i);
}
