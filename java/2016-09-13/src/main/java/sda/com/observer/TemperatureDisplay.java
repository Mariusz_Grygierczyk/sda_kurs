package sda.com.observer;

public class TemperatureDisplay implements Observer {

	@Override
	public void notify(int i) {
		System.out.println("temperature: " + i);

	}

}
