package sda.com.observer;

import java.util.ArrayList;
import java.util.List;

public class TemperatureObserver {
	int temperature;
	List<Observer> observers = new ArrayList<Observer>();
	
	public void setTemperature(int newTemperature){
		this.temperature = newTemperature;
		observers.forEach((o) -> o.notify(temperature));
	}
	
	public void addObserver(Observer newObserver){
		observers.add(newObserver);
	}
}
