package pl.sdacademy.course.model;

import java.util.HashMap;
import java.util.Map;


public class StudentsStorage {
    private static StudentsStorage instance;
    private Map<String, Student> students;

    private StudentsStorage(){
        students = new HashMap<String, Student>();
        students.put("1", new Student("1","1","b", "c","t"));
    }

    public static StudentsStorage getInstance(){
        if (instance == null) instance = new StudentsStorage();
        return instance;
    }

    public void addStudent(String id, Student student){
        student.setId(id);
        students.put(id,student);
    }

    public Map<String,Student> getStorageMap(){
        return students;
    }

    public boolean removeStudent(String id){
        if (students.containsKey(id)){
            students.remove(id);
            return true;
        } else return false;
    }

    public Student getStudent(String id) {
        return students.get(id);
    }

    public boolean containsID(String id){
        return students.containsKey(id);
    }
}
