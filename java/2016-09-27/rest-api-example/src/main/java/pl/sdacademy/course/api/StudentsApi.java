package pl.sdacademy.course.api;

import pl.sdacademy.course.model.Student;
import pl.sdacademy.course.model.StudentsStorage;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

@Path("/course")
public class StudentsApi {


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/students")
    public Response postStudent(Student student){
        StudentsStorage storage = StudentsStorage.getInstance();
        String id = UUID.randomUUID().toString();
        storage.addStudent(id,student);
        return Response.status(201).header("location", "http://localhost:8090/rest-api-1.0/api/course/student/"+id).entity(student.toString() + " created").build();

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/students")
    public List<Student> getStudents(){
        StudentsStorage storage = StudentsStorage.getInstance();
        List<Student> result = new ArrayList<>();
        Map<String, Student> studentMap = storage.getStorageMap();
        studentMap.forEach((key, value) -> result.add(value));
        return result;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/students/{id}")
    public Response getStudent(@PathParam("id") String id){
        StudentsStorage storage = StudentsStorage.getInstance();
        if (storage.containsID(id)) return Response.ok(storage.getStudent(id),MediaType.APPLICATION_JSON).build();
        else return Response.status(404).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/students/{id}")
    public Response putStudent(@PathParam("id") String id, Student student){
        StudentsStorage storage = StudentsStorage.getInstance();
        if(storage.containsID(id)){
            storage.addStudent(id,student);
            return Response.ok("replaced").build();
        } else return Response.status(404).build();
    }



    @DELETE
    @Path("/students/{id}")
    public Response deleteStudent(@PathParam("id") String id){
        StudentsStorage storage = StudentsStorage.getInstance();
        if (storage.removeStudent(id)) return Response.ok("deleted").build();
        else return Response.status(404).build();
    }
}
