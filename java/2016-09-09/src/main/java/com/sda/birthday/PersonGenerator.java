package com.sda.birthday;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PersonGenerator {
	private List<String> names = new ArrayList<String>(Arrays.asList("Adam","Marek","Anna","Maria"));
	Random random;
	
	public PersonGenerator(){
		random = new Random();
	}
	
	private LocalDate getRandomDate(){
		int year = random.nextInt(60) + 1950;
		int month = random.nextInt(12) + 1;
		int day;
		if (month == 2) day = random.nextInt(28) + 1;
		else if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) day = random.nextInt(31) + 1;
		else  day = random.nextInt(30) + 1;
		return LocalDate.of(year, month, day);
	}
	
	public Person generatePerson(){
		String name = names.get(random.nextInt(names.size() - 1));	
		return new Person(name, getRandomDate());
	}
	
}
