package com.sda.birthday;

import java.time.LocalDate;
import java.util.Objects;


public class Person {
	String name;
	LocalDate dateOfBirth;
	
	public Person(String name, LocalDate dateOfBirth) {
		super();
		this.name = name;
		this.dateOfBirth = dateOfBirth;
	}
	
	@Override
	public int hashCode(){
		return Objects.hash(dateOfBirth.getMonthValue(), dateOfBirth.getDayOfMonth());
	}
	
	@Override
	public boolean equals(Object obj){
		if (!(obj instanceof Person)) return false;
		else {
			Person other = (Person)obj;
			if((dateOfBirth.getMonthValue() == other.dateOfBirth.getMonthValue()) && (dateOfBirth.getDayOfMonth() == other.dateOfBirth.getDayOfMonth())) return true;
			else return false;
		}
	}
}
