package com.sda.birthday;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.OptionalDouble;

public class BirthsdayTest {
	public static void main(String[] args){
		PersonGenerator generator = new PersonGenerator();
		
		
		ArrayList<Integer> results = new ArrayList<Integer>(); 
		int result = 0;
		
		for (int i = 0; i < 30; i++){
			HashSet<Person> people = new HashSet<Person>();
			result = 0;
			while(people.add(generator.generatePerson())) result++;
			results.add(result);	
			System.out.println(result);
		}	
	
		OptionalDouble avg = results.stream().mapToInt((r) -> r.intValue() ).average();
		System.out.println("srednia " + avg.getAsDouble());
		
	}
}
