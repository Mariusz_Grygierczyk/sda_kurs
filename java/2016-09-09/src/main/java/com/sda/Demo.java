package com.sda;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

public class Demo {

    public static void main(String[] args) throws NoSuchAlgorithmException {

        Stream<String> stringStream = Stream.generate(() -> "Some string").limit(3);
        //Co sie stanie po odkomentowaniu. Stream zamykany
        // System.out.println("Ilosc elementow stream: "+stringStream.count());
//        System.out.println("Elementy: ");
//        stringStream.forEach(System.out::println);
      //  showIterable();
      //  showAllMatch();
      //  showAnyMatch();
      //  showSorted();
      //  showScanner();
      //  showBinarySearch();
        showSHA1();

    }



    //https://crackstation.net/ - tablica teczowa
    private static void showSHA1() throws NoSuchAlgorithmException {
        MessageDigest instance = MessageDigest.getInstance("SHA-1");
        String password = "password";
        byte[] digest = instance.digest(password.getBytes());

        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < digest.length; i++) {
            sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
        }

        System.out.println("Po wykonaniu funkcji sha-1: "+sb.toString());
    }

    private static void showBinarySearch() {
        List<Integer> integers = Stream.of(1, 2, 3, 4, 5, 6, 7, 8, 10).collect(Collectors.toList());

        int index = Collections.binarySearch(integers, 3);
        System.out.println("Indeks elementu 3 to: "+index);

    }

    private static void showScanner() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj swoje imie:");
        String next = sc.next();
        System.out.println("podales: "+next);
    }


    public static void showIterable() {

        Set<String> languages = Stream.of("Java", "c++", "html", "css", "java script").collect(toSet());
        SoftwareDeveloper mark = new SoftwareDeveloper("Mark", 29, languages);

        System.out.println("Tutaj: " + mark + ", ktory zna:");

        for (String language : mark) {
            System.out.print(language + " ");
        }

    }


    public static void showSorted() {
        Stream.of(100, 70, 90, 80, 50, 30).sorted().forEach(l -> System.out.print(l + " "));

        Set<String> markLang = Stream.of("Java", "c++", "html", "css", "java script").collect(toSet());
        Set<String> johnLang = Stream.of("C#", "c++", "css", "java script").collect(toSet());
        Set<String> kateLang = Stream.of("html","css","java script").collect(toSet());
        SoftwareDeveloper mark = new SoftwareDeveloper("Mark", 29, markLang);
        SoftwareDeveloper john = new SoftwareDeveloper("John", 26, johnLang);
        SoftwareDeveloper kate = new SoftwareDeveloper("Kate", 28, kateLang);

        Stream.of(mark,kate,john).sorted((s1,s2)->Integer.compare(s1.getAge(),s2.getAge())).forEach(s->System.out.print(s+" "));

    }


    public static void showAllMatch() {
        boolean allMatch = Stream.of(2, 4, 12, 22, 98, 1050, 44, 5874).allMatch(i -> i % 2 == 0);
        System.out.println("Czy wszystkie liczby sa parzyste: " + allMatch);
    }


    public static void showAnyMatch() {
        boolean anyMatch = Stream.of("One", "Two", "Three", "Five", "Ten", "Eleven").anyMatch(s -> s.contains("r"));
        System.out.println("Czy, ktorys wyraz zawiera 'r' " + anyMatch);
    }


    static class SoftwareDeveloper implements Iterable<String> {

        private String name;
        private int age;
        private Set<String> languages;

        public SoftwareDeveloper(String name, int age, Set<String> languages) {
            this.name = name;
            this.age = age;
            this.languages = languages;
        }

        @Override
        public Iterator<String> iterator() {
            return languages.iterator();
        }

        public String getName() {
            return name;
        }

        public int getAge() {
            return age;
        }

        public Set<String> getLanguages() {
            return languages;
        }

        @Override
        public String toString() {
            return "SoftwareDeveloper{" +
                    "name='" + name + '\'' +
                    ", age=" + age +
                    '}';
        }
    }

}
