package com.sda.binary;

import java.util.Random;

public class BinarySearchCpuPlayer implements Player{
	private String name;
	private Random random;
	private int min;
	private int max;
	private int guess;
	
	public BinarySearchCpuPlayer(String name) {
		this.name = name;
		random = new Random();
		min = 0;
		max = 100;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int randomNumber() {
		return random.nextInt(100) + 1;
	}

	@Override
	public int nextGuess() {
		guess = (min + max) / 2;
		return guess;
	}
	
	@Override
	public int nextGuess(int result) {
		if (result < 0) max = guess - 1;
		else min = guess + 1;
		guess = (min + max) / 2;
		return guess;
	}
}
