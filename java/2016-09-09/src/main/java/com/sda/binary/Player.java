package com.sda.binary;

public interface Player {
	int randomNumber();
	int nextGuess();
	int nextGuess(int result);
	String getName();
}
