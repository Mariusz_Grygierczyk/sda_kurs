package com.sda.binary;

import java.util.Scanner;

public class HumanPlayer implements Player{
	private String name;
	private Scanner input;
	
	public HumanPlayer(String name) {
		this.name = name;
		this.input = new Scanner(System.in);
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int randomNumber() {
		System.out.println(name + " podaj liczbe 1-100");
		return input.nextInt();
		
	}


	@Override
	public int nextGuess() {
		System.out.println(name + " podaj liczbe 1-100");
		return input.nextInt();
	}
	
	@Override
	public int nextGuess(int result) {
		System.out.println(name + " podaj liczbe 1-100");
		return input.nextInt();
	}
}
