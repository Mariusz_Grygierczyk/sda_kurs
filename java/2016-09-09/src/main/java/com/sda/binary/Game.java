package com.sda.binary;

public class Game {
	int winningNumber;
	int attempts; 
	
	public Game(int winningNumber){
		this.winningNumber = winningNumber;
		attempts = 0;
	}
	
	public int isWinningNumber(int number){
		attempts++;
		return winningNumber - number;		
	}

	public int getAttempts() {
		return attempts;
	}
	
	
}
