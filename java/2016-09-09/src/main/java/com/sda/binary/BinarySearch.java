package com.sda.binary;

public class BinarySearch {
	public static void main(String[] args){
		
		Player player1 = new HumanPlayer("HumanPlayer");
		Player player2 = new RandomCpuPlayer("StupidBot");
//		Player player2 = new BinarySearchCpuPlayer("SmartBot");

		
		Game game = new Game(player1.randomNumber());
		int guess = player2.nextGuess();
		int result;
		String resultString;
		
		while ((result = game.isWinningNumber(guess)) != 0){
			resultString = result > 0 ? "wieksza" : "mniejsza";
			System.out.println("proba: "+ game.getAttempts() + " szukana liczba jest " + resultString + " od " + guess);
			guess = player2.nextGuess(result);
		}
		System.out.println("szukana liczba to " + guess + " " + player2.getName() + " zgadl po " + game.getAttempts() + " probach");
	}
}
