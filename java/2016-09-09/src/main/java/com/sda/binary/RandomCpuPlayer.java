package com.sda.binary;

import java.util.Random;

public class RandomCpuPlayer implements Player {
	private String name;
	private Random random;
	
	public RandomCpuPlayer(String name) {
		this.name = name;
		random = new Random();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public int randomNumber() {
		return random.nextInt(100) + 1;
	}

	@Override
	public int nextGuess() {
		return random.nextInt(100) + 1;
	}
	@Override
	public int nextGuess(int result) {
		return random.nextInt(100) + 1;
	}


}
