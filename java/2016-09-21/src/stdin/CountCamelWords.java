package stdin;

import java.util.Scanner;

//zad 2
public class CountCamelWords {
	public static void main(String[] args){
		Scanner scanenr = new Scanner(System.in);
		CountCamelWords counter = new CountCamelWords();
		String input;
		int result;
		
		System.out.println("podaj fraze");
		
		input = scanenr.next();
		result = counter.countCamelWords(input);
		
		
		System.out.println("ilo�� wyraz�w: " + result);
		scanenr.close();
	}
	
	public int countCamelWords(String input){
		int result = 1;
		for(int i = 0; i < input.length(); i++){
			if (Character.isUpperCase(input.charAt(i))) result++;
		}
		return result;
	}
}
