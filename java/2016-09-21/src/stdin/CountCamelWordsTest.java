package stdin;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class CountCamelWordsTest {
	private CountCamelWords counter;

	@Before
	public void setUp() throws Exception {
		counter = new CountCamelWords();
	}

	@After
	public void tearDown() throws Exception {
		counter = null;
	}

	@Test
	public void test() {
		Assert.assertEquals(2, counter.countCamelWords("camelCase"));
		Assert.assertEquals(5, counter.countCamelWords("testTestTestTestTest"));
		Assert.assertEquals(1, counter.countCamelWords("camel"));
	}

}
