package stdin;

public class BigIntegerMath {
	public static void main(String[] args){
//		if(args.length < 2){
//			System.out.println("nie podano argumentów");
//			System.exit(1);
//		}
		
//		System.out.println(new BigInteger(args[0]).multiply(new BigInteger(args[1])));
		
		
//		int[] arg1 = stringToIntArray(args[0]);
//		int[] arg2 = stringToIntArray(args[1]);
//		int[] result = addBigInt(arg1, arg2);
//		System.out.println(intArrayToString(result));
		
		BigInteger test = new BigInteger("+1234");
		System.out.println(test);
		test = new BigInteger("-1234");
		System.out.println(test);
		test = new BigInteger("1234");
		System.out.println(test);
		
	}
	
	public static int[] addBigInt(int[] arg1, int[] arg2){
		int[] result = new int[Math.max(arg1.length, arg2.length) + 1];
		int current;
		int carry = 0;
		int i;
		
		for (i = 0; i < Math.min(arg1.length, arg2.length); i++){
			current = arg1[i] + arg2[i] + carry;
			result[i] = current % 10;
			carry = current/10;
		}
		
		if(arg1.length < arg2.length){
			while(i < result.length - 1){
				current = arg2[i] + carry;
				result[i] = current % 10;
				carry = current/10;
				i++;
			}
		} else {
			while(i < result.length - 1){
				current = arg1[i] + carry;
				result[i] = current % 10;
				carry = current/10;
				i++;
			}
		}
		
		result[i] = carry;
		
		
		if(result[result.length - 1] == 0){
			int[] tmp = result;
			result = new int[tmp.length - 1];
			System.arraycopy(tmp, 0, result, 0, result.length);
		}
		
		return result;
	}
	
	
	
	public static String intArrayToString(int[] arr){
		StringBuilder builder = new StringBuilder();
		
		for(int i = arr.length - 1; i >= 0; i--){
			builder.append(arr[i]);
		}
		return builder.toString();
	}
	
	public static int[] stringToIntArray(String string){
		int[] result = new int[string.length()];
		
		for(int i = 0; i < result.length; i++){
			result[i] = Character.getNumericValue(string.charAt(string.length() - i - 1));
		}
		
		return result;
	}
}
