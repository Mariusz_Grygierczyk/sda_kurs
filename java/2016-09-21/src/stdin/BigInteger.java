package stdin;

public class BigInteger{
	private boolean sign;
	private int[] val;

	public BigInteger(String string){
		if(string.charAt(0) == '+' || string.charAt(0) == '-'){
			sign = string.charAt(0) == '+';
			val = new int[string.length() - 1];
		} else {
			sign = true;
			val = new int[string.length()];
		}
		for(int i = 0; i < val.length; i++){
			val[i] = Character.getNumericValue(string.charAt(string.length() - i - 1));
		}
	}

	public BigInteger(boolean sign, int[] val){
		this.sign = sign;
		this.val = val;
	}

	@Override
	public String toString(){
		StringBuilder builder = new StringBuilder();

		if(!sign) builder.append('-');
		for(int i = val.length - 1; i >= 0; i--){
			builder.append(val[i]);
		}

		return builder.toString();
	}

	public BigInteger add(BigInteger other){
		boolean resultSign;
		int[] resultVal = new int[Math.max(this.val.length, other.val.length) + 1];
		int current;
		int carry = 0;
		int i;

		if(this.sign == other.sign){
			resultSign = this.sign;

			for (i = 0; i < Math.min(this.val.length, other.val.length); i++){
				current = this.val[i] + other.val[i] + carry;
				resultVal[i] = current % 10;
				carry = current/10;
			}

			if(this.val.length < other.val.length){
				while(i < resultVal.length - 1){
					current = other.val[i] + carry;
					resultVal[i] = current % 10;
					carry = current/10;
					i++;
				}
			} else {
				while(i < resultVal.length - 1){
					current = this.val[i] + carry;
					resultVal[i] = current % 10;
					carry = current/10;
					i++;
				}
			}

			resultVal[i] = carry;

			if(resultVal[resultVal.length - 1] == 0){
				int[] tmp = resultVal;
				resultVal = new int[tmp.length - 1];
				System.arraycopy(tmp, 0, resultVal, 0, resultVal.length);
			}

			return new BigInteger(resultSign, resultVal);

		} else if(this.sign) {
			return this.substract(other);
		} else {
			return other.substract(this);
		}
	}
	
	public BigInteger substract(BigInteger other){
		return this;
	}
	
	public BigInteger multiply(BigInteger other){
		boolean resultSign;		
		int[] resultVal = new int[this.val.length + other.val.length];
		int current = 0;
		int carry = 0;
		
		if(this.sign == other.sign){
			resultSign = true;
		} else{
			resultSign = false;
		}
		
		for(int j = 0; j < other.val.length; j++){
			carry = 0;
			for (int i = 0; i < this.val.length; i++){
				current += carry + this.val[i] * other.val[j];
				carry = current / 10;
				resultVal[i + j] = current % 10;
			}
			resultVal[j + this.val.length] += carry;
			
		}
		
		if(resultVal[resultVal.length - 1] == 0){
			int[] tmp = resultVal;
			resultVal = new int[tmp.length - 1];
			System.arraycopy(tmp, 0, resultVal, 0, resultVal.length);
		}
		
		return new BigInteger(resultSign, resultVal);
	}

	
}
