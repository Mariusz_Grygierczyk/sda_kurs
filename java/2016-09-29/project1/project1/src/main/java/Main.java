/**
 * Created by lukasz on 28.09.16.
 */
public class Main {

    public static void main(String [] args) {
        Long timeBefore = System.currentTimeMillis();
        new Generate().generateVeryBigFile();
        Long timeAfter = System.currentTimeMillis();
        System.out.println("time "+ (timeAfter-timeBefore) +" ms");
    }
}
