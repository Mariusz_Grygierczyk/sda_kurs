import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class FileSorting {
    public static void main(String[] args){

        List<Integer> data = loadFile("generated.txt");
        data.forEach(System.out::println);

//        data.sort(Comparator.reverseOrder());
//        saveFile(data);
    }

    private static List<Integer> loadFile(String filename){
        ClassLoader loader = FileSorting.class.getClassLoader();
        List<Integer> data = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get(loader.getResource(filename).toURI()))) {

            stream.forEach((line) -> data.add(Integer.parseInt(line)));

        } catch (IOException | URISyntaxException e) {
            System.out.println("błąd przy wczytywaniu danych");
            e.printStackTrace();
        }

        return data;
    }

    private static void saveFile(List<Integer> data){
        File outputFile = new File("output.txt");

        try(FileWriter writer = new FileWriter(outputFile)) {
//            data.forEach((line) -> writer.write(line.toString()+ "\n"));

            for(Integer integer : data) writer.write(integer.toString() + "\n");

        } catch (IOException e) {
            System.out.println("błąd przy zapisywaniu danych");
            e.printStackTrace();
        }

    }
}
