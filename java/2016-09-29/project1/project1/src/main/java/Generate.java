import java.io.*;
import java.util.*;


public class Generate {

        public void generateBigFile() {
            int capacity = 100_000_000;
            int counter = 1;
            int min = 1;
            Set<Integer> generatedList = new HashSet<>(capacity);
            Random random = new Random();
//            FileOutputStream fileOutputStream = null;
//            OutputStreamWriter outputStreamWriter = null;
//            Writer writer = null;
            File file = new File("generated.txt");
            try (   FileOutputStream fileOutputStream = new FileOutputStream(file);
                    OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                    Writer writer = new BufferedWriter(outputStreamWriter)){

                while (counter < capacity) {
                    int randomNumber = random.nextInt((Integer.MAX_VALUE - min) + 1) + min;
                       if( generatedList.add(randomNumber)){
                        writer.write(Integer.toString(randomNumber) + "\n");
                        counter++;
                       }
                }
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    public void generateVeryBigFile(){
        int capacity = 1_000_000_000;
        int counter = 1;
        int min = 1;
        BitSet generated = new BitSet(Integer.MAX_VALUE);
//        Set<Integer> generatedList = new HashSet<>(capacity);
        Random random = new Random();
        File file = new File("generated.txt");

        try (   FileOutputStream fileOutputStream = new FileOutputStream(file);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                Writer writer = new BufferedWriter(outputStreamWriter)){

            while (counter < capacity) {
                int randomNumber = random.nextInt((Integer.MAX_VALUE - min) + 1) + min;
                if( !generated.get(randomNumber)){
                    writer.write(Integer.toString(randomNumber) + "\n");
                    counter++;
                    generated.set(randomNumber, true);
                }
                if(counter%100000 == 0) System.out.println(counter);
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}


