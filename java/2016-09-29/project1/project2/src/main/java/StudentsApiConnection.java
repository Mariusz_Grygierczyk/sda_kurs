import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class StudentsApiConnection {
    private String url;
    private HttpClient client = HttpClientBuilder.create().build();
    final static Logger logger = Logger.getLogger(StudentsApiConnection.class);

    public StudentsApiConnection(String url) {
        this.url = url;
    }

    public void addStudent(String name, String surname, String email, String interests){
        HttpPost request = new HttpPost(url + "/students");
        request.addHeader("content-type","application/json ");

        String jsonRequest = "{" +
                "\"name\":\""+name+"\"," +
                "\"surname\":\""+surname+"\"," +
                "\"email\":\""+email+"\"," +
                "\"interests\":\""+interests+"\"" +
                "}";


        try {
            request.setEntity(new StringEntity(jsonRequest));
            HttpResponse response = executeHttpRequest(request);
            System.out.println(getResponseBody(response));

        } catch (IOException e) {
            logger.error("błąd przy dodawaniu studenta",e);
        }
    }

    public void addStudent(Student student){
        HttpPost request = new HttpPost(url + "/students");
        request.addHeader("content-type","application/json ");
        ObjectMapper mapper = new ObjectMapper();

        try {
            String jsonString = mapper.writeValueAsString(student);
            request.setEntity(new StringEntity(jsonString));
            HttpResponse response = executeHttpRequest(request);
            System.out.println(getResponseBody(response));

        } catch (IOException e) {
            logger.error("błąd przy dodawaniu studenta",e);
        }
    }

    public List<Student> getStudents(){
        HttpGet request = new HttpGet(url + "/students");
        ObjectMapper mapper = new ObjectMapper();
        Student[] studentsArray = null;
        try {
            HttpResponse httpResponse = executeHttpRequest(request);
            String responseBody = getResponseBody(httpResponse);
            studentsArray = mapper.readValue(responseBody,Student[].class);

        } catch (IOException e) {
            logger.error("błąd przy pobieraniu listy stdentów: ", e);
        }
        return new ArrayList<Student>(Arrays.asList(studentsArray));
    }

    public void deleteAllStudents(){
        int counter = 0;
        List<Student> students = this.getStudents();

        for(Student student : students){
            if(this.deleteStudent(student.getId()).getStatusLine().getStatusCode()==200) counter++;
        }

        System.out.println("deleted "+counter+" students");
    }

    public HttpResponse deleteStudent(String id) {
        HttpDelete request = new HttpDelete(url + "/students/"+id);
        HttpResponse response = null;
        try {
            return executeHttpRequest(request);
        } catch (IOException e) {
            logger.error("error deleting student");
        }
        return response;
    }

    public void updateStudent(Student student){
        HttpPut request = new HttpPut(url + "/students/"+student.getId());
        request.addHeader("content-type","application/json ");
        ObjectMapper mapper = new ObjectMapper();

        try {
            String jsonString = mapper.writeValueAsString(student);
            request.setEntity(new StringEntity(jsonString));
            HttpResponse response = executeHttpRequest(request);

            System.out.println(getResponseBody(response));

        } catch (IOException e) {
            logger.error("błąd przy zmianie studenta",e);
        }
    }


    private HttpResponse executeHttpRequest(HttpUriRequest request) throws IOException {

        HttpResponse response = client.execute(request);
        System.out.println("status code: "+response.getStatusLine().getStatusCode());
        for (Header header : response.getAllHeaders()){
            System.out.println(header);
        }
        return response;

    }

    private String getResponseBody(HttpResponse response) throws IOException {
        StringBuilder result = new StringBuilder();
        if (response.getEntity()==null) return "response body is empty";
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));
        String line;
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        return result.toString();
    }


}
