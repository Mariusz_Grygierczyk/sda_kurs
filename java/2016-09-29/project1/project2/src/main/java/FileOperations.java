import java.io.*;
import java.util.List;

public class FileOperations {
    public static void exportToCSV (List<Student> students, String filename){
        File outputFile = new File(filename);

        try(FileOutputStream fileOutputStream = new FileOutputStream(outputFile);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            Writer writer = new BufferedWriter(outputStreamWriter)) {

            for (Student student : students){
                writer.write(student.getCSVString(",")+"\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
