
public class Student implements Comparable<Student>{
    private String id;
    private String name;
    private String surname;
    private String email;
    private String interests;

    public Student(String name, String surname, String email, String interests) {
        this.name=name;
        this.surname=surname;
        this.email=email;
        this.interests=interests;
    }

    public Student(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(String interests) {
        this.interests = interests;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", interests='" + interests + '\'' +
                '}';
    }

    public String getCSVString(String separator){
        return id+separator+name+separator+surname+separator+email+separator+interests;
    }


    @Override
    public int compareTo(Student other) {
        return id.compareTo(other.id);
    }
}
