package streams;

import java.util.Scanner;

public class StandardIO {
	public static void main(String[] args){
		try(Scanner scanner = new Scanner(System.in)){
			while(scanner.hasNext()){
				String received = scanner.next();
				if(received.equals("quit")){
					System.out.println("bye bye");
					System.exit(0);
				}
				System.out.println("received = " + received);
			}
		}
	}
}
