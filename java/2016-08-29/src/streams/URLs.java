package streams;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class URLs {
	public static void main(String[] args){
		Scanner scanner = null;
		try {
			URL url = new URL("http://www.timeapi.org/utc/now");
			URLConnection connection = url.openConnection();
			InputStream is = connection.getInputStream();
			scanner = new Scanner(is);
			System.out.println("current time is " + scanner.nextLine());
		} catch (MalformedURLException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println("can't access timeapi.org because of " + e.getMessage());
		} finally{
			if(scanner != null){
				scanner.close();
			}
		}
		
	}
}
