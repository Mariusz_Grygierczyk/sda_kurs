package streams;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Scanner;

public class Files {

	private static final String PATH = "test.txt";
	
	public static void main(String[] args) throws IOException{
		Files io = new Files();
		io.displayCurrentPath();
		
		System.out.println("writing ..");
		io.writeToFile();
		
		System.out.println("reading ..");
		io.readFromFile();
	}
	
	private void readFromFile(){
		File file = new File(PATH);
		InputStream is = null;
		Scanner scanner = null;
		try {
			is = new FileInputStream(file);
			scanner = new Scanner(is);
			while(scanner.hasNextLine()){
				System.out.println("next line = " + scanner.nextLine());
			}
		} catch (FileNotFoundException e) {
			System.out.println("nie znaleziono pliku, sprawdź czy na pewno jest w tym samym katalogu co klasa IO");
		} finally{
			if(scanner != null){
				scanner.close();
			}
		}
	}
	
	private void writeToFile() throws IOException{
		File newFile = new File(PATH);
		newFile.createNewFile();
		
		try(OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream(newFile))){
			writer.append("test\n");
			writer.append("test");
		}
	}
	
	private void displayCurrentPath(){
		File directory = new File("");
		System.out.println("current path is " + directory.getAbsolutePath());
	}
	
}
