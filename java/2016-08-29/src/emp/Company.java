package emp;

import java.util.ArrayList;

public class Company implements CompanyPeople, CompanyEmployees{
	private ArrayList<Employee> employees = new ArrayList<Employee>();	
	private ArrayList<Customer> customers = new ArrayList<Customer>();
	
	public void addEmployee (Employee employee){
		employees.add(employee);
	}
	public void removeEmployee (Employee employee){
		employees.remove(employee);
	}
	
	public void addCustomer (Customer customer){
		customers.add(customer);
	}
	public void removeCustomer (Customer customer){
		customers.remove(customer);
	}
	
	public double getAverageSalary(){
		double sum = 0;
		for (Employee e : employees){
			sum += e.getNetSalary();
		}
		return sum / employees.size();
	}
	public double getTotalSalary(){
		double sum = 0;
		for (Employee e : employees){
			sum += e.getNetSalary();
		}
		return sum;
	}
	
	public Employee[] getAllEmployees(){
		return (Employee[]) employees.toArray();
	}
	public Person[] getAllPeople(){
		ArrayList<Person> result = new ArrayList<Person>();
		result.addAll(customers);
		result.addAll(employees);
		return (Person[])result.toArray();		
	}
}
