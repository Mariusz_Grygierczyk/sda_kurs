package emp;

public class Student extends Employee{
	public static final double TAX_RATE = 0.05;
	
	public Student(String firstName, String lastName, String email, double grossSalary) {
		super(firstName, lastName, email, grossSalary);
	}

	public double getNetSalary(){
		return super.getGrossSalary() * (1 - TAX_RATE);
	}
}
