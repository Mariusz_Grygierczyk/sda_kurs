package emp;

public abstract class Employee implements Person{
	private String firstName;
	private String lastName;
	private String email;
	private double grossSalary;
	
	public double getGrossSalary() {
		return grossSalary;
	}

	public Employee(String firstName, String lastName, String email, double grossSalary){
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.grossSalary = grossSalary;
	}
	
	public double getNetSalary(){
		return grossSalary;
	}
	
	public String getEmail() {
		return email;
	}
	
	@Override
	public boolean equals(Object other){
		if (!(other instanceof Employee)) return false;
		else {
			Employee otherAsEmployee = (Employee)other;
			return firstName.equals(otherAsEmployee.firstName) && lastName.equals(otherAsEmployee.lastName);
		}
	}
}
