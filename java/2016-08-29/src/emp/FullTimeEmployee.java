package emp;

public class FullTimeEmployee extends Employee{	
	public static final double TAX_RATE = 0.20;
	public static final double SOCIAL_INSURANCE = 1000.0;
	
	public FullTimeEmployee(String firstName, String lastName, String email, double grossSalary) {
		super(firstName, lastName, email, grossSalary);
	}

	public double getNetSalary(){
		return (super.getGrossSalary() - SOCIAL_INSURANCE) * (1 - TAX_RATE);
	}

}
