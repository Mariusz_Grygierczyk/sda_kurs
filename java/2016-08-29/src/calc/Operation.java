package calc;

public interface Operation {
	int calculate();
}
