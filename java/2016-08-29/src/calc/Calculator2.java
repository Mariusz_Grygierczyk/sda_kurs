package calc;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

public class Calculator2 {

	private static final String PATH = "input.txt";

	public static void main(String[] args) {

		File file = new File(PATH);
		InputStream is = null;
		Scanner scanner = null;
		try {
			is = new FileInputStream(file);
			scanner = new Scanner(is);
			while (scanner.hasNextLine()) {
				calculate(scanner.nextLine());
			}
		} catch (FileNotFoundException e) {
			System.out.println("nie znaleziono pliku, sprawdź czy na pewno jest w tym samym katalogu co klasa IO");
		} finally {
			if (scanner != null) {
				scanner.close();
			}

		}

	}
	
	public static void calculate(String inputString){
		String[] splitInput = inputString.split(" ");

		Operation operation = null;
		switch (splitInput[1]) {
		case "+":
			operation = new Addition(Integer.parseInt(splitInput[0]), Integer.parseInt(splitInput[2]));
			break;
		case "-":
			operation = new Substraction(Integer.parseInt(splitInput[0]), Integer.parseInt(splitInput[2]));
			break;
		case "/":
			operation = new Division(Integer.parseInt(splitInput[0]), Integer.parseInt(splitInput[2]));
			break;
		case "*":
			operation = new Multiplication(Integer.parseInt(splitInput[0]), Integer.parseInt(splitInput[2]));
			break;
		}

		System.out.println(splitInput[0] + " " + splitInput[1] + " " + splitInput[2] + " = " + operation.calculate());
		
	}
}
