package calc;

import java.util.Scanner;

public class Calculator {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Operation operation = null;

		while (scanner.hasNext()) {
			String inputString = scanner.nextLine();
			if (inputString.equals("exit")) {
				scanner.close();
				System.exit(0);
			}
			String[] splitInput = inputString.split(" ");

			switch (splitInput[1]) {
			case "+":
				operation = new Addition(Integer.parseInt(splitInput[0]), Integer.parseInt(splitInput[2]));
				break;
			case "-":
				operation = new Substraction(Integer.parseInt(splitInput[0]), Integer.parseInt(splitInput[2]));
				break;
			case "/":
				operation = new Division(Integer.parseInt(splitInput[0]), Integer.parseInt(splitInput[2]));
				break;
			case "*":
				operation = new Multiplication(Integer.parseInt(splitInput[0]), Integer.parseInt(splitInput[2]));
				break;
			}

			System.out.println(splitInput[0] + " " + splitInput[1] + " " + splitInput[2] + " = " + operation.calculate());

		}

	}
}
