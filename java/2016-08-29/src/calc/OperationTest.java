package calc;

import static org.junit.Assert.*;

import org.junit.Test;

public class OperationTest {

	@Test
	public void test() {
		Operation op = new Addition(2,2);
		assertEquals(op.calculate(), 4);
		op = new Addition(2,-2);
		assertEquals(op.calculate(), 0);
		op = new Addition(0,6);
		assertEquals(op.calculate(), 6);
		
		op = new Substraction(3, 2);
		assertEquals(op.calculate(), 1);
		op = new Substraction(2, 2);
		assertEquals(op.calculate(), 0);
		op = new Substraction(0, 2);
		assertEquals(op.calculate(), -2);
		
		op = new Multiplication(0, 2);
		assertEquals(op.calculate(), 0);
		op = new Multiplication(2, 2);
		assertEquals(op.calculate(), 4);
		op = new Multiplication(-3, 2);
		assertEquals(op.calculate(), -6);
		
		op = new Division(0, 2);
		assertEquals(op.calculate(), 0);
		op = new Division(8, 2);
		assertEquals(op.calculate(), 4);
		op = new Division(3, 2);
		assertEquals(op.calculate(), 1);
	}

}
