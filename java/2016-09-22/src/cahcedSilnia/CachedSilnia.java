package cahcedSilnia;

import java.util.TreeMap;

public class CachedSilnia {
private TreeMap<Long, Long> cache = new TreeMap<Long, Long>();
	
	
	public static void main(String[] args){
		CachedSilnia silnia = new CachedSilnia();
		
		System.out.println(silnia.iterSilnia(6l));
		System.out.println(silnia.iterSilnia(18l));
	}
	
	public CachedSilnia(){
		cache.put(0l, 1l);
		cache.put(1l, 1l);
	}
	
	public long recSilnia(Long arg){
		//if i<0 error
		if(cache.containsKey(arg)) return cache.get(arg);
		else {
			long result = recSilnia(arg-1) * arg;
			cache.put(arg, result);
			return result;
		}
	}
	
	public long iterSilnia(Long arg){
		//if i<0 error 
		if(cache.containsKey(arg)) return cache.get(arg);
		else {			
			Long i = cache.floorKey(arg);
			Long result = cache.get(i);
			while (i < arg){
				i++;
				result *= i;
				cache.put(i, result);
			}
//			System.out.println(cache);
			return result;
			
		}
	}
	
	
}
