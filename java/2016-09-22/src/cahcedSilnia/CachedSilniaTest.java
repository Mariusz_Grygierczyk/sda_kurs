package cahcedSilnia;


import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class CachedSilniaTest {
	protected CachedSilnia silnia;
	
	@Before
	public void setUp() throws Exception {
		silnia = new CachedSilnia();
	}

	@After
	public void tearDown() throws Exception {
		silnia = null;
	}

	@Test
	public void test() {
		Assert.assertEquals(720l, silnia.iterSilnia(6l));
		Assert.assertEquals(720l, silnia.recSilnia(6l));
	}
	
	@Test
	public void test2() {
		Assert.assertEquals(6402373705728000l, silnia.iterSilnia(18l));
		Assert.assertEquals(6402373705728000l, silnia.recSilnia(18l));
	}
	
	@Test
	public void test3() {
		Assert.assertEquals(362880l, silnia.iterSilnia(9l));
		Assert.assertEquals(362880l, silnia.recSilnia(9l));
	}

}
