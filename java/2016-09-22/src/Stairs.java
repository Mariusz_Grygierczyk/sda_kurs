
public class Stairs {
	public static void stairs(int number){
		for(int i = 1; i <= number; i++){
			for(int j = 1; j <= number; j++){
				if (j <= number - i) System.out.print(' ');
				else System.out.print('#');
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args){
		stairs(7);
	}
}
