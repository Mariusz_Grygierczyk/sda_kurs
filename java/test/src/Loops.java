public class Loops{

	public static void main(String[] args){
		for(int i = 0; i < 10; i++){
			System.out.println("for = " + i);
		}

		int j = 0;

		while(j < 10){
			System.out.println("while = " + j);
			j = j + 1;
		}

		int k = 0;

		do{
			System.out.println("do while = " + k);
			k = k + 1;
		} while (k < 10);

		int[] tablica = new int[]{1,2,3,4,5};

		for(int element : tablica){
			System.out.println("tablica = " + element);
		}
	}
}