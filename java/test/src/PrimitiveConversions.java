public class PrimitiveConversions{
	public static void main(String[] args){
		
		int a = 5;
		
		long b = a;
		
		//a = b;
		
		a = (int) b;
		
		Long autoboxedFive = 5l;
		
		String autoboxedFiveAsStr = autoboxedFive.toString();
		
		String parsed = String.valueOf(autoboxedFive);
		
		Long five = Long.valueOf("5");
		long primitiveFive = Long.parseLong("5");
		long backToPrimitiveFive = five.longValue();
		
		System.out.println(backToPrimitiveFive == primitiveFive); // primitives
		System.out.println(autoboxedFive == five); // wrappers
		
		Long one = 435l;
		Long other = 435l;
		
		System.out.println(one == other);
	}
}
