public class Encapsulation{
	public static void main(String[] args){
		Car car = new Car(1000.0);
		System.out.println(car.getLocationInKilometers());
	}

	private static class Car{
		public static final double RATIO = 1.609;
		private double locationInMiles;

		public Car(double locationInMiles){
			if(locationInMiles < 0){
				throw new RuntimeException("location has to be positive");
			}
			this.locationInMiles = locationInMiles;
		}
		
		public double getLocationInKilometers(){
			return locationInMiles * RATIO;
		}
	}
}



