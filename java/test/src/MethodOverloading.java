public class MethodOverloading{
	public static void main(String[] args){
		Calculator calculator = new Calculator();
		
		calculator.sum(1,2);
		calculator.sum(1.0,2.0);
		
		int intNumber = 1;
		double number = intNumber;
		
		calculator.sum(number, number);
	}
}

class Calculator{
	public double sum(int a, int b){
		System.out.println("sum of ints");
		return a + b;
	}
	
	public double sum(double a, double b){
		System.out.println("sum of two doubles");
		return a + b;
	}
	
	public double sum(double a, double b, double c){
		System.out.println("sum of three doubles");
		return a + b + c;
	}
}
