public class Methods{
	public static void main(String[] args){
		Car car = new Car();
		car.accelerate();
		car.accelerate();
		System.out.println(car.getCurrentSpeed());
		car.brake();
		System.out.println(car.getCurrentSpeed());
		
		System.out.println(Engine.getNumberOfEngines());
		Engine engine1 = new Engine();
		Engine engine2 = new Engine();
		System.out.println(Engine.getNumberOfEngines());
	}

	private static class Car{
		private int speed = 0;
		
		public void accelerate(){
			speed++;
		}
		
		public void brake(){
			speed--;
		}

		public int getCurrentSpeed(){
			return speed;
		}
	}
	
	static class Engine{
		private static int numberOfEngines = 0;

		public Engine(){
			numberOfEngines++;
		}

		public static int getNumberOfEngines(){
			return numberOfEngines;
		}
	}
}
