public class Strings{

	public static void main(String[] args){
		String napis = "hello";
		String napis2 = new String("hello");

		// string pool
		System.out.println(napis == napis2);
		System.out.println(napis.equals(napis2));
		System.out.println(napis == napis2.intern());
		
		System.out.println(napis + ", " + napis2);
		
		// immutable (niezmienny)
		String joined = napis.concat(" world");
		System.out.println("new napis = " + napis);
		System.out.println("joined = " + joined);
		
		System.out.println(napis.length());
		System.out.println(napis.isEmpty());
		
		// access char at index 0
		System.out.println(napis.charAt(0));
		
		// create substring from index 1 (inclusive) to index 3 (exclusive)
		System.out.println(napis.substring(1,3));
	}
}