package com.sda.calc;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class BasicCalculatorTest {

    private Calculator calculator;

    @Before
    public void setUp() {
        calculator = new BasicCalculator();
        //       System.out.println("Before");
    }

    @After
    public void tearDown() {
        calculator = null;
//        System.out.println("After");
    }


    @Test
    public void testAdd() {
        //given
        int expected = 5;

        //when
        int result = calculator.add(2, 3);

        //then
        Assert.assertEquals(expected, result);
    }

    @Test
    public void testSubtract() {
        int expected = 99;
        
        int result = calculator.subtract(200, 101);
        
        Assert.assertEquals(expected, result);
    }


    @Test
    public void testMultiply() {
    	int expected = 256;
        
        int result = calculator.multiply(64, 4);
        
        Assert.assertEquals(expected, result);
    }


    @Test
    public void testDivide() {
        float expected = 5;
        
        float result = calculator.divide(50, 10);
        
        Assert.assertEquals(expected, result, 0.0001f);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testThrownIllegalArgumentException() {
        calculator.divide(10, 0);
    }


    @Test
    public void testThrownExceptionOtherApproach() {
        try {
            calculator.divide(10, 0);
            Assert.fail();
        } catch (IllegalArgumentException e) {
            Assert.assertEquals("Can't divide by 0", e.getMessage());
        }
    }


}