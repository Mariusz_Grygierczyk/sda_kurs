package com.sda.sort;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SortableTest {

	private Sortable sort;
	private RandomArray randomArray;
	
	@Before
	public void setUp() throws Exception {
		randomArray = new RandomArray();
		
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testRandomArray() {
		int[] array = randomArray.getRandomArray(100, 100);
		
		Assert.assertEquals(100, array.length);
	}
	
	@Test
	public void testBubbleSort() {
		sort = new BubbleSort();
		int[] array = randomArray.getRandomArray(100, 100);
		
		sort.sort(array);
		
		Assert.assertEquals(100, array.length);
		for(int i = 1; i < array.length; i++){
			if (array[i-1] > array[i])Assert.fail();
		}
	}
	
	@Test
	public void testBubbleSortLargeSize() {
		sort = new BubbleSort();
		int[] array = randomArray.getRandomArray(100_000, 100);
				
		sort.sort(array);
		
		Assert.assertEquals(100_000, array.length);
		for(int i = 1; i < array.length; i++){
			if (array[i-1] > array[i])Assert.fail();
		}		
	}
	
	@Test
	public void testCountingSort() {
		sort = new CountingSort();
		int[] array = randomArray.getRandomArray(100, 100);
		
		sort.sort(array);
		
		Assert.assertEquals(100, array.length);		
		for(int i = 1; i < array.length; i++){
			if (array[i-1] > array[i])Assert.fail();
		}		
	}
	
	@Test
	public void testCountingSortLargeSize() {
		sort = new CountingSort();
		int[] array = randomArray.getRandomArray(100_000, 100);
		
		sort.sort(array);
		
		Assert.assertEquals(100_000, array.length);
		for(int i = 1; i < array.length; i++){
			if (array[i-1] > array[i])Assert.fail();
		}		
	}

}
