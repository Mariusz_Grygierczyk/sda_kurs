package com.sda.mock;


import org.junit.Test;
import org.mockito.InOrder;

import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;

public class ListTest {


    @Test
    public void testGetFirst(){
        // tworzymy mocka
        List mockedList = mock(List.class);

    // dodaj do list element
        mockedList.add("one");
        mockedList.clear();

    // sprawdz czy zostalo uruchomione
        verify(mockedList).add("one");
        verify(mockedList).clear();
    }


    @Test(expected = RuntimeException.class)
    public void stubbing(){
        LinkedList mockedList = mock(LinkedList.class);

        //stub
        when(mockedList.get(0)).thenReturn("first");
        when(mockedList.get(1)).thenThrow(new RuntimeException());

        //wypisze "first"
        System.out.println(mockedList.get(0));

        //rzuci wyjatek
        System.out.println(mockedList.get(1));
    }


    @Test
    public void atLeastAtMost(){
        LinkedList mockedList = mock(LinkedList.class);

        mockedList.add("once");

        mockedList.add("twice");
        mockedList.add("twice");

        mockedList.add("three times");
        mockedList.add("three times");
        mockedList.add("three times");

        //following two verifications work exactly the same - times(1) is used by default
        verify(mockedList).add("once");
        verify(mockedList, times(1)).add("once");

        //exact number of invocations verification
        verify(mockedList, times(2)).add("twice");
        verify(mockedList, times(3)).add("three times");

        //verification using never(). never() is an alias to times(0)
        verify(mockedList, never()).add("never happened");

        //verification using atLeast()/atMost()
        verify(mockedList, atLeastOnce()).add("three times");
        verify(mockedList, atLeast(2)).add("twice");
        verify(mockedList, atMost(5)).add("three times");

    }


    @Test(expected = RuntimeException.class)
    public void stubbingVoidMethod(){
        List list = mock(List.class);

        doThrow(new RuntimeException()).when(list).clear();

        //following throws RuntimeException:
        list.clear();
    }


    @Test
    public void order(){
        List singleMock = mock(List.class);

        //Dodajemy 2 elementy
        singleMock.add("was added first");
        singleMock.add("was added second");

        //tworzymy weryfikacje
        InOrder inOrder = inOrder(singleMock);

        //zamien kolejnoscia i zobacz co sie stanie
        inOrder.verify(singleMock).add("was added first");
        inOrder.verify(singleMock).add("was added second");

    }

}
