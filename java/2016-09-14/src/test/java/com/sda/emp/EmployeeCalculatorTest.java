package com.sda.emp;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmployeeCalculatorTest {

	private EmployeeCalculator calculator;
	
	@Before
	public void setUp(){
		calculator = new EmployeeCalculatorImplementation();
	}

	@After
	public void tearDown(){
	}

	@Test
	public void testSalaryDifference() {
		Employee first = new Employee("aaa", 11000.0, LocalDate.now());
		Employee second = new Employee("bbb", 12000.0, LocalDate.now());
		
		double expected = 1000.0;
		
		double result = calculator.salaryDifference(first, second);
		
		Assert.assertEquals(expected, result, 0.01);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testSalaryIllegalArgument() {
		Employee first = new Employee("aaa", 15000.0, LocalDate.now());
		Employee second = new Employee("bbb", 12000.0, LocalDate.now());
		
		calculator.salaryDifference(first, second);
	}
	
	@Test
	public void testAgeDifference(){
		LocalDate date1 = LocalDate.of(1991, 1, 12);
		Employee first = new Employee("aaa", 0.0, date1);
		LocalDate date2 = LocalDate.of(1992, 2, 11);
		Employee second = new Employee("bbb", 0.0, date2);
		
		long expected = ChronoUnit.DAYS.between(date1, date2);
		
		long result = calculator.ageDifference(first, second);
		
		Assert.assertEquals(expected, result);
	}

}
