package com.sda.bank;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BankServiceTest {
	private BankService service;

	@Before
	public void setUp() throws Exception {
		service = new BankService();
	}

	@After
	public void tearDown() throws Exception {
		service = null;
	}

	@Test
	public void test() {
		double expected1 = 1161.62;
		double expected2 = 3153.32;
		double expected3 = 2489.60;
		
		double result1 = service.calculateInvestment(new BankDeposit(1000, 5, 0.03, 12));
		double result2 = service.calculateInvestment(new BankDeposit(3000, 2, 0.025, 4));
		double result3 = service.calculateInvestment(new BankDeposit(2300, 4, 0.02, 1));
		
		Assert.assertEquals(expected1, result1, 0.01);
		Assert.assertEquals(expected2, result2, 0.01);
		Assert.assertEquals(expected3, result3, 0.01);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArguments1(){
		service.calculateInvestment(new BankDeposit(1200, -3, 0.1, 2));
	}
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArguments2(){
		service.calculateInvestment(new BankDeposit(1200, 2, 0, 2));
	}
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArguments3(){
		service.calculateInvestment(new BankDeposit(-12, 2, 0.1, 2));
	}
	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArguments4(){
		service.calculateInvestment(new BankDeposit(1200, 2, 0.1, 22));
	}

}
