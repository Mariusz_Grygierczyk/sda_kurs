package com.sda.bank;



import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class UserTest {
	private User user;
	
	@Before
	public void setUp(){
		user = new User("Jan Kowalski");
		user.addBankAccount(new BankAccount("1", 315));
		user.addBankAccount(new BankAccount("2", 1841));
		user.addBankAccount(new BankAccount("3", 2753));
	}

	
	@After
	public void tearDown(){
		user = null;
	}

	@Test
	public void testGetTotalAmount() {
		long expected = 4909;
		
		long result = user.getTotalAmount();
		
		Assert.assertEquals(expected, result);
	}
	
	@Test
	public void testGetAmount(){
		long expected1 = 315;
		long expected2 = 1841;
		long expected3 = 2753;
		
		long result1 = user.getAmount("1");
		long result2 = user.getAmount("2");
		long result3 = user.getAmount("3");
		
		Assert.assertEquals(expected1, result1);
		Assert.assertEquals(expected2, result2);
		Assert.assertEquals(expected3, result3);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetAmountIllegalArgument() {
        user.getAmount("4");
    }

}
