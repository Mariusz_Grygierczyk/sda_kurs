package com.sda.bank;

public class BankDeposit {
	private long startMoney;
	private int years;
	private double intrestRate;
	private int periodsPerYear;
	
	
	
	public BankDeposit(long startMoney, int years, double intrestRate, int periodsPerYear) {
		if (startMoney <= 0 || years <= 0 || intrestRate <= 0 || periodsPerYear <= 0 || periodsPerYear > 12) throw new IllegalArgumentException();
		this.startMoney = startMoney;
		this.years = years;
		this.intrestRate = intrestRate;
		this.periodsPerYear = periodsPerYear;
	}
	public long getStartMoney() {
		return startMoney;
	}
	public void setStartMoney(long startMoney) {
		this.startMoney = startMoney;
	}
	public int getYears() {
		return years;
	}
	public void setYears(int years) {
		this.years = years;
	}
	public double getIntrestRate() {
		return intrestRate;
	}
	public void setIntrestRate(double intrestRate) {
		this.intrestRate = intrestRate;
	}
	public int getPeriods() {
		return periodsPerYear;
	}
	public void setPeriods(int periods) {
		this.periodsPerYear = periods;
	}
	
	
}
