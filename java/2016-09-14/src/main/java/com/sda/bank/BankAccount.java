package com.sda.bank;



public class BankAccount {

    private String number;
    private long moneyAmount;
    
	

	public BankAccount(String number, long moneyAmount) {
		this.number = number;
		this.moneyAmount = moneyAmount;
	}
	
	public long getMoneyAmount() {
		return moneyAmount;
	}

	public String getNumber() {
		return number;
	}

    

}
