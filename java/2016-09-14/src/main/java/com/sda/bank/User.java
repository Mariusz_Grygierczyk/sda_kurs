package com.sda.bank;


import java.util.ArrayList;
import java.util.List;


public class User {

	private String name;
	private List<BankAccount> bankAccounts;

	public User(String name) {
		this.name = name;
		this.bankAccounts = new ArrayList<BankAccount>();
	}

	
	public void addBankAccount(BankAccount newAccount){
		bankAccounts.add(newAccount);
	}
	
	public long getTotalAmount(){
		return bankAccounts.stream().mapToLong((acc) -> acc.getMoneyAmount()).sum();
	}
	
	public long getAmount (String accountNumber){
		for (BankAccount acc : bankAccounts){
			if (acc.getNumber().equals(accountNumber)) return acc.getMoneyAmount();
		}
		throw new IllegalArgumentException("account number not found");
	}
}
