package com.sda.bank;

public class BankService {
	
	
	
	public double calculateInvestment (BankDeposit deposit){
		double startMoney = deposit.getStartMoney();
		double rate = deposit.getIntrestRate();
		int periods = deposit.getPeriods();
		int years = deposit.getYears();
		return startMoney * Math.pow((1 + (rate/periods)), years * periods);
	}
}
