package com.sda.calc;


public class BasicCalculator implements Calculator{

    @Override
    public int add(int a, int b) {
        return a + b;
    }

    @Override
    public int subtract(int a, int b) {
        return a - b;
    }

    @Override
    public int multiply(int a, int b) {
       return a * b;
    }

    @Override
    public float divide(int a, int b) {
       if (b == 0) throw new IllegalArgumentException("Can't divide by 0");
       return a / b;
    }

}
