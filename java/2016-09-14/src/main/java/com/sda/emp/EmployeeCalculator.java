package com.sda.emp;

public interface EmployeeCalculator {
	public double salaryDifference(Employee first, Employee second);
	public long ageDifference(Employee first, Employee second);
}
