package com.sda.emp;

import java.time.LocalDate;

public class Employee {
	private String name;
	private double salary;
	private LocalDate dateOfBirth;
	
	public Employee(String name, double salary, LocalDate dateOfBirth) {
		this.name = name;
		this.salary = salary;
		this.dateOfBirth = dateOfBirth;
	}

	public String getName() {
		return name;
	}

	public double getSalary() {
		return salary;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	
	
}
