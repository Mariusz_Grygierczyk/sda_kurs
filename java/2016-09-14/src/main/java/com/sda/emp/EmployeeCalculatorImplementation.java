package com.sda.emp;

import java.time.temporal.ChronoUnit;

public class EmployeeCalculatorImplementation implements EmployeeCalculator{
	
	@Override
	public long ageDifference(Employee first, Employee second) {
		return Math.abs(ChronoUnit.DAYS.between(first.getDateOfBirth(), second.getDateOfBirth()));
	}
	@Override
	public double salaryDifference(Employee first, Employee second) {
		double result = second.getSalary() - first.getSalary();
		if (result < 0) throw new IllegalArgumentException();
		return result;
	}
}
