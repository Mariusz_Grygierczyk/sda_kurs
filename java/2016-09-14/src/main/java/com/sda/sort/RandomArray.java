package com.sda.sort;

import java.util.Random;

public class RandomArray {
	private Random random = new Random();
	
	public int[] getRandomArray(int size,int range){
		int[] arr = new int[size]; 
		for (int i = 0; i < size; i++){
			arr[i] = random.nextInt(range);
		}
		return arr;
	}
}
