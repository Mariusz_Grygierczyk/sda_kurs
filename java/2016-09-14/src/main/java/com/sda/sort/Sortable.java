package com.sda.sort;

public interface Sortable {
	public int[] sort(int [] array);
}
