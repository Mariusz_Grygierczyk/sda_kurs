package com.sda.sort;

import java.util.Arrays;

public class CountingSort implements Sortable{
	public int[] sort(int[] array) {
		int max = 0;
		int min = Integer.MAX_VALUE;
		
		for (int i : array){
			if (i < min) min = i;
			else if (i > max) max = i;
		}
		
		int[] count = new int[max-min+1];
		Arrays.fill(count, 0);
		for (int i : array){
			count[i-min]++;
		}
		
		int index = 0;
	    for (int i = 0; i < count.length; i++)
	    {
	        Arrays.fill(array, index, index + count[i], i + min); 
	        index += count[i];
	    }
	    return array;
	}
}
