package volatility;


public class VolatileThread extends Thread{
    private String name;
    private boolean stillAlive;

    public VolatileThread(String name) {
        this.name = name;
        this.stillAlive = true;
    }

    @Override
    public void run() {
        while (stillAlive){
            System.out.println("i live!!! " + name);
        }
    }

    public void kill(){
        stillAlive = false;
        System.out.println("killed " + name);
    }
}
