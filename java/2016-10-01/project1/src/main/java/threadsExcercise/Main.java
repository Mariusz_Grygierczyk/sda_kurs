package threadsExcercise;


import java.io.File;

public class Main {
    public static void main(String[] args){
        File inputFile = new File("input.txt");
        String url = "http://rest-showcase.cloudhub.io/api";

        Thread fileReaderThread = new Thread(new FileReaderThread(inputFile, url));

        fileReaderThread.start();
        try {
            fileReaderThread.join();
        } catch (InterruptedException e) {
            System.out.println("ThreadTest Thread interrupted");
            e.printStackTrace();
        }
    }
}
