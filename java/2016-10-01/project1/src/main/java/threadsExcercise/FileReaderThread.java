package threadsExcercise;

import studentsApi.Student;
import studentsApi.StudentsApiConnection;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class FileReaderThread implements Runnable {
    private File inputFile;
    private StudentsApiConnection connection;

    public FileReaderThread(File inputFile, String url) {
        this.inputFile = inputFile;
        this.connection = new StudentsApiConnection(url);
    }


    @Override
    public void run() {

        System.out.println(Thread.currentThread().toString() + " starting");
        String line;
        Student student;
        Thread senderThread = null;
        try(Scanner scanner = new Scanner(inputFile)) {

            while(scanner.hasNextLine()){
                line = scanner.nextLine();
                student = Student.parseCSVString(line, ",");
                senderThread = new Thread(new RestSenderThread(student, connection));
                senderThread.start();

            }
            if (senderThread!=null)senderThread.join();
        } catch (IOException e) {
            System.out.println("IOException in reader thread");
            e.printStackTrace();
        } catch (InterruptedException e) {
            System.out.println("Reader Thread interrupted");
            e.printStackTrace();
        }

    }
}
