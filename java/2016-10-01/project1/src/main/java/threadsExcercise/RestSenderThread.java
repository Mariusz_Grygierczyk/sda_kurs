package threadsExcercise;


import studentsApi.Student;
import studentsApi.StudentsApiConnection;

public class RestSenderThread implements Runnable{
    private Student student;
    private StudentsApiConnection connection;

    public RestSenderThread(Student student, StudentsApiConnection connection) {
        this.student = student;
        this.connection = connection;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().toString() + " starting");
        connection.addStudent(student);
    }
}
