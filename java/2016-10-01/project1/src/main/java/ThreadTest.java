
public class ThreadTest {
    public static void main(String[] args){
        StringBuilder builder1 = new StringBuilder();
        StringBuilder builder2 = new StringBuilder();

        Thread thread1 = new Thread(new ThreadExample("cat", builder1, builder2));
        Thread thread2 = new Thread(new ThreadExample("dog", builder2, builder1));

        thread1.start();
        thread2.start();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(builder1.toString());
        System.out.println(builder2.toString());
    }
}
