
public class ThreadExample implements Runnable {

    private String name;
    private StringBuilder builder1;
    private StringBuilder builder2;


    public ThreadExample(String name, StringBuilder builder1, StringBuilder builder2) {
        this.name = name;
        this.builder1 = builder1;
        this.builder2 = builder2;
    }

    public String getName() {
        return name;
    }

    @Override
    public void run() {



        for (int i = 0; i < 10; i++) {


                synchronized (builder1) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    synchronized (builder2) {
                        builder2.append(name +" "+ i +"\n");
                    }
                }

            }



    }
}
