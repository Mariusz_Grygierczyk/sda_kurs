package day1;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;


public class Controller {
    @FXML
    private WebView webView;

    public void showGoogle(ActionEvent actionEvent) {
        WebEngine webEngine = webView.getEngine();
        webEngine.load("http://www.google.pl");
    }
}
