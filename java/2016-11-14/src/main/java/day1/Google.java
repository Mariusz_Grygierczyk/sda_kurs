package day1;

import javafx.application.Application;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;


public class Google extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = getScene();
        scene.getStylesheets().add(Google.class.getResource("styles.css").toExternalForm());
        stage.setScene(scene);
        stage.show();
        stage.setMaximized(true);
    }

    private Scene getScene(){
        BorderPane mainPane = new BorderPane();
        mainPane.setLeft(getLeftPane());
        mainPane.setRight(getRightPane());
        mainPane.setBottom(getBottomPane());
        mainPane.setCenter(getCenterPane());
        mainPane.setTop(getTopPane());
        return new Scene(mainPane, 800, 600);
    }


    private Node getLeftPane() {
        VBox leftPane = new VBox();
        Button button1 = new Button("button1");
        Button button2 = new Button("button2");
        Button button3 = new Button("button3");
        button2.setId("btn2");
        leftPane.getChildren().add(button1);
        leftPane.getChildren().add(button2);
        leftPane.getChildren().add(button3);

        return leftPane;
    }

    private Node getRightPane() {
        TextArea textArea = new TextArea();
        TitledPane pane1 = new TitledPane("panel 1", textArea);
        TitledPane pane2 = new TitledPane("panel 2", new Button("button in panel 2"));

        Accordion accordion = new Accordion(pane1, pane2);
        HBox rightPane = new HBox(accordion);

        return rightPane;
    }

    private Node getTopPane() {
        Label label = new Label("Label");
        ComboBox comboBox = new ComboBox();
        comboBox.getItems().addAll("aa", "bb", "cc");

        HBox topPane = new HBox(label, comboBox);

        return topPane;
    }

    private Node getBottomPane() {
        FlowPane bottomPane = new FlowPane();
        DatePicker datePicker = new DatePicker();
        bottomPane.getChildren().add(datePicker);
        Slider slider = new Slider();
        bottomPane.getChildren().add(slider);

        for (int i = 0; i < 8; i++) {
            bottomPane.getChildren().add(new Button("Button " + i));
        }

        return bottomPane;
    }

    private Node getCenterPane() {
        Tab tab1 = getBrowserTab();
        Tab tab2 = new Tab("Image", new ImageView(new Image("http://java2s.com/style/demo/Firefox.png", true)));

        TabPane centerPane = new TabPane(tab1, tab2);

        return centerPane;
    }

    private Tab getBrowserTab() {
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.load("http://www.google.pl");
        return new Tab("Web View", webView);
    }

    public static void main(String[] args) {
        launch(args);
    }

}
