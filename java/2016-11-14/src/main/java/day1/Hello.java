package day1;


import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Hello extends Application{



    @Override
    public void start(Stage stage) throws Exception {
        Button button = new Button();
        button.setText("Hello world");
        StackPane stackPane = new StackPane();
        stackPane.getChildren().add(button);
        Scene scene = new Scene(stackPane, 300, 300);
        stage.setScene(scene);
        stage.show();
        button.setOnAction((event -> System.out.println("hello java fx")));
    }

    public static void main(String[] args){
        launch(args);
    }
}

