package browser;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;



public class Browser extends Application{
    private View view;
    private Controller controller;
    private Configuration configuration;

    @Override
    public void init() {
        view = new View();
        configuration = Configuration.load();
        controller = new Controller();
        view.setController(controller);
        controller.setView(view);
        controller.setConfiguration(configuration);
    }

    @Override
    public void start(Stage primaryStage) {
        Scene scene = view.getScene();
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setMaximized(true);

    }

    @Override
    public void stop() {
        configuration.save();
    }

    public static void main(String[] args){
        launch(args);
    }
}
