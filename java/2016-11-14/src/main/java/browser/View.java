package browser;

import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;


public class View {
    private Controller controller;

    private WebEngine webEngine;

    public Scene getScene() {
        BorderPane mainPane = new BorderPane();
        mainPane.setRight(getRightPane());
        mainPane.setCenter(getCenterPane());
        mainPane.setTop(getTopPane());
        return new Scene(mainPane, 800, 600);
    }

    private Node getTopPane() {
        TextField addressField = new TextField();
        addressField.setPromptText("address");
        addressField.setOnAction(controller::addressBarEnterPressed);
        Button goButton = new Button("=>");
        goButton.setOnAction(controller::goButtonClicked);
        goButton.setUserData(addressField);
        Button homeButton = new Button("home");
        homeButton.setOnAction(controller::homepageButtonClicked);

        return new HBox(addressField, goButton, homeButton);
    }

    private Node getCenterPane() {
        WebView webView = new WebView();
        webEngine = webView.getEngine();

        return webView;
    }

    private Node getRightPane() {
        TitledPane settingsPane = new TitledPane("Settings", getSettingsPane());
        TitledPane bookmarksPane = new TitledPane("Bookmarks", getBookmarksPane());
        return new Accordion(settingsPane, bookmarksPane);
    }

    private Node getSettingsPane() {
        TextField homepageTextField = new TextField();
        HBox homepagePane = new HBox(new Label("homepage"), homepageTextField);

        Button saveButton = new Button("save");
        saveButton.setUserData(homepageTextField);
        saveButton.setOnAction(controller::saveSettings);

        return new VBox(homepagePane, saveButton);
    }

    private Node getBookmarksPane() {
        Node bookmarksListPane = controller.getBookmarksListPane();

        TextField addTextField = new TextField();
        Button addButton = new Button("save");
        addButton.setUserData(addTextField);
        addButton.setOnAction(controller::addBookmarkButtonClicked);
        HBox addBookmarkPane = new HBox(new Label("add"), addTextField, addButton);

        return new VBox(bookmarksListPane, addBookmarkPane);
    }

    public WebEngine getWebEngine() {
        return webEngine;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }


}
