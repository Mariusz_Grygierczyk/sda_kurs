package browser;


import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller {
    private View view;
    private Configuration configuration;

    private Pattern httpPrefix = Pattern.compile("^http://|^https://");

    private VBox bookmarksListPane;

    public String parseURL(String url) {
        url = ensureHttpPrefix(url);
        return url;
    }

    private String ensureHttpPrefix(String url) {
        Matcher matcher = httpPrefix.matcher(url);
        if (!matcher.matches()) return "http://" + url;
        else return url;
    }

    public void goButtonClicked(ActionEvent actionEvent) {
        Button eventSource = (Button)actionEvent.getSource();
        TextField addressBar = (TextField)eventSource.getUserData();
        String url = addressBar.getText();
        url = parseURL(url);
        goTo(url);
    }

    public void addressBarEnterPressed(ActionEvent actionEvent) {
        TextField addressBar = (TextField)actionEvent.getSource();
        String url = addressBar.getText();
        url = parseURL(url);
        goTo(url);
    }

    public void homepageButtonClicked(ActionEvent actionEvent) {
        goTo(configuration.getHomepage());
    }

    private void goTo(String url){
        view.getWebEngine().load(url);
    }

    public void saveSettings(ActionEvent actionEvent) {
        Button eventSource = (Button)actionEvent.getSource();
        TextField homepageTextField = (TextField)eventSource.getUserData();
        String url = homepageTextField.getText();
        url = parseURL(url);
        configuration.setHomepage(url);
    }

    public void addBookmarkButtonClicked(ActionEvent actionEvent) {
        Button eventSource = (Button)actionEvent.getSource();
        TextField addBookmarkTextField = (TextField)eventSource.getUserData();
        String url = addBookmarkTextField.getText();
        url = parseURL(url);
        configuration.addBookmark(url);
        bookmarksListPane.getChildren().add(generateBookmarkPane(url));
        addBookmarkTextField.setText("");
    }

    private Node generateBookmarkPane(String url) {
        Button goButton = new Button(url);
        goButton.setOnAction(this::bookmarkGoButtonClicked);
        Button deleteButton = new Button("X");
        deleteButton.setOnAction(this::bookmarkDeleteButtonClicked);
        return new HBox(goButton, deleteButton);
    }

    public void bookmarkGoButtonClicked(ActionEvent actionEvent) {
        Button eventSource = (Button)actionEvent.getSource();
        String url = eventSource.getText();
        goTo(url);
    }

    public void bookmarkDeleteButtonClicked(ActionEvent actionEvent) {

    }

    public void setView(View view) {
        this.view = view;
    }

    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    public Node getBookmarksListPane() {
        bookmarksListPane = new VBox();
        for (String url: configuration.getBookmarks()){
            bookmarksListPane.getChildren().add(generateBookmarkPane(url));
        }
        return bookmarksListPane;
    }

}
