package browser;

import com.google.gson.Gson;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class Configuration {


    private String homepage = "http://www.google.pl";
    private Set<String> bookmarks = new HashSet<>();

    public static Configuration load() {
        File configFile = new File("config.json");
        try {
            FileReader fileReader = new FileReader(configFile);
            Gson gson  = new Gson();
            return gson.fromJson(fileReader, Configuration.class);
        } catch (FileNotFoundException e) {
            System.err.println("error loading configuration, using default");
            return new Configuration();
        }

    }

    public void save() {
        File configFile = new File("config.json");
        try {
            FileWriter fileWriter = new FileWriter(configFile);
            fileWriter.write(generateJSONString());
            fileWriter.close();
        } catch (IOException e) {
            System.err.println("error saving configuration");
            e.printStackTrace();
        }
    }

    private String generateJSONString() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public void addBookmark(String url){
        bookmarks.add(url);
    }

    public Set<String> getBookmarks() {
        return bookmarks;
    }
}
