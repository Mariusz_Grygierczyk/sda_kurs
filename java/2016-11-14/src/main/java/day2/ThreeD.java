package day2;

import javafx.animation.Animation;
import javafx.animation.RotateTransition;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.PointLight;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.shape.Box;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * Created by RENT on 2016-11-15.
 */
public class ThreeD extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        Box box = new Box(100, 100, 100);
        box.setTranslateX(150);
        box.setTranslateY(0);
        box.setTranslateZ(400);

        PointLight light = new PointLight();
        light.setTranslateX(350);
        light.setTranslateY(100);
        light.setTranslateZ(300);

        PerspectiveCamera camera = new PerspectiveCamera(false);
        camera.setTranslateX(100);
        camera.setTranslateY(-50);
        camera.setTranslateZ(300);


        Group group = new Group(box, light);
        Scene scene = new Scene(group, 800, 600, true);
        scene.setCamera(camera);
        primaryStage.setScene(scene);
        primaryStage.show();

        scene.setOnKeyPressed(key -> {
            if (key.getCode() == KeyCode.UP) {
                box.setTranslateY(box.getTranslateY() - 15);
            } else if (key.getCode() == KeyCode.DOWN) {
                box.setTranslateY(box.getTranslateY() + 15);
            } else if (key.getCode() == KeyCode.LEFT) {
                box.setTranslateX(box.getTranslateX() - 15);
            } else if (key.getCode() == KeyCode.RIGHT) {
                box.setTranslateX(box.getTranslateX() + 15);
            }
        });

    }

    public static void main(String[] args) {
        launch(args);
    }
}
