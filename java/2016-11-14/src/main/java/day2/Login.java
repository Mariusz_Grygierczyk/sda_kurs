package day2;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Login extends Application{

    private Stage stage;
    private TextField loginTextField;
    private TextField passwordTextField;

    @Override
    public void start(Stage primaryStage) throws Exception {
        Scene scene = getLoggedOutScene();
        this.stage = primaryStage;
        stage.setScene(scene);
        stage.show();
    }

    private Scene getLoggedOutScene(){
        return new Scene(getLoggedOutPane(),800, 600);
    }
    private Scene getLoggedInScene(){
        return new Scene(getLoggedInPane(),800, 600);
    }

    private VBox getLoggedOutPane(){
        loginTextField = new TextField();
        HBox loginBox = new HBox(new Label("login"), loginTextField);
        passwordTextField = new TextField();
        HBox passwordBox = new HBox(new Label("password"), passwordTextField);
        Button loginButton = new Button("login");
        loginButton.setOnAction(this::logIn);

        return new VBox(loginBox, passwordBox, loginButton);
    }

    private VBox getLoggedInPane(){
        Button logoutButton = new Button("logout");
        logoutButton.setOnAction(this::logOut);
        return new VBox(new Label("Welcome Admin"), logoutButton);
    }

    private void logOut(ActionEvent actionEvent) {
        stage.setScene(getLoggedOutScene());
    }

    private void logIn(ActionEvent actionEvent) {
        if (loginTextField.getText().equals("admin") && passwordTextField.getText().equals("admin")){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("login success");
            alert.setContentText("login success");
            alert.show();
            stage.setScene(getLoggedInScene());
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("login fail");
            alert.setContentText("login fail");
            alert.show();
        }

    }

    public static void main(String[] args){
        launch();
    }
}
