package day2;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.effect.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.stage.Stage;


public class Face extends Application{
    @Override
    public void start(Stage primaryStage) throws Exception {
        Circle background = new Circle(250,250,150, Color.RED);
        Polygon nose = new Polygon(270, 290, 210, 290, 245, 250);
        QuadCurve mouth = new QuadCurve(180, 350, 200, 370, 320, 350);

        Ellipse eyeL = new Ellipse(200,190,30, 40);
        eyeL.setFill(Color.BLUE);
        Ellipse eyeR = new Ellipse(300,190,30, 40);
        eyeR.setFill(Color.BLUE);
        Circle irisL = new Circle(205, 220, 5, Color.WHITE);
        Circle irisR = new Circle(295, 220, 5, Color.WHITE);

        background.setEffect(new DropShadow(170, 20, 20, Color.GREEN));
        nose.setEffect(new GaussianBlur());
        mouth.setEffect(new Reflection());

        Group group = new Group(background, nose, mouth, eyeL, eyeR, irisL, irisR);

        Scene scene = new Scene(group, 500, 500);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    public static void main(String[] args){
        launch(args);
    }
}
