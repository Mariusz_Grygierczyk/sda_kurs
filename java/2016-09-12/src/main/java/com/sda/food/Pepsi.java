package com.sda.food;

public class Pepsi extends ColdDrink {

	@Override
	public String name() {
		return "Pepsi";
	}

	@Override
	public double price() {
		return 2.5;
	}

}
