package com.sda.food;

public class BeefBurger extends Burger {

	@Override
	public String name() {
		return "Beef burger";
	}

	@Override
	public double price() {
		return 19.5;
	}

}