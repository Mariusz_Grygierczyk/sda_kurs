package com.sda.food;

public interface Item {
	public String name();
	public Packing packing();
	public double price();
}
