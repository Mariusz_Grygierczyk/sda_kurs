package com.sda.food;

import java.util.ArrayList;
import java.util.List;

public class Meal {
	private List<Item> items = new ArrayList<Item>();

	public void addItem(Item item){
		items.add(item);
	}

	public double getCost(){
		return items.stream().mapToDouble((i) -> i.price()).sum();
	}

	public void showItems(){
		items.stream().forEach((i) -> System.out.println(i.name()));
	}
}
