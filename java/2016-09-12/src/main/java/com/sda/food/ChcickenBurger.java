package com.sda.food;

public class ChcickenBurger extends Burger {

	@Override
	public String name() {
		return "Chicken burger";
	}

	@Override
	public double price() {
		return 15.3;
	}

}
