package com.sda.food;

public class BuilderDemo {
	public static void main(String[] args){
		MealBuilder builder = new MealBuilder();
		
		Meal meal1 = builder.createBeefBurger();
		meal1.showItems();
		System.out.println(meal1.getCost());
		
		System.out.println();
		Meal meal2 = builder.createChickenBurger();
		meal2.showItems();
		System.out.println(meal2.getCost());
	}
}
