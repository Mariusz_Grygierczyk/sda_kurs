package com.sda.money;

import java.util.HashMap;
import java.util.Map;



public class MoneyExchanger {
	private static MoneyExchanger instance = new MoneyExchanger();
	private Map<CurrencyPair, Double> rates = new HashMap<CurrencyPair, Double>();
	
	private MoneyExchanger(){
	}
	
	public static MoneyExchanger getInstance() {
		return instance;
	}	
	
	public Money exchangeMoney(Money money, String toCurrency){
		CurrencyPair exchangedCurrencies = new CurrencyPair(money.getCurrency(), toCurrency);
		
		if(rates.containsKey(exchangedCurrencies)){
			return new Money(toCurrency, money.getAmount() * rates.get(exchangedCurrencies));
		} else {
			System.out.println("nie wymieniamy tych walut");
			return money;
		}
		
		
		
	}
	
	public void setCurrencyRate(String fromCurrency, String toCurrency, double rate ){
		rates.put(new CurrencyPair(fromCurrency, toCurrency), rate);
	}
	
	class CurrencyPair{
		private String from;
		private String to;
		
		public CurrencyPair(String from, String to) {
			this.from = from;
			this.to = to;
		}

		public String getFrom() {
			return from;
		}

		public String getTo() {
			return to;
		}		
		
		@Override 
		public boolean equals(Object o){
			if (!(o instanceof CurrencyPair)) return false;
			else {
				CurrencyPair other = (CurrencyPair)o;
				if((from == other.from) && (to == other.to)) return true;
				else return false;
			}
		}
		
		@Override
		public int hashCode(){
			return from.hashCode() + to.hashCode();
		}
		
	}
	
	
}
