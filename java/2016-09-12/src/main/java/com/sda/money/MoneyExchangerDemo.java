package com.sda.money;

public class MoneyExchangerDemo {
	public static void main(String[] args){
		MoneyExchanger exchanger = MoneyExchanger.getInstance();
		
		
//		PLN -> USD 3.84
		exchanger.setCurrencyRate("PLN", "USD", 1/3.84);
//		USD -> PLN 3.87
		exchanger.setCurrencyRate("USD", "PLN", 3.87);
//		PLN -> EUR 4.32
		exchanger.setCurrencyRate("PLN", "EUR", 1/4.32);
//		EUR -> PLN 4.35
		exchanger.setCurrencyRate("EUR", "PLN", 4.35);
//		PLN -> GBP 5.10
		exchanger.setCurrencyRate("PLN", "GBP", 1/5.10);
//		GBP -> PLN 5.14
		exchanger.setCurrencyRate("GBP", "PLN", 5.14);
//		PLN -> CHF 3.93 
		exchanger.setCurrencyRate("PLN", "CHF", 1/3.93);
//		CHF -> PLN 3.99 
		exchanger.setCurrencyRate("CHF", "PLN", 3.99);
		
		
		Money m = new Money("PLN", 100);
		m = exchanger.exchangeMoney(m, "USD");
		System.out.println(m.getCurrency() + " " + m.getAmount());
		m = exchanger.exchangeMoney(m, "PLN");
		System.out.println(m.getCurrency() + " " + m.getAmount());
		m = exchanger.exchangeMoney(m, "USD");
		System.out.println(m.getCurrency() + " " + m.getAmount());
		m = exchanger.exchangeMoney(m, "PLN");
		System.out.println(m.getCurrency() + " " + m.getAmount());
		
	}
}
