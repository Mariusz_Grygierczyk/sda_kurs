package com.sda.drinks;

public class Latte implements Drink {

	@Override
	public String getDescription() {
		return "Kawa latte";
	}

	@Override
	public double calculateCost() {
		return 5.5;
	}

}
