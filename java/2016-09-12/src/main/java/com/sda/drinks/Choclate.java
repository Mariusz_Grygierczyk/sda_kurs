package com.sda.drinks;

public class Choclate extends DrinkIngredient {

	public Choclate(Drink drink) {
		super(drink);
	}

	@Override
	public String getDescription() {
		return drink.getDescription() + " czekolada";
	}

	@Override
	public double calculateCost() {
		return drink.calculateCost() + 1.5;
	}

}
