package com.sda.drinks;

public class Espresso implements Drink {

	@Override
	public String getDescription() {
		return "Kawa Espresso";
	}

	@Override
	public double calculateCost() {
		return 5.2;
	}

}
