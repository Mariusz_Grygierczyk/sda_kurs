package com.sda.drinks;

public class Cream extends DrinkIngredient {

	public Cream(Drink drink) {
		super(drink);
	}

	@Override
	public String getDescription() {
		return drink.getDescription() + " bita smietana";
	}

	@Override
	public double calculateCost() {
		return drink.calculateCost() + 1.5;
	}

}
