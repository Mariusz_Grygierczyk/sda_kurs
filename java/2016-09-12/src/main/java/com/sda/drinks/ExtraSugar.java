package com.sda.drinks;

public class ExtraSugar extends DrinkIngredient {

	public ExtraSugar(Drink drink) {
		super(drink);
	}

	@Override
	public String getDescription() {
		return drink.getDescription() + " dodatkowy cukier";
	}

	@Override
	public double calculateCost() {
		return drink.calculateCost() + 0.3;
	}

}
