package com.sda.drinks;

public class DecoratorDemo {
	public static void main(String[] args){
		Drink drink = new Choclate(new Cream(new Espresso()));
		System.out.println(drink.getDescription() + " " + drink.calculateCost());
		drink = new ExtraSugar(new Choclate(new Latte()));
		System.out.println(drink.getDescription() + " " + drink.calculateCost());
	}
}
