package com.sda.drinks;

public interface Drink {
	public String getDescription();
	public double calculateCost();
}
