package com.sda.shapes;

public class ShapeFactory {
	public Shape CreateShape(String shapeName){
		if (shapeName.equalsIgnoreCase("square")) return new Square();
		else if (shapeName.equalsIgnoreCase("rectangle")) return new Rectangle();
		else if (shapeName.equalsIgnoreCase("circle")) return new CircleAdapter();
		else return null;
	}
}
