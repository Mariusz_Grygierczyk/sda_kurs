package com.sda.shapes;

public interface Shape {
	public void draw();
}
