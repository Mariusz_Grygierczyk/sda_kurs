package com.sda.shapes;

public class FactoryDemo {
	public static void main(String[] args){
		ShapeFactory factory = new ShapeFactory();
		
		Shape square = factory.CreateShape("square");
		square.draw();
		Shape circle = factory.CreateShape("circle");
		circle.draw();
		Shape rectangle = factory.CreateShape("rectangle");
		rectangle.draw();
	}
}
