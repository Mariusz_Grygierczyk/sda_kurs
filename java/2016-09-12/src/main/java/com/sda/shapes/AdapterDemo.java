package com.sda.shapes;

public class AdapterDemo {
	
	public static void main(String[] args){
		Shape square = new Square();
		Shape circle = new CircleAdapter();
		Shape rectangle = new Rectangle();
		
		square.draw();
		circle.draw();
		rectangle.draw();
	}
	
}
