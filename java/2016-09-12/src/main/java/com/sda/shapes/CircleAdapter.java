package com.sda.shapes;

public class CircleAdapter implements Shape {
	private Circle circle;
	
	public CircleAdapter() {
		this.circle = new Circle();
	}
	
	@Override
	public void draw() {
		circle.drawCircle();
	}

}
