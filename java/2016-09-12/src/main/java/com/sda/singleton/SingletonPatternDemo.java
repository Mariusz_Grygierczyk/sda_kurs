package com.sda.singleton;


public class SingletonPatternDemo {

    public static void main(String[] args) {
        SingleObject object = SingleObject.getInstance();
        object.showMessage();
//        showRaceCondition();

    }


    public static void showRaceCondition(){
        Counter counter = new Counter();
        IncrementTask incrementTask = new IncrementTask(counter);
        Thread t1 = new Thread(incrementTask);
        Thread t2 = new Thread(incrementTask);
        Thread t3 = new Thread(incrementTask);
        t1.start();
        t2.start();
        t3.start();
        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Powinno byc 300 a jest: "+counter.getCounter());
    }

    static class IncrementTask implements Runnable {

        private Counter counter;


        public IncrementTask(Counter counter) {
            this.counter = counter;
        }

        @Override
        public void run() {
            for (int i = 0; i < 100; ++i) {
                counter.increment();
                try {
                    Thread.sleep(10l);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class Counter {
        private int counter;

        public Counter() {
            this.counter = 0;
        }


        public void increment() {
            ++counter;
        }

        public int getCounter() {
            return counter;
        }
    }
}
