package com.sda.operations;

public interface OperationStrategy {
	int doOperation(int a, int b); 
}
