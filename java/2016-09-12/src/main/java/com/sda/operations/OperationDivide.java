package com.sda.operations;

public class OperationDivide implements OperationStrategy{
	@Override
	public int doOperation(int a, int b) {
		if (b != 0) return a / b;
		else return 0;
	}
}
