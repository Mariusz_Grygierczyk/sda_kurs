package com.sda.operations;

public class OperationAdd implements OperationStrategy {

	@Override
	public int doOperation(int a, int b) {
		return a + b;
	}

}
