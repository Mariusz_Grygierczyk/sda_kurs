package com.sda.operations;

import java.util.Scanner;

public class DemoStrategy {
	public static void main(String[] Args){
		
	Scanner input = new Scanner(System.in);
	OperationStrategy add = new OperationAdd();
	OperationStrategy substract = new OperationSubstract();
	OperationStrategy multiply = new OperationMultiply();
	OperationStrategy divide = new OperationDivide();

	while(true){
		System.out.println("podaj 2 liczby");
		int a = input.nextInt();
		int b = input.nextInt();
		System.out.println("1)Dodawanie/n"
				+ "2)Odejmowanie/n"
				+ "3)Mnozenie/n"
				+ "4)Dzielenie/n"
				+ "5)Koniec/n");
		switch (input.nextInt()) {
		case 1:
			System.out.println(add.doOperation(a, b));
			break;
		case 2:
			System.out.println(substract.doOperation(a, b));
			break;
		case 3:
			System.out.println(multiply.doOperation(a, b));
			break;
		case 4:
			System.out.println(divide.doOperation(a, b));
			break;
		case 5:
			input.close();
			System.exit(0);
			break;

		default:
			System.out.println("Nie ma takiej opcji");
			break;
		}


	}
}
}
