package day6;

import org.junit.Test;
import org.junit.Assert;
import org.junit.Before;

public class OpenAddressHashTableTest {
    private OpenAddressHashTable<TestKey, String> table;

    @Before
    public void setUP() {
        table = new OpenAddressHashTable<>(10);
    }

    @Test
    public void testInsertGet() {
        TestKey key1 = new TestKey(1, 1);
        TestKey key2 = new TestKey(2, 2);

        table.put(key1, "a");
        table.put(key2, "b");
        Assert.assertEquals("a", table.get(key1));
        Assert.assertEquals("b", table.get(key2));
        table.put(key1, "c");
        Assert.assertEquals("c", table.get(key1));
    }

    @Test
    public void testInsertGetWithCollisions() {
        TestKey key1 = new TestKey(1, 1);
        TestKey key2 = new TestKey(1, 2);
        table.put(key1, "a");
        table.put(key2, "b");
        Assert.assertEquals("a", table.get(key1));
        Assert.assertEquals("b", table.get(key2));
        table.put(key1, "c");
        Assert.assertEquals("c", table.get(key1));
    }


    @Test
    public void testInsertGetWithResize() {
        for (int i = 0; i < 20; i++) {
            table.put(new TestKey(i, i), Integer.toString(i));
        }
        for (int i = 0; i < 20; i++) {
            Assert.assertEquals(Integer.toString(i), table.get(new TestKey(i, i)));
        }
    }

    @Test
    public void testGetNotFound() {
        TestKey key1 = new TestKey(1, 1);
        TestKey key2 = new TestKey(2, 2);
        TestKey key3 = new TestKey(3, 3);
        Assert.assertEquals(null, table.get(key1));
        table.put(key1, "a");
        table.put(key2, "b");
        Assert.assertEquals(null, table.get(key3));
    }

    @Test
    public void testContainsKey() {
        TestKey key1 = new TestKey(1, 1);
        TestKey key2 = new TestKey(2, 2);
        TestKey key3 = new TestKey(3, 3);
        Assert.assertFalse(table.containsKey(key1));
        table.put(key1, "a");
        table.put(key2, "b");
        Assert.assertTrue(table.containsKey(key1));
        Assert.assertTrue(table.containsKey(key2));
        Assert.assertFalse(table.containsKey(key3));
    }

    @Test
    public void testContainsKeyWithCollisions() {
        TestKey key1 = new TestKey(1, 1);
        TestKey key2 = new TestKey(1, 2);
        TestKey key3 = new TestKey(1, 3);
        Assert.assertFalse(table.containsKey(key1));
        table.put(key1, "a");
        table.put(key2, "b");
        Assert.assertTrue(table.containsKey(key1));
        Assert.assertTrue(table.containsKey(key2));
        Assert.assertFalse(table.containsKey(key3));
    }

    @Test
    public void testContainsValue() {
        TestKey key1 = new TestKey(1, 1);
        TestKey key2 = new TestKey(2, 2);
        Assert.assertFalse(table.containsValue("a"));
        table.put(key1, "a");
        table.put(key2, "b");
        Assert.assertTrue(table.containsValue("a"));
        Assert.assertTrue(table.containsValue("b"));
        Assert.assertFalse(table.containsValue("c"));
    }

    @Test
    public void testContainsValueWithCollisions() {
        TestKey key1 = new TestKey(1, 1);
        TestKey key2 = new TestKey(1, 2);
        Assert.assertFalse(table.containsValue("a"));
        table.put(key1, "a");
        table.put(key2, "b");
        Assert.assertTrue(table.containsValue("a"));
        Assert.assertTrue(table.containsValue("b"));
        Assert.assertFalse(table.containsValue("c"));
    }

}
