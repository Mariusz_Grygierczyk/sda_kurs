package day6;

import java.util.ArrayList;

public class TestKey {
    private int hash;
    private Integer value;

    TestKey(int hash, int value) {
        this.hash = hash;
        this.value = value;
    }
    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(TestKey.class)) {
            TestKey other = (TestKey) obj;
            return value.equals(other.value);
        } else return false;
    }
}