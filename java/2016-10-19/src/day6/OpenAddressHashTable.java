package day6;


import java.util.Arrays;

public class OpenAddressHashTable<K, V> {


    private class Pair<K, V> {
        private K key;
        private V value;

        private Pair(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private static final int DEFAULT_INITIAL_SIZE = 255;

    private Pair<K, V>[] storage;

    public OpenAddressHashTable(int initialSize) {
        storage = new Pair[initialSize];
    }

    public OpenAddressHashTable() {
        storage = new Pair[DEFAULT_INITIAL_SIZE];
    }

    public void put(K key, V value) {
        int index = key.hashCode() % storage.length;
        boolean inserted = false;
        while (!inserted) {
            if (storage[index] == null) {
                storage[index] = new Pair<>(key, value);
                inserted = true;
            } else if (storage[index].key.equals(key)) {
                storage[index].value = value;
                inserted = true;
            } else {
                index++;
                if (index >= storage.length) {
                    resize(storage.length * 2);
                }
            }
        }
    }

    public V get(K key) {
        int index = key.hashCode() % storage.length;
        while (true) {
            if (storage[index] == null) {
                return null;
            } else if (storage[index].key.equals(key)) {
                return storage[index].value;
            } else {
                index++;
                if (index >= storage.length) {
                    return null;
                }
            }
        }
    }

    private void resize(int newCapacity) {
        Pair<K, V>[] oldStorage = storage;
        storage = new Pair[newCapacity];
        for (Pair<K, V> pair : oldStorage) put(pair.key, pair.value);
    }

    public boolean containsKey(K key) {
        int index = key.hashCode() % storage.length;
        while (true) {
            if (storage[index] == null) {
                return false;
            } else if (storage[index].key.equals(key)) {
                return true;
            } else {
                index++;
                if (index >= storage.length) {
                    return false;
                }
            }
        }
    }

    public boolean containsValue(V value) {
        for (Pair<K, V> pair : storage) {
            if (pair != null) {
                if (pair.value.equals(value)) return true;
            }
        }
        return false;
    }

    public static void main(String[] args){
        TestKey key1 = new TestKey(1, 1);
        TestKey key2 = new TestKey(2, 2);
        TestKey key3 = new TestKey(1, 1);

        System.out.println(key1.hashCode());
        System.out.println(key2.hashCode());
        System.out.println(key1.equals(key2));
        System.out.println(key1.equals(key3));
    }
}
