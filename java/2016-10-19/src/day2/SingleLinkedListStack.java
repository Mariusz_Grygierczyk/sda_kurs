package day2;


import java.util.EmptyStackException;

public class SingleLinkedListStack<T> implements Stack<T> {

    private SingleLinkedList<T> storage;

    public SingleLinkedListStack(){
        this.storage = new SingleLinkedList<>();
    }

    public SingleLinkedListStack(SingleLinkedList list){
        this.storage = list;
    }

    @Override
    public void push(T element) {
        storage.prepend(element);
    }

    @Override
    public T peek() {
        if (storage.isEmpty()) throw new EmptyStackException();
        return storage.get(0);
    }

    @Override
    public T pop() {
        if (storage.isEmpty()) throw new EmptyStackException();
        T result = storage.get(0);
        storage.deleteFirst();
        return result;
    }

    @Override
    public boolean isEmpty() {
        return storage.isEmpty();
    }
}
