package day2;

import day1.DynamicArray;
import day2.Stack;


public class DynamicArrayStack<T> implements Stack<T> {
    private DynamicArray<T> storage;

    public DynamicArrayStack(){
        this.storage = new DynamicArray<T>();
    }

    public DynamicArrayStack(DynamicArray array){
        storage = array;
    }

    @Override
    public void push(T element) {
        storage.append(element);
    }

    @Override
    public T peek() {
        return storage.get(storage.length() - 1);
    }

    @Override
    public T pop() {
        T result = storage.get(storage.length() - 1);
        storage.delete(storage.length() - 1);
        return result;
    }

    @Override
    public boolean isEmpty() {
        return storage.isEmpty();
    }
}
