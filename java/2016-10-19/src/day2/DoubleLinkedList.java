package day2;


import java.util.function.Consumer;

public class DoubleLinkedList<T> {

    private class Node {
        private T data;
        private Node next;
        private Node previous;

        private Node(T data) {
            this.data = data;
            this.next = null;
            this.previous = null;
        }
    }

    private Node first = null;
    private Node last = null;
    private int length = 0;

    public boolean isEmpty() {
        return first == null;
    }

    public int length() {
        return length;
    }

    private Node findIndex(int index) {
        int counter;
        Node result;
        if (index <= length / 2) {
            counter = 0;
            result = first;
            while (counter < index) {
                result = result.next;
                counter++;
            }
        } else {
            counter = length - 1;
            result = last;
            while (counter > index) {
                result = result.next;
                counter--;
            }
        }
        return result;
    }

    public void prepend(T element) {
        Node newNode = new Node(element);
        if (isEmpty()) {
            first = newNode;
            last = newNode;
        } else {
            Node nextNode = first;
            first = newNode;
            newNode.next = nextNode;
            nextNode.previous = newNode;
        }
        length++;
    }

    public void append(T element) {
        Node newNode = new Node(element);
        if (isEmpty()) {
            first = newNode;
            last = newNode;
        } else {
            Node previousNode = last;
            last = newNode;
            newNode.previous = previousNode;
            previousNode.next = newNode;
        }
        length++;
    }

    public void insert(int index, T element) {
        if (index > length() || index < 0) throw new IllegalArgumentException();
        else if (index == 0) prepend(element);
        else if (index == length()) append(element);
        else {
            Node previousNode = findIndex(index - 1);
            Node nextNode = previousNode.next;
            Node newNode = new Node(element);
            previousNode.next = newNode;
            nextNode.previous = newNode;
            newNode.previous = previousNode;
            newNode.next = nextNode;
            length++;
        }
    }

    public T get(int index) {
        if (index >= length() || index < 0) throw new IllegalArgumentException();
        else return findIndex(index).data;
    }

    public void deleteFirst() {
        if (!isEmpty()) {
            Node nextNode = first.next;
            if (nextNode != null) nextNode.previous = null;
            first = nextNode;
            length--;
        }

    }

    public void deleteLast() {
        if (!isEmpty()) {
            Node previousNode = last.previous;
            if (previousNode != null) previousNode.next = null;
            last = previousNode;
            length--;
        }
    }

    public void delete(int index) {
        if (index >= length() || index < 0) throw new IllegalArgumentException();
        else if (index == 0) deleteFirst();
        else if (index == length() - 1) deleteLast();
        else {
            Node deletedNode = findIndex(index);
            Node previousNode = deletedNode.previous;
            Node nextNode = deletedNode.next;
            nextNode.previous = previousNode;
            previousNode.next = nextNode;
            length--;
        }
    }


    public DoubleLinkedList reverse() {
        DoubleLinkedList<T> reversedList = new DoubleLinkedList<>();
        forEach(reversedList::prepend);
        return reversedList;
    }

    public void forEach(Consumer<T> function) {
        Node node = first;
        while (node != null) {
            function.accept(node.data);
            node = node.next;
        }
    }


    public static void main(String[] args) {
        DoubleLinkedList<Integer> list = new DoubleLinkedList<>();
        list.append(1);
        list.append(2);
        list.append(3);
        list.forEach(System.out::println);
    }

}
