package day1;

public interface Stack<T> {
    void push(T element);
    T pop();
}
