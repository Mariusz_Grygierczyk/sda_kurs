package day1;

/**
 * Created by RENT on 2016-10-20.
 */
public class SingleLinkedListStack<T> implements Stack<T> {

    private SingleLinkedList<T> storage = new SingleLinkedList<T>();

    @Override
    public void push(T element) {
        storage.prepend(element);
    }

    @Override
    public T pop() {
        T result = storage.get(0);
        storage.deleteFirst();
        return result;
    }
}
