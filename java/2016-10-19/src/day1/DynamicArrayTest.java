package day1;

import day1.DynamicArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class DynamicArrayTest {
    private DynamicArray<Integer> array;

    @Before
    public void setUp() {
        array = new DynamicArray<Integer>(4);
    }

    @Test
    public void testAppend(){
        array.append(1);
        array.append(2);
        array.append(3);
        array.append(4);
        array.append(5);

        Assert.assertEquals(1,(int)array.get(0));
        Assert.assertEquals(2,(int)array.get(1));
        Assert.assertEquals(3,(int)array.get(2));
        Assert.assertEquals(4,(int)array.get(3));
        Assert.assertEquals(5,(int)array.get(4));
        Assert.assertEquals(5,array.length());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetInvalidArguments1(){
        array.append(1);
        array.append(2);
        array.append(3);
        array.append(4);
        array.append(5);

        array.get(10);
    }
    @Test(expected = IllegalArgumentException.class)
    public void testGetInvalidArguments2(){
        array.append(1);
        array.append(2);
        array.append(3);
        array.append(4);
        array.append(5);

        array.get(-10);
    }

    @Test
    public void testPrepend(){
        array.append(1);
        array.append(2);
        array.append(3);
        array.append(4);
        array.append(5);

        array.prepend(-3);
        Assert.assertEquals(-3,(int)array.get(0));
        Assert.assertEquals(1,(int)array.get(1));
        Assert.assertEquals(5,(int)array.get(5));
        Assert.assertEquals(6,array.length());
        array.prepend(-2);
        Assert.assertEquals(-2,(int)array.get(0));
        Assert.assertEquals(-3,(int)array.get(1));
        Assert.assertEquals(1,(int)array.get(2));
        Assert.assertEquals(5,(int)array.get(6));
        Assert.assertEquals(7,array.length());
    }

    @Test
    public void testInsert(){
        array.append(1);
        array.append(2);
        array.append(3);
        array.append(4);
        array.append(5);

        array.insert(2, 0);
        Assert.assertEquals(0, (int)array.get(2));
        Assert.assertEquals(3, (int)array.get(3));
        Assert.assertEquals(6, array.length());
        array.insert(2, 0);
        Assert.assertEquals(0, (int)array.get(2));
        Assert.assertEquals(0, (int)array.get(3));
        Assert.assertEquals(3, (int)array.get(4));
        Assert.assertEquals(7, array.length());
    }

    @Test
    public void testDelete(){
        array.append(1);
        array.append(2);
        array.append(3);
        array.append(4);
        array.append(5);

        array.delete(2);
        Assert.assertEquals(4, (int)array.get(2));
        Assert.assertEquals(4, array.length());
        array.delete(2);
        Assert.assertEquals(5, (int)array.get(2));
        Assert.assertEquals(3, array.length());

    }



}
