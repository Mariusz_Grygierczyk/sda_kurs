package day1;

public class DynamicArray<T> {
    static final int DEFAULT_INITIAL_CAPACITY = 4;

    private T[] storage;
    private int firstEmptyIndex;

    public DynamicArray(int initialCapacity) {
        this.storage = (T[]) new Object[initialCapacity];
        this.firstEmptyIndex = 0;
    }

    public DynamicArray() {
        this.storage = (T[]) new Object[DEFAULT_INITIAL_CAPACITY];
        this.firstEmptyIndex = 0;
    }

    private void changeArraySize(int size){
        T[] temp = (T[]) new Object[size];
        System.arraycopy(storage,0,temp,0, length());
        storage = temp;
    }

    private void shiftRight (int first, int last){
        for (int i = last; i >= first; i--){
            if (i > 0) storage[i] = storage[i-1];
            else storage[i] = null;
        }
    }

    private void shiftLeft (int first, int last){
        for (int i = first; i <= last; i++){
            storage[i] = storage[i+1];
        }
    }

    private boolean isFull() {
        return firstEmptyIndex >= storage.length;
    }

    public void append(T element) {
        if (isFull()) {
            changeArraySize(storage.length * 2);
        }
        storage[firstEmptyIndex] = element;
        firstEmptyIndex++;
    }

    public void prepend(T element) {
        insert(0,element);
    }

    public void insert(int index, T element) {
        if (index > firstEmptyIndex || index < 0) throw new IllegalArgumentException();

        if (isFull()) {
            changeArraySize(storage.length * 2);
        }
        shiftRight(index, firstEmptyIndex);
        storage[index] = element;
        firstEmptyIndex++;
    }

    public void delete(int index) {
        if (index >= firstEmptyIndex || index < 0) throw new IllegalArgumentException();

        if (index == firstEmptyIndex) {
            firstEmptyIndex--;
        } else {
            shiftLeft(index, firstEmptyIndex - 1);
            firstEmptyIndex--;
        }
        if(firstEmptyIndex < (storage.length * 3) / 4){
            changeArraySize((int)Math.ceil((storage.length * 3) / 4));
        }
    }

    public T get(int index) {
        if (index >= firstEmptyIndex || index < 0) throw new IllegalArgumentException();

        else return storage[index];
    }

    public int length() {
        return firstEmptyIndex;
    }


    public static void main(String[] args){
        DynamicArray<Integer> arr = new DynamicArray<Integer>(4);

        arr.append(1);
        arr.append(2);
        arr.append(3);
        arr.append(4);

        arr.append(5);

        for (int i = 0; i < arr.length(); i++){
            System.out.print(arr.get(i));
        }
        System.out.println();

        arr.prepend(0);
        arr.insert(3, 0);
        for (int i = 0; i < arr.length(); i++){
            System.out.print(arr.get(i));
        }
        System.out.println();
        arr.insert(arr.length(), 0);
        for (int i = 0; i < arr.length(); i++){
            System.out.print(arr.get(i));
        }
        System.out.println();
        arr.delete(3);
        for (int i = 0; i < arr.length(); i++){
            System.out.print(arr.get(i));
        }
    }

    public boolean isEmpty() {
        return firstEmptyIndex == 0;
    }
}

