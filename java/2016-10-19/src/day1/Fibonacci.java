package day1;
import java.util.HashMap;
import java.util.Map;

public class Fibonacci {

    private static Map<Integer, Long> cache = new HashMap<>();



    public static long rFibonacci (int n){
        if(n == 0) return 0;
        else if(n == 1 || n == 2) return 1;
        else return rFibonacci(n-1) + rFibonacci(n-2);
    }

    public static long cachedRFibonacci (int n){
        long result;

        if(cache.containsKey(n)) return cache.get(n);

        if(n == 0) result = 0;
        else if(n == 1) result = 1;
        else result = cachedRFibonacci(n-1) + cachedRFibonacci(n-2);
        cache.put(n, result);
        return result;
    }

    public static long iFibonacci(int n){
        if(n == 0) return 0;

        long first = 0;
        long second = 1;

        for (int i = 1; i < n; i++){
            long temp = first + second;
            first = second;
            second = temp;
        }
        return second;
    }

    public static void main(String[] args){
        long timeBefore;
        long timeAfter;


        timeBefore = System.currentTimeMillis();
        System.out.print("iteracyjnie: " + iFibonacci(50)+ " ");
        timeAfter = System.currentTimeMillis();
        System.out.println("czas: "+ (timeAfter - timeBefore));

        timeBefore = System.currentTimeMillis();
        System.out.print("cached: " + cachedRFibonacci(50)+ " ");
        timeAfter = System.currentTimeMillis();
        System.out.println("czas: "+ (timeAfter - timeBefore));

        timeBefore = System.currentTimeMillis();
        System.out.print("rekurencyjnie: " + rFibonacci(50) + " ");
        timeAfter = System.currentTimeMillis();
        System.out.println("czas: "+ (timeAfter - timeBefore));
    }

}
