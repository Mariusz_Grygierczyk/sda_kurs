package day1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class SingleLinkedListTest {

    private SingleLinkedList<Integer> list;

    @Before
    public void setUp(){
        list = new SingleLinkedList<>();
    }

    private void checkListContents(SingleLinkedList list, int... expected ){
        Assert.assertEquals(expected.length, list.length());
        for (int i = 0; i < list.length(); i++){
            Assert.assertEquals(expected[i], list.get(i));
        }
    }

    @Test
    public void testAppend(){
        list.append(1);
        list.append(2);
        list.append(3);
        checkListContents(list, 1,2,3);
    }

    @Test
    public void testPrepend(){
        list.prepend(1);
        list.prepend(2);
        list.prepend(3);
        checkListContents(list,3,2,1);
    }

    @Test
    public void testInsert(){
        list.append(1);
        list.append(2);
        list.append(3);
        list.insert(1, 0);
        checkListContents(list, 1,0,2,3);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testInsertInvalidArgument1(){
        list.insert(10, 0);
    }
    @Test (expected = IllegalArgumentException.class)
    public void testInsertInvalidArgument2(){
        list.insert(-10, 0);
    }

    @Test
    public void testDeleteLast(){
        list.append(1);
        list.append(2);
        list.append(3);
        list.deleteLast();
        checkListContents(list, 1,2);
    }

    @Test
    public void testDeleteFirst(){
        list.append(1);
        list.append(2);
        list.append(3);
        list.deleteFirst();
        checkListContents(list, 2,3);
    }

    @Test
    public void testDelete(){
        list.append(1);
        list.append(2);
        list.append(3);
        list.delete(1);
        checkListContents(list, 1,3);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testDeleteInvalidArgument1(){
        list.delete(-2);
    }
    @Test (expected = IllegalArgumentException.class)
    public void testDeleteInvalidArgument2(){
        list.delete(10);
    }

    @Test
    public void testReverse(){
        Assert.assertTrue(list.reverse().isEmpty());
        list.append(1);
        list.append(2);
        list.append(3);
        checkListContents(list,1,2,3);
        checkListContents(list.reverse(),3,2,1);
    }


}
