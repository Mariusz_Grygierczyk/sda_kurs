package day1;


import java.util.function.Consumer;

public class SingleLinkedList<T> {

    private class Node {
        private T data;
        private Node next;


        private Node(T data) {
            this.data = data;
        }
    }

    private Node first = null;
    private int length = 0;


    public boolean isEmpty() {
        return first == null;
    }

    private Node findLast() {

        Node result = first;
        while (result.next != null) result = result.next;
        return result;
    }

    private Node findIndex(int index) {
        if (index == 0) return first;
        else {
            int counter = 0;
            Node result = first;
            while (counter < index) {
                result = result.next;
                counter++;
            }
            return result;
        }
    }

    public int length() {
        return length;
    }

    public void prepend(T element) {
        if (isEmpty()) first = new Node(element);
        else {
            Node nextNode = first;
            first = new Node(element);
            first.next = nextNode;
        }
        length++;
    }

    public void append(T element) {
        if (isEmpty()) first = new Node(element);
        else findLast().next = new Node(element);
        length++;
    }

    public void insert(int index, T element) {
        if (index > length() || index < 0) throw new IllegalArgumentException();
        else if (index == 0) prepend(element);
        else if (index == length()) append(element);
        else {
            Node previousNode = findIndex(index - 1);
            Node nextNode = previousNode.next;
            Node newNode = new Node(element);
            previousNode.next = newNode;
            newNode.next = nextNode;
            length++;
        }
    }

    public T get(int index) {
        if (index >= length() || index < 0) throw new IllegalArgumentException();
        else return findIndex(index).data;
    }

    public void deleteFirst() {
        if (!isEmpty()) {
            first = first.next;
            length--;
        }

    }

    public void deleteLast() {
        if (!isEmpty()) {
            Node previousNode = findIndex(length() - 2);
            previousNode.next = null;
            length--;
        }
    }

    public void delete(int index) {
        if (index >= length() || index < 0) throw new IllegalArgumentException();
        else if (index == 0) deleteFirst();
        else if (index == length() - 1) deleteLast();
        else {
            Node previousNode = findIndex(index - 1);
            previousNode.next = previousNode.next.next;
            length--;
        }

    }

    public SingleLinkedList reverse() {
        SingleLinkedList<T> reversedList = new SingleLinkedList<>();
        forEach(reversedList::prepend);
        return reversedList;
    }

    public void forEach(Consumer<T> function) {
        Node node = first;
        while (node != null) {
            function.accept(node.data);
            node = node.next;
        }
    }


    public static void main(String[] args) {


    }


}
