package day7;



import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.EmptyStackException;

public class ArrayHeap<T extends Comparable<T>> {
    private final static int DEFAULT_INITIAL_CAPACITY = 4;

    private T[] storage;

    public ArrayHeap(){
        this.storage = (T[])Array.newInstance(Comparable.class,DEFAULT_INITIAL_CAPACITY );
    }

    public ArrayHeap(int initialCapacity){
        this.storage = (T[])Array.newInstance(Comparable.class,initialCapacity );
    }

    private void shiftUp(int index){
        int parent = (index-1) / 2;
        while (index > 0 && storage[index].compareTo(storage[parent]) < 0){
            swap(parent, index);
            index = parent;
            parent = (index-1) / 2;
        }
    }

    private void shiftDown(int index){
        int leftChild = leftChild(index);
        int rightChild = rightChild(index);
        int smallest = index;
        if (leftChild < storage.length && storage[leftChild] != null && storage[leftChild].compareTo(storage[index]) < 0) {
            smallest = leftChild;
        }
        if (rightChild < storage.length &&storage[rightChild] != null && storage[rightChild].compareTo(storage[smallest]) < 0) {
            smallest = rightChild;
        }
        if (smallest != index) {
            swap(smallest, index);
            shiftDown(smallest);
        }
    }

    private int leftChild(int index){
        return (index * 2) + 1;
    }
    private int rightChild(int index){
        return (index * 2) + 2;
    }


    private void swap(int firstIndex, int secondIndex){
        T temp = storage[firstIndex];
        storage[firstIndex] = storage[secondIndex];
        storage[secondIndex] = temp;
    }

    private void resize(int size){
        storage = Arrays.copyOf(storage, size);
    }

    public void push(T element){
        int index = 0;
        while (index < storage.length && storage[index] != null) index++;
        if (index >= storage.length) resize(storage.length * 2);
        storage[index] = element;
        shiftUp(index);
    }

    public T peek(){
        if (storage[0] == null) throw new EmptyStackException();
        return storage[0];
    }

    public T pop(){
        if (storage[0] == null) throw new EmptyStackException();
        T result = storage[0];
        int lastIndex = 0;
        while (lastIndex < storage.length - 1 && storage[lastIndex + 1] != null) lastIndex++;
        storage[0] = storage[lastIndex];
        storage[lastIndex] = null;
        shiftDown(0);
        if(lastIndex < (storage.length * 3) / 4){
            resize((int)Math.ceil((storage.length * 3) / 4));
        }
        return result;
    }

    public void print(){
        for (T element : storage) System.out.print(element + " ");
        System.out.println();
    }

    public boolean isEmpty(){
        return storage[0] == null;
    }

    public static void main(String[] args){
        ArrayHeap<Integer> heap = new ArrayHeap<>();
        heap.push(44);
        heap.push(22);
        heap.push(1);
        heap.print();
        heap.pop();
        heap.print();
    }

}
