package day7;

import org.junit.Test;
import org.junit.Assert;
import org.junit.Before;

import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.Random;

public class ArrayHeapTest {
    private ArrayHeap<Integer> heap;

    @Before
    public void setUp(){
        heap = new ArrayHeap<>(10);
    }

    @Test
    public void testPushPop(){
        Assert.assertTrue(heap.isEmpty());
        heap.push(44);
        heap.push(55);
        heap.push(1);
        Assert.assertEquals(1, (int)heap.peek());
        Assert.assertEquals(1, (int)heap.pop());
        Assert.assertEquals(44, (int)heap.peek());
        Assert.assertEquals(44, (int)heap.pop());
        Assert.assertEquals(55, (int)heap.peek());
        Assert.assertEquals(55, (int)heap.pop());
    }

    @Test
    public void testPushPopWithResizing(){
        Assert.assertTrue(heap.isEmpty());
        Random random = new Random();
        int[] array = new int[40];

        for (int i = 0; i < 40; i++){
            array[i] = random.nextInt();
            heap.push(array[i]);
        }
        Arrays.sort(array);
        for (int i = 0; i < 40; i++){
            Assert.assertEquals(array[i], (int)heap.peek());
            Assert.assertEquals(array[i], (int)heap.pop());

        }
    }

    @Test(expected = EmptyStackException.class)
    public void testPopIfEmpty(){
        heap.pop();
    }
    @Test(expected = EmptyStackException.class)
    public void testPeekIfEmpty(){
        heap.peek();
    }
}
