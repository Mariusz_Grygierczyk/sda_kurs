package day5;

import org.junit.Test;
import org.junit.Assert;
import org.junit.Before;

public class ChainedHashTableTest {
    private ChainedHashTable<Integer, String> table;

    @Before
    public void setUP(){
        table = new ChainedHashTable<>();
    }

    @Test
    public void testPutGet(){
        table.put(1,"a");
        table.put(2,"b");
        Assert.assertEquals("a",table.get(1));
        Assert.assertEquals("b",table.get(2));
        table.put(1,"c");
        Assert.assertEquals("c",table.get(1));
    }


    @Test
    public void testGetNotFound(){
        Assert.assertEquals(null, table.get(1));
        table.put(1,"a");
        table.put(2,"b");
        Assert.assertEquals(null, table.get(3));
    }

    @Test
    public void testContainsKey(){
        Assert.assertFalse(table.containsKey(1));
        table.put(1,"a");
        table.put(2,"b");
        Assert.assertTrue(table.containsKey(1));
        Assert.assertTrue(table.containsKey(2));
        Assert.assertFalse(table.containsKey(3));
    }

    @Test
    public void testContainsValue(){
        Assert.assertFalse(table.containsValue("a"));
        table.put(1,"a");
        table.put(2,"b");
        Assert.assertTrue(table.containsValue("a"));
        Assert.assertTrue(table.containsValue("b"));
        Assert.assertFalse(table.containsValue("c"));
    }

}
