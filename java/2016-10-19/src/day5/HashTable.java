package day5;


import java.util.ArrayList;
import java.util.List;

public class HashTable<K, V> {
    private class Pair<K, V>{
        private K key;
        private V value;

        private Pair(K key, V value){
            this.key = key;
            this.value = value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Pair<?, ?> pair = (Pair<?, ?>) o;
            return key.equals(pair.key);
        }
    }

    private static final int DEFAULT_NUMBER_OF_BUCKETS = 16;

    private List<Pair<K, V>>[] storage;

    public HashTable(int numberOfBuckets){
        storage = new List[numberOfBuckets];
        for (int i = 0; i < storage.length; i++) storage[i] = new ArrayList<>();
    }

    public HashTable(){
        storage = new List[DEFAULT_NUMBER_OF_BUCKETS];
        for (int i = 0; i < storage.length; i++) storage[i] = new ArrayList<>();
    }

    public void insert(K key,V value){
        Pair<K, V> newPair = new Pair<>(key, value);
        if (!storage[key.hashCode()].contains(newPair)) storage[key.hashCode()].add(newPair);
        else {
            for(Pair<K, V> p : storage[key.hashCode()]){
                if (p.key.equals(key)) p.value = value;
            }
        }
    }

    public V get(K key){
        for(Pair<K, V> p : storage[key.hashCode()]){
            if (p.key.equals(key)) return p.value;
        }
        throw new IllegalStateException("not found");
    }

    public static void main(String[] args){
        HashTable<Integer, String> table = new HashTable<>();
        table.insert(1,"kot");
        System.out.println(table.get(1));
        table.insert(1,"pies");
        System.out.println(table.get(1));
    }

}
