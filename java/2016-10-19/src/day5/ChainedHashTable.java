package day5;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChainedHashTable<K, V> {
    private class Pair<K, V> {
        private K key;
        private V value;

        private Pair(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private static final int DEFAULT_NUMBER_OF_BUCKETS = 16;

    private List<Pair<K, V>>[] storage;

    public ChainedHashTable(int numberOfBuckets) {
        storage = new List[numberOfBuckets];
        for (int i = 0; i < storage.length; i++) storage[i] = new ArrayList<>();
    }

    public ChainedHashTable() {
        storage = new List[DEFAULT_NUMBER_OF_BUCKETS];
        for (int i = 0; i < storage.length; i++) storage[i] = new ArrayList<>();
    }

    public void put(K key, V value) {
        boolean alreadyExists = false;
        for (Pair<K, V> p : storage[key.hashCode()]) {
            if (p.key.equals(key)) {
                p.value = value;
                alreadyExists = true;
            }
        }
        if (!alreadyExists) storage[key.hashCode()].add(new Pair<>(key, value));
    }

    public V get(K key) {
        for (Pair<K, V> p : storage[key.hashCode()]) {
            if (p.key.equals(key)) return p.value;
        }
        return null;
    }

    public boolean containsKey(K key){
        for (Pair<K, V> p : storage[key.hashCode()]) {
            if (p.key.equals(key)) {
                return true;
            }
        }
        return false;
    }
    public boolean containsValue(V value){
        for (List<Pair<K, V>> list : storage){
            for (Pair<K, V> pair : list){
                if (pair.value.equals(value)) return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        ChainedHashTable<Integer, String> table = new ChainedHashTable<>();
        table.put(1, "kot");
        System.out.println(table.get(1));
        table.put(1, "pies");
        System.out.println(table.get(1));
        table.put(2, "owca");
        System.out.println(table.get(2));
        System.out.println(table.containsKey(1));
        System.out.println(table.containsKey(5));
        System.out.println(table.containsValue("pies"));
        System.out.println(table.containsValue("aaa"));

    }

}
