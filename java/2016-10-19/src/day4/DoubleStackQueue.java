package day4;


import day2.SingleLinkedListStack;
import day2.Stack;

public class DoubleStackQueue<T> implements Queue<T>{
    private Stack<T> oldOnTopStack = new SingleLinkedListStack<>();
    private Stack<T> newOnTopStack = new SingleLinkedListStack<>();


    @Override
    public void enqueue(T element) {
        newOnTopStack.push(element);
    }

    @Override
    public T dequeue() {
        if (isEmpty()) throw new RuntimeException("queue is empty");
        if (oldOnTopStack.isEmpty()){
            while (!newOnTopStack.isEmpty()){
                oldOnTopStack.push(newOnTopStack.pop());
            }
        }
        return oldOnTopStack.pop();
    }

    @Override
    public boolean isEmpty() {
        return oldOnTopStack.isEmpty()&& newOnTopStack.isEmpty();
    }
}
