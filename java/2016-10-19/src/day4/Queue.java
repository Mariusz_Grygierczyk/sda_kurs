package day4;


public interface Queue<T> {
    void enqueue(T element);
    T dequeue();
    boolean isEmpty();
}
