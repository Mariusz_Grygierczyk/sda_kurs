package day4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DoubleLinkedListQueueTest {
    private DoubleLinkedListQueue<Integer> queue;

    @Before
    public void setUp(){
        queue = new DoubleLinkedListQueue<>();
    }


    @Test
    public void testExceptionOnEmptyQueue(){
        try {
            queue.dequeue();
            Assert.fail("exception not thrown");
        } catch (RuntimeException e){
            if (!e.getMessage().equals( "queue is empty")) Assert.fail("wrong exception thrown");
        }
    }

    @Test
    public void testIfWorking(){
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        Assert.assertEquals(1, (int)queue.dequeue());
        queue.enqueue(4);
        Assert.assertEquals(2, (int)queue.dequeue());
        Assert.assertEquals(3, (int)queue.dequeue());
        Assert.assertEquals(4, (int)queue.dequeue());
    }
}
