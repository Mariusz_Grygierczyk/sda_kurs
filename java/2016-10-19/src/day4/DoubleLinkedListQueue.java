package day4;

import day2.DoubleLinkedList;

/**
 * Created by RENT on 2016-10-25.
 */
public class DoubleLinkedListQueue<T> implements Queue<T> {
    private DoubleLinkedList<T> storage;

    public DoubleLinkedListQueue(DoubleLinkedList<T> list){
        this.storage = list;
    }

    public DoubleLinkedListQueue(){
        this.storage = new DoubleLinkedList<T>();
    }

    @Override
    public void enqueue(T element) {
        storage.append(element);
    }

    @Override
    public T dequeue() {
        if (isEmpty()) throw new RuntimeException("queue is empty");
        else {
            T result = storage.get(0);
            storage.deleteFirst();
            return result;
        }
    }

    @Override
    public boolean isEmpty() {
        return storage.isEmpty();
    }
}
