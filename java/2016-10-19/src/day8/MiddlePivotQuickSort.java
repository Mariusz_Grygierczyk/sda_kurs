package day8;

import java.util.List;

/**
 * Created by RENT on 2016-11-03.
 */
public class MiddlePivotQuickSort<T extends Comparable<T>> extends QuickSort<T>{
    @Override
    protected int pivot(List<T> input) {
        int midIndex = input.size()/2;
        int result = 0;
        if (input.get(0).compareTo(input.get(midIndex)) < 0 && input.get(midIndex).compareTo(input.get(input.size() - 1)) < 0) result = midIndex;
        if (input.get(0).compareTo(input.get(midIndex)) > 0 && input.get(midIndex).compareTo(input.get(input.size() - 1)) < 0) result = input.size() - 1;
        return result;
    }
}
