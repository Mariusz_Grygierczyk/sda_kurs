package day8;


import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FixedPivotQuickSortTest {
    private QuickSort<Integer> sort = new FixedPivotQuickSort<>();

    private void checkListContents(List list, int... expected ){
        Assert.assertEquals(expected.length, list.size());
        for (int i = 0; i < list.size(); i++){
            Assert.assertEquals(expected[i], list.get(i));
        }
    }

    @Test
    public void smallListTest(){
        List<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(5);
        list.add(1);
        list.add(10);
        list.add(4);
        list = sort.sort(list);
        checkListContents(list, 1,3,4,5,10);
    }

    @Test
    public void bigListTest(){
        Random random = new Random();
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();

        for (int i = 0; i < 10000; i++) {
            int randomInt = random.nextInt();
            list1.add(randomInt);
            list2.add(randomInt);
        }
        list1.sort(null);
        list2 = sort.sort(list2);
        for (int i = 0; i < 10000; i++) {
            Assert.assertEquals(list1.get(i), list2.get(i));
        }

    }
}