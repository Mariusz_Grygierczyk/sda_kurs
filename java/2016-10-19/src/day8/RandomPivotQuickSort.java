package day8;

import java.util.List;
import java.util.Random;

public class RandomPivotQuickSort<T extends Comparable<T>> extends QuickSort<T>{

    @Override
    protected int pivot(List<T> input) {
        Random random = new Random();
        return random.nextInt(input.size() - 1);
    }

}
