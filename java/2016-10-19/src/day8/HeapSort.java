package day8;


import java.util.Random;

public class HeapSort {
    private static int[] arr;
    private static int lastIndex;


    public static void buildHeap(){
        lastIndex =arr.length-1;
        for(int i = lastIndex /2; i>=0; i--){
            shiftDown(i);
        }
    }

    public static void shiftDown(int i){
        int left = 2 * i;
        int right = 2 * i + 1;
        int largest;
        if(left <= lastIndex && arr[left] > arr[i]){
            largest = left;
        }
        else{
            largest =i;
        }

        if(right <= lastIndex && arr[right] > arr[largest]){
            largest = right;
        }
        if(largest !=i){
            swap(i, largest);
            shiftDown(largest);
        }
    }

    public static void swap(int i, int j){
        int t= arr[i];
        arr[i]= arr[j];
        arr[j]=t;
    }

    public static void sort(int []a0){
        arr =a0;
        buildHeap();

        for(int i = lastIndex; i>0; i--){
            swap(0, i);
            lastIndex = lastIndex -1;
            shiftDown(0);
        }
    }

    public static void main(String[] args){
        int[] arr = new int[10];
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            arr[i] = random.nextInt(15);
        }
        sort(arr);
        for (int i: arr) System.out.print(i + " ");
    }
}
