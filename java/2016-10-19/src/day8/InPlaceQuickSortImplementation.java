package day8;

import java.util.List;


public class InPlaceQuickSortImplementation<T extends Comparable<T>> extends InPlaceQuickSort<T>{
    @Override
    protected int split(List<T> input, int startIndex, int endIndex) {
        int pivotIndex = startIndex;
        for (int i = startIndex + 1; i <= endIndex; i++){
            if (input.get(i).compareTo(input.get(pivotIndex)) < 0) {
                swap(input, pivotIndex, i);
                swap(input, i, pivotIndex + 1);
                pivotIndex++;
            }
        }
        return pivotIndex;
    }
}
