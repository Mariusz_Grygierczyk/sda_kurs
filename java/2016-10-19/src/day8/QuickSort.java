package day8;

import java.util.ArrayList;
import java.util.List;

public abstract class QuickSort<T extends Comparable<T>> {
    public List<T> sort(List<T> input){
        if (input.size() <= 1) return input;
        if (input.size() == 2) {
            List<T> result = new ArrayList<T>();
            if (input.get(0).compareTo(input.get(1)) > 0) {
                result.add(input.get(1));
                result.add(input.get(0));
            } else {
                result.add(input.get(0));
                result.add(input.get(1));
            }
            return result;
        }

        int pivotIndex = pivot(input);
        List<T> leftList = new ArrayList<T>();
        List<T> rightList = new ArrayList<T>();
        for (int i = 0; i < input.size(); i++){
            if (i != pivotIndex){
                if (input.get(i).compareTo(input.get(pivotIndex)) < 1){
                    leftList.add(input.get(i));
                } else {
                    rightList.add(input.get(i));
                }
            }
        }
        leftList =  sort(leftList);
        rightList = sort(rightList);

        ArrayList<T> result = new ArrayList<T>();
        leftList.forEach(result::add);
        result.add(input.get(pivotIndex));
        rightList.forEach(result::add);
        return result;
    }

    protected abstract int pivot(List<T> input);
}
