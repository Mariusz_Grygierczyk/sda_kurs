package day8;


import java.util.List;

public abstract class InPlaceQuickSort<T extends Comparable<T>> extends QuickSort<T>{
    public List<T> sort (List<T> input){
        sort(input, 0, input.size() - 1);
        return input;
    }

    private List<T> sort(List<T> input, int startIndex, int endIndex){
        if (startIndex >= endIndex) return input;
        int pivotIndex = split(input, startIndex, endIndex);
        sort(input, startIndex, pivotIndex - 1);
        sort(input, pivotIndex + 1, endIndex);
        return input;
    }

    protected abstract int split(List<T> input, int startIndex, int endIndex);

    protected void swap(List<T> input, int firstIndex, int secondIndex){
        T tmp = input.get(firstIndex);
        input.set(firstIndex, input.get(secondIndex));
        input.set(secondIndex, tmp);
    }

    @Override
    protected int pivot(List<T> input) {
        return 0;
    }
}
