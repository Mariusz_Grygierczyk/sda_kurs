package day3;

import day2.SingleLinkedListStack;
import day2.Stack;

import java.util.function.Consumer;

public class PersistentSingleLinkedList<T> {

    private class Node {
        private final T DATA;
        private final Node NEXT;


        private Node(T data, Node next) {
            this.DATA = data;
            this.NEXT = next;
        }
    }

    private final Node FIRST;
    private final int LENGTH;

    public PersistentSingleLinkedList(){
        this.FIRST = null;
        this.LENGTH = 0;
    }

    private PersistentSingleLinkedList(Node first, int length){
        this.FIRST = first;
        this.LENGTH = length;
    }

    public boolean isEmpty() {
        return FIRST == null;
    }

    public int length() {
        return LENGTH;
    }

    public T get(int index) {
        if (index >= length() || index < 0) throw new IllegalArgumentException();
        else return findIndex(index).DATA;
    }

    public void forEach(Consumer<T> function) {
        Node node = FIRST;
        while (node != null) {
            function.accept(node.DATA);
            node = node.NEXT;
        }
    }

    private Node findIndex(int index) {
        if (index == 0) return FIRST;
        else {
            int counter = 0;
            Node result = FIRST;
            while (counter < index) {
                result = result.NEXT;
                counter++;
            }
            return result;
        }
    }

    public PersistentSingleLinkedList<T> prepend(T element){
        Node newFirstNode = new Node(element, FIRST);
        return new PersistentSingleLinkedList<>(newFirstNode, LENGTH + 1);
    }

    public PersistentSingleLinkedList<T> deleteFirst(){
        if (!isEmpty()){
            return new PersistentSingleLinkedList<>(FIRST.NEXT, LENGTH - 1);
        }
        else return new PersistentSingleLinkedList<>();
    }





    public PersistentSingleLinkedList<T> insert(int index, T element){
        if (index > length() || index < 0) throw new IllegalArgumentException();
        if (index == 0) return prepend(element);

        Stack<T> values = new SingleLinkedListStack<>();
        Node currentNode = FIRST;
        int counter = 0;
        while (currentNode != null && counter < index){
            values.push(currentNode.DATA);
            currentNode = currentNode.NEXT;
            counter++;
        }
        values.push(element);

        while (!values.isEmpty()){
            currentNode = new Node(values.pop(), currentNode);
        }

        return new PersistentSingleLinkedList<>(currentNode, LENGTH + 1);

    }

    public PersistentSingleLinkedList<T> delete(int index){
        if (index >= length() || index < 0) throw new IllegalArgumentException();
        if (index == 0) return deleteFirst();

        Stack<T> values = new SingleLinkedListStack<>();
        Node currentNode = FIRST;
        int counter = 0;
        while (currentNode != null && counter < index){
            values.push(currentNode.DATA);
            currentNode = currentNode.NEXT;
            counter++;
        }
        currentNode = currentNode.NEXT;

        while (!values.isEmpty()){
            currentNode = new Node(values.pop(), currentNode);
        }

        return new PersistentSingleLinkedList<>(currentNode, LENGTH - 1);
    }

    public PersistentSingleLinkedList<T> append(T element){
        return insert(LENGTH, element);
    }

    public PersistentSingleLinkedList<T> deleteLast(){
        return delete(LENGTH - 1);
    }


    public static void main(String[] args){
        PersistentSingleLinkedList<Integer> list = new PersistentSingleLinkedList<>();
        list = list.prepend(6);
        list = list.prepend(5);
        list = list.prepend(4);
        list = list.prepend(3);
        list = list.prepend(2);
        list = list.insert(0,1);
        list.forEach((element) -> System.out.print(element + " "));

    }
}
