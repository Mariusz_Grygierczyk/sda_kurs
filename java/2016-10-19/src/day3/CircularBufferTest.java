package day3;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CircularBufferTest {
    private CircularBuffer<Integer> buffer;

    @Before
    public void setUp(){
        buffer = new CircularBuffer<>(8);
    }

    private void checkBufferContents(CircularBuffer buffer, int... expected ){

        for (int i = 0; i < expected.length; i++){
            Assert.assertEquals(expected[i], buffer.pop());
        }
    }

    @Test
    public void testIfWorking(){
        buffer.push(1);
        buffer.push(2);
        buffer.push(3);
        Assert.assertEquals(1, (int)buffer.pop());
        buffer.push(4);
        checkBufferContents(buffer, 2,3,4);
        for (int i = 0; i < 8; i++) buffer.push(i);
        checkBufferContents(buffer, 0,1,2,3,4,5,6,7);
    }

    @Test
    public void testExceptionOnEmptyBuffer(){
        try {
            buffer.pop();
            Assert.fail("exception not thrown");
        } catch (RuntimeException e){
            if (!e.getMessage().equals("buffer is empty")) Assert.fail("wrong exception thrown");
        }
    }

    @Test
    public void testExceptionOnFullBuffer(){
        try {
            for (int i = 0; i < 9; i++) buffer.push(i);
            Assert.fail("exception not thrown");
        } catch (RuntimeException e){
            if (!e.getMessage().equals("buffer is full")) Assert.fail("wrong exception thrown");
        }
    }

}
