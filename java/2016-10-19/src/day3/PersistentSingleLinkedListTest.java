package day3;

import org.junit.Test;
import org.junit.Assert;
import org.junit.Before;

public class PersistentSingleLinkedListTest {

    private PersistentSingleLinkedList<Integer> list;

    @Before
    public void setUp(){
        list = new PersistentSingleLinkedList<>();
    }

    private void checkListContents(PersistentSingleLinkedList list, int... expected ){
        Assert.assertEquals(expected.length, list.length());
        for (int i = 0; i < list.length(); i++){
            Assert.assertEquals(expected[i], list.get(i));
        }
    }

    @Test
    public void testAppend(){
        list = list.append(1);
        checkListContents(list, 1);
        list = list.append(2);
        checkListContents(list, 1,2);
        list = list.append(3);
        checkListContents(list, 1,2,3);
    }

    @Test
    public void testPrepend(){
        list = list.prepend(1);
        checkListContents(list,1);
        list = list.prepend(2);
        checkListContents(list,2,1);
        list = list.prepend(3);
        checkListContents(list,3,2,1);
    }

    @Test
    public void testInsert(){
        list = list.append(1);
        checkListContents(list, 1);
        list = list.append(2);
        checkListContents(list, 1,2);
        list = list.append(3);
        checkListContents(list, 1,2,3);
        list = list.insert(1, 0);
        checkListContents(list, 1,0,2,3);
        list = list.insert(0, 0);
        checkListContents(list, 0,1,0,2,3);
        list = list.insert(list.length(), 0);
        checkListContents(list, 0,1,0,2,3,0);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testInsertInvalidArgument1(){
        list = list.insert(10, 0);
    }
    @Test (expected = IllegalArgumentException.class)
    public void testInsertInvalidArgument2(){
        list = list.insert(-10, 0);
    }

    @Test
    public void testDeleteLast(){
        list = list.append(1);
        checkListContents(list, 1);
        list = list.append(2);
        checkListContents(list, 1,2);
        list = list.append(3);
        checkListContents(list, 1,2,3);
        list = list.deleteLast();
        checkListContents(list, 1,2);
        list = list.deleteLast();
        checkListContents(list, 1);
        list = list.deleteLast();
        Assert.assertTrue(list.isEmpty());
    }

    @Test
    public void testDeleteFirst(){
        list = list.append(1);
        checkListContents(list, 1);
        list = list.append(2);
        checkListContents(list, 1,2);
        list = list.append(3);
        checkListContents(list, 1,2,3);
        list = list.deleteFirst();
        checkListContents(list, 2,3);
        list = list.deleteFirst();
        checkListContents(list, 3);
        list = list.deleteFirst();
        Assert.assertTrue(list.isEmpty());
    }

    @Test
    public void testDelete(){
        list = list.append(1);
        checkListContents(list, 1);
        list = list.append(2);
        checkListContents(list, 1,2);
        list = list.append(3);
        checkListContents(list, 1,2,3);
        list = list.delete(1);
        checkListContents(list, 1,3);
        list = list.delete(list.length() - 1);
        checkListContents(list, 1);
        list = list.delete(0);
        Assert.assertTrue(list.isEmpty());
    }

    @Test (expected = IllegalArgumentException.class)
    public void testDeleteInvalidArgument1(){
        list = list.delete(-2);
    }
    @Test (expected = IllegalArgumentException.class)
    public void testDeleteInvalidArgument2(){
        list = list.delete(10);
    }


}
