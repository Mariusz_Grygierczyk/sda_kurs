package day3;



public class CircularBuffer<T> {
    private static final int DEFAULT_INITIAL_CAPACITY = 8;

    private T[] storage;
    private int startIndex = 0;
    private int endIndex = 0;

    public CircularBuffer(int initialCapacity) {
        this.storage = (T[]) new Object[initialCapacity + 1];
    }

    public CircularBuffer() {
        this.storage = (T[]) new Object[DEFAULT_INITIAL_CAPACITY + 1];
    }

    public boolean isEmpty(){
        return (startIndex == endIndex);
    }

    public boolean isFull(){
        return (startIndex == (endIndex + 1) % storage.length);
    }

    public void push(T element){
        if (isFull()){
            throw new RuntimeException("buffer is full");
        } else {
            storage[endIndex] = element;
            endIndex = (endIndex + 1) % storage.length;
        }
    }

    public T pop(){
        if(isEmpty()) throw new RuntimeException("buffer is empty");
        else {
            T result = storage[startIndex];
            storage[startIndex] = null;
            startIndex = (startIndex + 1) % storage.length;
            return result;
        }
    }


    public void print() {
        for (T element: storage) {
            System.out.print(element + " ");
        }
    }

    public static void main(String[] args){



    }
}
