package com.sda.janusz;

public interface CardScanner {
	boolean hasCard (String payload);
	String maskCard (String payload);
}
