package com.sda.janusz;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

public class CardScannerTest {

	private CardScanner scanner;
	
	@Before
	public void setUp() throws Exception {
		scanner = new CardScannerImplementation();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		String testString = "";
		
		Assert.assertFalse( scanner.hasCard(testString));
		Assert.assertEquals(testString, scanner.maskCard(testString));
	}
	
	@Test
	public void test1() {
		String testString = "1111111111111111";
		
		Assert.assertTrue( scanner.hasCard(testString));
		Assert.assertEquals("****************", scanner.maskCard(testString));
	}
	
	@Test
	public void test2() {
		String testString = "11111111111111111111";
		
		Assert.assertFalse( scanner.hasCard(testString));
		Assert.assertEquals(testString, scanner.maskCard(testString));
	}
	
	@Test
	public void test3() {
		String testString = "1111111111111111aaaa";
		
		Assert.assertTrue( scanner.hasCard(testString));
		Assert.assertEquals("****************aaaa", scanner.maskCard(testString));
	}
	
	@Test
	public void test4() {
		String testString = "ada1111111111111111aaaa";
		
		Assert.assertTrue( scanner.hasCard(testString));
		Assert.assertEquals("ada****************aaaa", scanner.maskCard(testString));
	}
	
	@Test
	public void test5() {
		String testString = "1111111111111111 1111111111111111";
		
		Assert.assertTrue( scanner.hasCard(testString));
		Assert.assertEquals("**************** ****************", scanner.maskCard(testString));
	}
	
	@Test
	public void test6() {
		String testString = "1111111111111111a1111111111111111";
		
		Assert.assertTrue( scanner.hasCard(testString));
		Assert.assertEquals("****************a****************", scanner.maskCard(testString));
	}
	
	@Test
	public void test7() {
		String testString = "a1111111111111111a1111111111111111a";
		
		Assert.assertTrue( scanner.hasCard(testString));
		Assert.assertEquals("a****************a****************a", scanner.maskCard(testString));
	}
	
	@Test
	public void test8() {
		String testString = "11111111111111";
		
		Assert.assertFalse( scanner.hasCard(testString));
		Assert.assertEquals(testString, scanner.maskCard(testString));
	}

}
