package com.sda.janusz;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CardScannerImplementation implements CardScanner{

	@Override
	public boolean hasCard(String payload) {
		return payload.matches("(\\D*\\d{16}\\D*)+");
	}

	@Override
	public String maskCard(String payload) {

		return payload.replaceAll("(\\D*)\\d{16}(\\D*)", "$1****************$2");	
		
	}
	

}
