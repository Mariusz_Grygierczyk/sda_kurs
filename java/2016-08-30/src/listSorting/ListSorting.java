package listSorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

public class ListSorting {
	public static void main(String[] args){
		List<Integer> list = new ArrayList<Integer>(Arrays.asList(1,3,4,2,5,1,2));
				
		list = removeDuplicatesAndSort(list);
		System.out.println(list.toString());
	}
	
	public static List<Integer> removeDuplicatesAndSort(List<Integer> list){		
		return new ArrayList<Integer>(new TreeSet<Integer>(list));
	}
	

}
