package listSorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IntersectionAndDifference {
	public static void main(String[] args){
		List<Integer> listA = new ArrayList<Integer>(Arrays.asList(1,2,3));
		List<Integer> listB = new ArrayList<Integer>(Arrays.asList(2,3,4));
		System.out.println(getIntersection(listA, listB));
		System.out.println(getSymetricDifference(listA, listB));
	}
	
	public static List<Integer> getIntersection(List<Integer> listA, List<Integer> listB){
		Set<Integer> set = new HashSet<Integer>(listA);
		List<Integer> result = new ArrayList<Integer>();
		for(Integer i : listB){
			if(set.contains(i)) result.add(i);
		}
		return result;
	}
	
	public static List<Integer> getSymetricDifference(List<Integer> listA, List<Integer> listB){
		List<Integer> result = new ArrayList<Integer>();
		
		result.addAll(getDifference(listA, listB));
		result.addAll(getDifference(listB, listA));
		
		return result;
	}
	
	public static List<Integer> getDifference(List<Integer> listA, List<Integer> listB){
		Set<Integer> set = new HashSet<Integer>(listA);
		List<Integer> result = new ArrayList<Integer>();
		
		for(Integer i : listB){
			if(!set.contains(i)) result.add(i);
		}	
		
		return result;
	}
}
