package collections;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Maps {
	public static void main(String[] args){
		Map<Integer, String> map = new HashMap<>(); // new TreeMap();
		
		map.put(2, "two");
		map.put(2, "dwa");
		
		System.out.println("2 = " + map.get(2));
		
		map.putIfAbsent(2, "two");
		
		System.out.println("2 = " + map.get(2));
		
		map.put(3, "three");
		map.put(5, "five");
		
		System.out.println(" contains 3 ? " + map.containsKey(3));
		System.out.println(" contains value two ? " + map.containsValue("two"));
		
		for(Integer key : map.keySet()){
			System.out.println("key = " + key);
		}
		
		for(String value : map.values()){
			System.out.println("value = " + value);
		}
		
		for(Entry<Integer, String> entry : map.entrySet()){
			System.out.println("entry key = " + entry.getKey() + " value = " + entry.getValue());
		}
		
		map.remove("two");
		map.remove(3);
		
		System.out.println(map);
		
	}
}
