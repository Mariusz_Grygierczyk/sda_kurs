package collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Lists {
	public static void main(String[] args){
		List<String> list = new ArrayList<>();
		list = new LinkedList<>();
		
		System.out.println(" is empty ? " + list.isEmpty());
		
		list.add("first");
		list.add("second");
		list.add(1, "third");
		
		System.out.println("size = " + list.size());
		
		System.out.println("element 0 " + list.get(0));
		System.out.println("element 1 " + list.get(1));
		System.out.println("element 2 " + list.get(2));
		
		System.out.println("sublist = " + list.subList(1, 2));
		
		for(String element : list){
			System.out.println("foreach " + element);
		}
		
		System.out.println("index of second = " + list.indexOf("third"));
		
		Iterator<String> iterator = list.iterator();
		while(iterator.hasNext()){
			String element = iterator.next();
			System.out.println("iterator " + element);
		}
		
		list.remove(1);
		list.remove("third");
		System.out.println("list after removals = " + list);
		
		LinkedList<String> linked = (LinkedList<String>) list;
		System.out.println("first element of linked list " + linked.getFirst());
		System.out.println("last element of linked list " + linked.getLast());
	
		List<String> unmodifiableList = Collections.unmodifiableList(list);
		unmodifiableList.add("fourth");
		
	}
}
