package collections;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class Queues {

	public static void main(String[] args){
		Queue<Integer> queue = new LinkedList<>();
		
		queue.offer(1);
		queue.offer(2);
		queue.offer(3);
		
		System.out.println(queue.poll());
		System.out.println(queue.poll());
		System.out.println(queue.poll());
		
		queue.offer(3);
		
		System.out.println(" peek = " + queue.peek());
		
		System.out.println(" size = " + queue.size());
		
		
		PriorityQueue<Integer> heap = new PriorityQueue<>();
		
		heap.offer(3);
		heap.offer(5);
		heap.offer(1);
		
		System.out.println(heap.poll());
		System.out.println(heap.poll());
		System.out.println(heap.poll());
	}
}
