package com.sda.excercise;

public class ArrayMerger {
	public static int[] merge (int[] a, int[] b){
		int[] result = new int[a.length + b.length];
		int indexA = 0;
		int indexB = 0;
		
		 int indexResult = 0;
	        while (indexA < a.length && indexB < b.length) {
	            if (a[indexA] < b[indexB]) {
	                result[indexResult] = a[indexA];
	                indexA++;
	            } else {
	                result[indexResult] = b[indexB];
	                indexB++;
	            }
	            indexResult++;
	        }
	        System.arraycopy(a, indexA, result, indexResult, a.length - indexA);
	        System.arraycopy(b, indexB, result, indexResult, b.length - indexB);
	        
	        return result;
		
	}
}
