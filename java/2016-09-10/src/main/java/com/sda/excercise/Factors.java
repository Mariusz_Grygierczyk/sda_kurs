package com.sda.excercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Factors {
	
	public static int[] getFactors(int number){
		List<Integer> result = new ArrayList<Integer>();
		
		for(int i = 1; i <= number / 2; i++){
			if((number % i) == 0) result.add(i);
		}
		
		return result.stream().mapToInt((e) -> e.intValue()).toArray();		
	}
	
	public static void main(String[] args){
		
//		for(int i : getFactors(9)) System.out.print(i + " ");
//		System.out.println(isPerfect(9));
		
		ArrayList<Integer> perfects = new ArrayList<Integer>();
		for(int i = 1; i < 10000; i++){
			if (isPerfect(i)) perfects.add(i);
		}
		perfects.stream().forEach((e) -> System.out.print(e + " "));
	}
	
	public static boolean isPerfect(int number){
		return number == Arrays.stream(getFactors(number)).sum();		
	}
}
