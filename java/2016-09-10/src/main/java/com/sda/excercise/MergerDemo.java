package com.sda.excercise;

import java.util.Arrays;
import java.util.Random;

public class MergerDemo {
	public static void main(String[] args){
		int[] arrA = getRandomArray(20, 100);
		System.out.print("A: ");
		System.out.println(Arrays.toString(arrA));
		
		int[] arrB = getRandomArray(32, 100);		
		System.out.print("B: ");
		System.out.println(Arrays.toString(arrB));
		
		Arrays.sort(arrA);
		Arrays.sort(arrB);
		
		
		System.out.print("A: ");
		System.out.println(Arrays.toString(arrA));		
		
		System.out.print("B: ");
		System.out.println(Arrays.toString(arrB));
		
		int[] arrC = ArrayMerger.merge(arrA, arrB);
		System.out.print("C: ");
		System.out.println(Arrays.toString(arrC));
		
	}
	
	public static int[] getRandomArray(int size, int range){
		Random rand = new Random();
		
		int[] result = new int[size];
		
		for (int i = 0; i < size; i++){
			result[i] = rand.nextInt(range);
		}
		
		return result;		
	}
	
	public static boolean isSorted(int[] array){
		for (int i = 1; i < array.length; i++){
			if (array[i - 1] > array[i]) return false;
		}
		return true;
	}
}
