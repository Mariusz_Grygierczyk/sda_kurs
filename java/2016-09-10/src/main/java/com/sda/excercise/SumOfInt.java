package com.sda.excercise;

import java.util.Scanner;

public class SumOfInt {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		
		System.out.println("podaj liczbe");
		int number = input.nextInt();
		input.close();		
		
		int result = 0;
		while (number > 0){
			result += number % 10;
			number = number / 10;
		}
		
		System.out.println("suma cyfr to: " + result);
	}
}
