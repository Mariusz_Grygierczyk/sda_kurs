package com.sda;

import java.lang.management.ManagementFactory;

import javax.management.MBeanServer;
import javax.management.ObjectName;

public class ServerAgent {
	private MBeanServer mbs = null;

	public void waitForEnterPressed() {
		try {
			System.out.println("Press  to continue...");
			System.in.read();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public ServerAgent(){
		mbs = ManagementFactory.getPlatformMBeanServer();

		Server serverBean = new Server();
		ObjectName serverName = null;

		try {
			serverName = new ObjectName("FOO:name=ServerBean");
			mbs.registerMBean(serverBean, serverName);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
