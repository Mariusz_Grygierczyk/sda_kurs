package lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Lambdas {

	public static void main(String[] args){
		
		List<Car> cars = new ArrayList<>();
		
		cars.add(new Car("mazda"));
		cars.add(new Car("seat"));
		cars.add(new Car("aston martin"));
		cars.add(new Car("akura"));
		
		// old way
		for(Car car : cars){
			System.out.println(car);
		}
		
		// lambdas
		cars.forEach(System.out::println);
		
		// old way
		List<Car> startingWithA = findCarsStartingWithA(cars);
		System.out.println("old way starting with a = " + startingWithA);
		List<Car> endingWithA = findCarsEndingWithA(cars);
		System.out.println("old way ending with a = " + endingWithA);
		
		// lambdas
		List<Car> startingWithANew = findCarsFilteredCars(cars, car -> car.getBrand().startsWith("a"));
		System.out.println("new way starting with a = " + startingWithANew);
		List<Car> endingWithANew = findCarsFilteredCars(cars, car -> car.getBrand().endsWith("a"));
		System.out.println("new way ending with a = " + endingWithANew);
	}
	
	public static List<Car> findCarsStartingWithA(List<Car> input){
		List<Car> result = new ArrayList<>();
		for(Car car : input){
			if(car.getBrand().startsWith("a")){
				result.add(car);
			}
		}
		return result;
	}
	
	public static List<Car> findCarsEndingWithA(List<Car> input){
		List<Car> result = new ArrayList<>();
		for(Car car : input){
			if(car.getBrand().endsWith("a")){
				result.add(car);
			}
		}
		return result;
	}
	
	public static List<Car> findCarsFilteredCars(List<Car> input, Predicate<Car> filter){
		List<Car> result = new ArrayList<>();
		for(Car car : input){
			if(filter.test(car)){
				result.add(car);
			}
		}
		return result;
	}
}

class Car{
	private String brand;
	
	public Car(String brand){
		this.brand = brand;
	}
	
	public String getBrand(){
		return brand;
	}
	
	public String toString(){
		return brand;
	}
}
