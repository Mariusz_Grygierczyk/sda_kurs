package taskQueue;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class TaskQueue {
	private List<Task> runningTasks = new ArrayList<Task>(4);
	private Queue<Task> taskQueue = new LinkedList<Task>();

	public Task addTask(String id){
		Task task = new Task(id);
		if (runningTasks.size() < 4) {
			System.out.println(task.getID() + " starting");
			runningTasks.add(task);
		}
		else {
			System.out.println(task.getID() + " added to queue");
			taskQueue.offer(task);
		}
		return task;
	}

	
	
	class Task {
		private String id;
		
		public Task(String id){
			this.id = id;
		}
		
		public String getID() {
			return id;
		}
		public void end(){
			System.out.print(getID() + " ending ");
			runningTasks.remove(this);
			if (!taskQueue.isEmpty()) {
				System.out.print(taskQueue.peek().getID() + " starting");
				runningTasks.add(taskQueue.poll());
			}
			System.out.println();
		}
		
	}
	
	public void printStatus(){
		System.out.print("running: ");
		for(Task t : runningTasks) System.out.print(t.getID() + " ");
		System.out.println();
		System.out.print("queued: ");
		for(Task t : taskQueue) System.out.print(t.getID() + " ");
		System.out.println();
	}
	
	public static void main(String[] args){
		TaskQueue taskQueue = new TaskQueue();
		
		
	

		Task task1 = taskQueue.addTask("task 1");
		Task task2 = taskQueue.addTask("task 2");
		Task task3 = taskQueue.addTask("task 3");
		Task task4 = taskQueue.addTask("task 4");
		Task task5 = taskQueue.addTask("task 5");
		Task task6 = taskQueue.addTask("task 6");
	
		taskQueue.printStatus();
		
		Task task7 = taskQueue.addTask("task 7");
		task3.end();
		
		taskQueue.printStatus();
		
		task1.end();
		Task task8 = taskQueue.addTask("task 8");
		task5.end();

		taskQueue.printStatus();
		
		task4.end();
		task2.end();
		task6.end();
		task8.end();
		task7.end();
		
	}
}
