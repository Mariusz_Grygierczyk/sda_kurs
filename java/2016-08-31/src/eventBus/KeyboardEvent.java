package eventBus;

public class KeyboardEvent {
	private String key;
	
	public KeyboardEvent(String key){
		this.key = key;
	}
	
	public String getKey(){
		return key;
	}
}
