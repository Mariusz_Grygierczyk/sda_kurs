package eventBus;

public class PrintToConsole implements Handler{
	private String prefix;
	
	public PrintToConsole(String prefix) {
		this.prefix = prefix;
	}
	
	@Override
	public void onKeyboardEvent(KeyboardEvent event) {
		System.out.println(prefix + event.getKey());
			
	}

}
