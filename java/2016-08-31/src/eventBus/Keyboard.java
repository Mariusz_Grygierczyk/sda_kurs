package eventBus;

public class Keyboard {
	private EventBus eventBus;
	
	public Keyboard(EventBus eventBus){
		this.eventBus = eventBus;
	}
	
	public void sendEvent(String string){
		eventBus.fireEvent(new KeyboardEvent(string));
	}
	
	public static void main(String[] args){
		Handler handler = new PrintToConsole("key = ");
		EventBus eventBus = new EventBus();
		eventBus.registerHandler(handler);
		Keyboard keyboard = new Keyboard(eventBus);
				
		keyboard.sendEvent("aaa");
		
		Handler handler2 = new PrintToConsole("key : ");
		eventBus.registerHandler(handler2);
		
		keyboard.sendEvent("bbb");

	}
}
