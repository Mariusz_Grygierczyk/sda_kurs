package eventBus;

import java.util.ArrayList;
import java.util.List;

public class EventBus {
	private List<Handler> handlerList = new ArrayList<Handler>();
	
	public void fireEvent (KeyboardEvent event){
		for(Handler handler : handlerList){
			handler.onKeyboardEvent(event);
		}
	}
	
	public void registerHandler(Handler handler){
		handlerList.add(handler);
	}
	
	
}
