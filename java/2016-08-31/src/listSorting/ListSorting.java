package listSorting;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.TreeMap;

public class ListSorting {
	public static void main(String[] Args){
		Random random = new Random();
		List<Integer> list = new ArrayList<Integer>(10);
		
		for (int i = 0; i < 10 ; i++) list.add(random.nextInt(100));
		
		Map<Integer, Integer> map = new TreeMap<Integer, Integer>();
		for (Integer i : list){
			//map.putIfAbsent(i, Collections.frequency(list, i));
			if (map.containsKey(i)) map.put(i, map.get(i)+1);
			else map.put(i, 1);
		}
		System.out.println(list);
		System.out.println(map);
		list = new ArrayList<Integer>(10);
		for(Entry<Integer, Integer> entry : map.entrySet()){
			for(int i = 0; i < entry.getValue(); i++) list.add(entry.getKey());
		}
		System.out.println(list);
		
	}
}
