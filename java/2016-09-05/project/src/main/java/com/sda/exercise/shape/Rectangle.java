package com.sda.exercise.shape;

public class Rectangle implements Shape{
	private double a;
	private double b;
	
	public Rectangle(double a, double b){
		this.a = a;
		this.b = b;
	}
	
	public double area(){
		return a * b;
	}
	public String toString(){
		return "prostokat a = " + a + " b = " + b;
	}
}
