package com.sda.excercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Prime {
	private TreeSet<Integer> primes = new TreeSet<Integer>();
	
	public void Priems(){
		primes.add(2);
		primes.add(3);
	}
	
	public static void main(String[] args){
		Prime prime = new Prime();
		
		long timeBefore = System.currentTimeMillis();
		for(int i = 0; i <= 100000; i++){
			if(prime.isPrime2(i)) System.out.println(i);
		}
		long timeAfter = System.currentTimeMillis();
        System.out.println("czas = " + (timeAfter - timeBefore) + "ms");
		System.out.println(prime.isPrime(12));
		System.out.println(prime.isPrime2(12));
		
	}
	
	public boolean isPrime(int number){
		if (number <= 1) return false;
		if (number <= 3) return true;
		for (int i = 2; i < number; i++){
			if(number % i == 0) return false;
		}
		return true;
		
		
	}
	
	public boolean isPrime2(int number){
		if (number <= 1) return false;
		if (primes.contains(number)) return true;
		
		for(int p : primes.headSet((int)(Math.sqrt(number)))){
			if (number % p == 0) return false;
		}
		primes.add(number);
		return true;
	}
	
	public boolean[] isPrime3(int number){
		boolean[] isPrime = new boolean[number];
		Arrays.fill(isPrime, true);
		
		for(int i = 2; i*i <= number; i++){
			if(isPrime[i]){
				for(int j = i*i; j <= number; j += i){
					isPrime[j] = false;
				}
			}			
		}
		
		
		return isPrime;
	}
}
