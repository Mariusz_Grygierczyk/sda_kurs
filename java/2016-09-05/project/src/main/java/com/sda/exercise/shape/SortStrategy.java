package com.sda.exercise.shape;

import java.util.List;

public interface SortStrategy {
	public abstract void sortByArea(List<Shape> shapes);
}
