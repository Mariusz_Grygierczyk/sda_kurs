package com.sda.exercise.shape;

import java.util.List;

public class BubbleSort implements SortStrategy {

	@Override
	public void sortByArea(List<Shape> shapes) {
		boolean swapped = true;
		while(swapped){
			swapped = false;
			for (int i = 0; i < shapes.size()-1; i++){
				if(shapes.get(i).area() > shapes.get(i+1).area()){
					Shape swap = shapes.get(i);
					shapes.set(i, shapes.get(i+1));
					shapes.set(i+1, swap);
					swapped = true;
				}
			}
		}

	}

}
