package com.sda.exercise.shape;

public class Circle implements Shape{
	private double r;
	
	public Circle(double r){
		this.r = r;
	}
	
	public double area(){
		return Math.PI * Math.pow(r, 2);
	}
	public String toString(){
		return "kolo r = " + r;
	}

}
