package com.sda.exercise.shape;

public interface Shape {
	public abstract double area();
	public abstract String toString();
}
