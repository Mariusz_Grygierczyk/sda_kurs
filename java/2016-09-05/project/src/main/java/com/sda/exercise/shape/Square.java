package com.sda.exercise.shape;

public class Square implements Shape{
	private double a;

	
	public Square(double a){
		this.a = a;
	}
	
	public double area(){
		return Math.pow(a, 2);
	}
	
	public String toString(){
		return "kwadrat a = " + a;
	}
}
