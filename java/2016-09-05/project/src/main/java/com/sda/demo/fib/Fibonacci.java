package com.sda.demo.fib;


public class Fibonacci {


    public static void main(String[] args) {

        //Uruchom dla n = 36,38,43.. kiedy czas oczekiwania jest nieakceptowalny ?
        int n = 3;
        
        long timeBefore = System.currentTimeMillis();
        System.out.println("Dla: " + n + " wartosc ciagu fibbonacciego: " + fibonacci(n));
        long timeAfter = System.currentTimeMillis();
        System.out.println("czas = " + (timeAfter - timeBefore) + "ms");
    }


    // Jaka tutaj jest zlozonosc ?
    public static int fibonacci(int n) {
        if (n < 2) {
            return n;
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }


}
