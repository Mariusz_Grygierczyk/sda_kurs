package com.sda.excercise;

public class SumOfDigits {
	
	
	public static int sumOfDigits(int number){
		int sum = 0;
		for (int i = 0; i < (Math.log10(number)+1) ; i++) {
			sum += number / Math.pow(10, i) % 10;
		}
		return sum;
	}
	
	
	public static void main(String[] args){
		System.out.println(sumOfDigits(123));
	}
}
