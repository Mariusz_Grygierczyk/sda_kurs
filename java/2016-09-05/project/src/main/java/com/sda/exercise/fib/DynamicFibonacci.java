package com.sda.exercise.fib;


public class DynamicFibonacci {

    public static void main(String[] args) {

        int n = 46;

        long timeBefore = System.currentTimeMillis();
        System.out.println("Dla: " + n + " wartosc ciagu fibbonacciego: " + fibonacci(n));
        long timeAfter = System.currentTimeMillis();
        System.out.println("czas = " + (timeAfter - timeBefore) + "ms");

    }


    public static int fibonacci(int n){
//    	int results[] = new int[n+1];
//            	
//        for(int i = 0; i <= n; i++){
//        	if(i < 2) results[i] = i;
//        	else results[i] = results[i-1] + results[i-2];
//        }
//        return results[n]; 	
    	
    	
    	if(n == 0) return 0;
    	
    	int first = 0;
    	int second = 1;    	
    	
    	for (int i = 1; i < n; i++){
    		int temp = first + second; 
    		first = second;
    		second = temp;    		
    	}    	
    	return second;

    }

}
