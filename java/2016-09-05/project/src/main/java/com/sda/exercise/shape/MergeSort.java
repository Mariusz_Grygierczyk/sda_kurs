package com.sda.exercise.shape;

import java.util.ArrayList;
import java.util.List;

public class MergeSort implements SortStrategy {

	@Override
	public void sortByArea(List<Shape> shapes) {
		shapes = split(shapes);	

	}
	
	private List<Shape> split(List<Shape> shapes) {
		List<Shape> left = new ArrayList<Shape>();
	    List<Shape> right = new ArrayList<Shape>();
	    int center;
	 
	    if (shapes.size() == 1) {    
	        return shapes;
	    } else {
	        center = shapes.size()/2;
	        for (int i=0; i<center; i++) {
	                left.add(shapes.get(i));
	        }
	 
	        for (int i=center; i<shapes.size(); i++) {
	                right.add(shapes.get(i));
	        }
	 
	        left  =  split(left);
	        right =  split(right);
	 
	        merge(left, right, shapes);
	    }
	    return shapes;
	}
	
	private void merge(List<Shape> left, List<Shape> right, List<Shape> whole) {
	    int leftIndex = 0;
	    int rightIndex = 0;
	    int wholeIndex = 0;
	 
	    while (leftIndex < left.size() && rightIndex < right.size()) {
	        if ( (left.get(leftIndex).area()<(right.get(rightIndex).area()))) {
	            whole.set(wholeIndex, left.get(leftIndex));
	            leftIndex++;
	        } else {
	            whole.set(wholeIndex, right.get(rightIndex));
	            rightIndex++;
	        }
	        wholeIndex++;
	    }
	 
	    List<Shape> rest;
	    int restIndex;
	    if (leftIndex >= left.size()) {
	        rest = right;
	        restIndex = rightIndex;
	    } else {
	        rest = left;
	        restIndex = leftIndex;
	    }
	 
	    for (int i=restIndex; i<rest.size(); i++) {
	        whole.set(wholeIndex, rest.get(i));
	        wholeIndex++;
	    }
	}
	
}
