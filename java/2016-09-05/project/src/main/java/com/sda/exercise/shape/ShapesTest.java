package com.sda.exercise.shape;

import java.util.ArrayList;
import java.util.List;

public class ShapesTest {
	public static void main(String[] args){
		List<Shape> shapes = new ArrayList<Shape>();
		
		shapes.add(new Rectangle(2, 3));
		shapes.add(new Square(2));
		shapes.add(new Circle(3));
		shapes.add(new Square(3));
		
		
		
		for(Shape s : shapes){
			System.out.println(s.area());
		}
		long timeBefore = System.currentTimeMillis();
		SortStrategy sort = new MergeSort();
		sort.sortByArea(shapes);
		long timeAfter = System.currentTimeMillis();
        System.out.println("czas = " + (timeAfter - timeBefore) + "ms");
		System.out.println("====");
		for(Shape s : shapes){
			System.out.println(s.area());
		}

	}
}
