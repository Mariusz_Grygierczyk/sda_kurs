public class Interfaces{
	public static void main(String[] args){
		Car fast = new FastCar();
		Car slow = new SlowCar();
		
		takeShortRide(fast);
		takeShortRide(slow);
		
		System.out.println("location of fast car = " + fast.getCurrentLocation());
		System.out.println("location of slow car = " + slow.getCurrentLocation());
	}

	public static void takeShortRide(Car car){
		car.moveForward();
		car.moveForward();
		car.moveForward();
	}
	
	private static interface Car{
		public abstract void moveForward();
		public abstract int getCurrentLocation();
	}

	private static class FastCar implements Car{
		private int location = 0;

		public void moveForward(){
			location += 3;
		}
		
		public int getCurrentLocation(){
			return location;
		}
	}

	private static class SlowCar implements Car{
		private int location = 0;

		public void moveForward(){
			location += 1;
		}
		
		public int getCurrentLocation(){
			return location;
		}
	}
}


