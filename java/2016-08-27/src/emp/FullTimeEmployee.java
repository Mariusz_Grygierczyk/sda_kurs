package emp;

public class FullTimeEmployee extends Employee{	
	public static final double TAX_RATE = 0.20;
	public static final double SOCIAL_INSURANCE = 1000.0;
	
	public FullTimeEmployee(double grossSalary) {
		super(grossSalary);
	}

	public double getNetSalary(){
		return (super.getGrossSalary() - SOCIAL_INSURANCE) * (1 - TAX_RATE);
	}

}
