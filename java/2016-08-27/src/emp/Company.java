package emp;

import java.util.ArrayList;

public class Company {
	private ArrayList<Employee> employees = new ArrayList<Employee>();
	
	
	public void addEmployee (Employee employee){
		employees.add(employee);
	}
	public void removeEmployee (Employee employee){
		employees.remove(employee);
	}
	public double getAverageSalary(){
		double sum = 0;
		for (Employee e : employees){
			sum += e.getNetSalary();
		}
		return sum / employees.size();
	}
	public double getTotalSalary(){
		double sum = 0;
		for (Employee e : employees){
			sum += e.getNetSalary();
		}
		return sum;
	}
}
