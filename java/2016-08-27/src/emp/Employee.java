package emp;

public abstract class Employee {
	private String firstName;
	private String lastName;
	private double grossSalary;
	
	public double getGrossSalary() {
		return grossSalary;
	}
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}

	public Employee(double grossSalary){
		this.grossSalary = grossSalary;
	}
	
	public double getNetSalary(){
		return grossSalary;
	}
	
	@Override
	public boolean equals(Object other){
		if (!(other instanceof Employee)) return false;
		else {
			Employee otherAsEmployee = (Employee)other;
			return firstName.equals(otherAsEmployee.getFirstName()) && lastName.equals(otherAsEmployee.getLastName());
		}
	}
}
