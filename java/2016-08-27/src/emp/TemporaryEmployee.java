package emp;

public class TemporaryEmployee extends Employee{
	public static final double TAX_RATE = 0.10;

	
	public TemporaryEmployee(double grossSalary) {
		super(grossSalary);
	}

	public double getNetSalary(){
		return super.getGrossSalary() * (1 - TAX_RATE);
	}
}
