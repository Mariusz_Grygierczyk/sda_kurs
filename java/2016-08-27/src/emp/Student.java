package emp;

public class Student extends Employee{
	public static final double TAX_RATE = 0.05;
	
	public Student(double grossSalary) {
		super(grossSalary);
	}

	public double getNetSalary(){
		return super.getGrossSalary() * (1 - TAX_RATE);
	}
}
