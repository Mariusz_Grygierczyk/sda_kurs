package calc;

public class Division implements Operation{
	int a;
	int b;
	
	public Division(int a, int b){
		this.a = a;
		this.b = b;
	}
	
	public int calculate(){
		return a / b;
	}
}
