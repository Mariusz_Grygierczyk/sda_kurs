package calc;

public class Addition implements Operation{
	int a;
	int b;
	
	public Addition(int a, int b){
		this.a = a;
		this.b = b;
	}
	
	public int calculate(){
		return a + b;
	}
}
