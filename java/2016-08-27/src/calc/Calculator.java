package calc;

import java.util.Scanner;

public class Calculator {
	public static void main (String[] args){
		Scanner scanner = new Scanner(System.in);
		int a, b, result;
		String operator;
		Operation operation = null;
		

		System.out.println("podaj pierwszy argument:");
		a = scanner.nextInt();
		System.out.println("wybierz dzialanie:");
		operator = scanner.next();
		System.out.println("podaj drugi argument:");
		b = scanner.nextInt();
		
		switch(operator){
		case "+": operation = new Addition(a, b);
		break;
		case "-": operation = new Substraction(a, b);
		break;
		case "/": operation = new Division(a, b);
		break;
		case "*": operation = new Multiplication(a, b);
		break;
		}
		
		
		System.out.println(a + " " + operator + " " + b + " = " + operation.calculate());
		
	}
}
