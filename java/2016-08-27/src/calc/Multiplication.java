package calc;

public class Multiplication implements Operation{
	int a;
	int b;
	
	public Multiplication(int a, int b){
		this.a = a;
		this.b = b;
	}
	
	public int calculate(){
		return a * b;
	}
}
