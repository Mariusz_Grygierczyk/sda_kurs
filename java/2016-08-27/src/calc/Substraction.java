package calc;

public class Substraction implements Operation{
	int a;
	int b;
	
	public Substraction(int a, int b){
		this.a = a;
		this.b = b;
	}
	
	public int calculate(){
		return a - b;
	}
}
