package shapes;

public interface Shape {
	public abstract double area();
}
