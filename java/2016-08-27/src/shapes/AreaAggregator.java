package shapes;

public class AreaAggregator {
	
	public static void main(String[] args){
		Shape[] shapes = new Shape[5];
		shapes[0] = new Rectangle(10.0, 5.0);
		shapes[1] = new Circle(2.0);
		shapes[2] = new Square(4);
		shapes[3] = new Circle(3.0);
		shapes[4] = new Rectangle(2, 4);
		
		AreaAggregator aggregator = new AreaAggregator();
		double aggregatedArea = aggregator.aggregate(shapes);
		double expectedAggregatedArea = 50. + 4 * Math.PI + 16 + 9 * Math.PI + 8;
		
		System.out.println("expected = " + expectedAggregatedArea + " result = " + aggregatedArea);
	}
	
	public double aggregate(Shape[] shapes){
		double result = 0;
		for (Shape shape : shapes){
			result += shape.area();
		}
		return result;
	}
}
