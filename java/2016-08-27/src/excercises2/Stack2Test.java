package excercises2;

import static org.junit.Assert.*;

import org.junit.Test;

public class Stack2Test {

	@Test
	public void test() {
		Stack2 stack = new Stack2(2);
		stack.push(2);
		stack.push(3);
		
		assertEquals(stack.pop(), 3);
		assertEquals(stack.pop(), 2);
	}

}
