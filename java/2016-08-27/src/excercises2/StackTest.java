package excercises2;

import static org.junit.Assert.*;

import org.junit.Test;

public class StackTest {

	@Test
	public void test() {
		Stack stack = new Stack();
		stack.push(2);
		stack.push(3);
		
		assertEquals(stack.pop(), 3);
		assertEquals(stack.pop(), 2);
		assertEquals(stack.pop(), null);
	}

}
