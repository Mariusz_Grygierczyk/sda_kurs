package excercises2;

public class Stack {
	private Element topElement = null;

	public void push(Object elem) {
		topElement = new Element(elem, topElement);
	}

	public Object pop() {
		Element tmp = topElement;
		if (topElement != null)
			topElement = topElement.nextElement;
		return tmp != null ? tmp.contents : null;
	}

	private class Element {
		private Object contents;
		private Element nextElement;

		public Element(Object contents, Element firstElement) {
			this.contents = contents;
			this.nextElement = firstElement;
		}

	}
}