package excercises2;

public class Stack2 {
	int[] tab;
	int currentIndex = -1;

	public Stack2(int length) {
		tab = new int[length];
	}

	public void push(int elem) {
		if (currentIndex >= tab.length) {
			int[] oldTab = tab;
			tab = new int[oldTab.length * 2];
			System.arraycopy(oldTab, 0, tab, 0, oldTab.length);
		}
		currentIndex++;
		tab[currentIndex] = elem;
	}

	public int pop() {
		int ret = 0;
		if (!isEmpty()) {
			ret = tab[currentIndex];
			currentIndex--;
		} else {} // error

		if (currentIndex < tab.length * 0.75) {
			int[] oldTab = tab;
			tab = new int[(int)Math.ceil(tab.length * 0.75)];
			System.arraycopy(oldTab, 0, tab, 0, tab.length);
		}
		
		return ret;
	}

	public boolean isEmpty() {
		return currentIndex < 0;
	}
}
