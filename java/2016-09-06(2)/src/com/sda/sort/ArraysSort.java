package com.sda.sort;

import java.util.Arrays;

public class ArraysSort implements Sortable {

	@Override
	public void sort(int[] array) {
		Arrays.sort(array);

	}

}
