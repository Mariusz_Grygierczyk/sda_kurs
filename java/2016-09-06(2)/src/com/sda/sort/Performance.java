package com.sda.sort;

public class Performance {

	public static void main(String[] args) {	
		
		test(new CountSort(),10000);
		test(new BubbleSort(),10000);
		test(new HeapSort(),10000);
		test(new MergeSort(),10000);
		test(new QuickSort(),10000);
		System.out.println();
		test(new CountSort(),50000);
		test(new BubbleSort(),50000);
		test(new HeapSort(),50000);
		test(new MergeSort(),50000);
		test(new QuickSort(),50000);
		System.out.println();
		test(new CountSort(),100000);
		test(new BubbleSort(),100000);
		test(new HeapSort(),100000);
		test(new MergeSort(),100000);
		test(new QuickSort(),100000);
		System.out.println();
		test(new QuickSort(),1000000);
		test(new QuickSort(),10000000);
		test(new CountSort(),1000000);
		test(new CountSort(),10000000);
		
		
	}
	
	public static void test(Sortable sort, int size){
		
		int[] array = Sort.getRandomValueArray(size);		
		long timeBefore = System.currentTimeMillis();
		sort.sort(array);
		long timeAfter = System.currentTimeMillis();
		System.out.println(sort.getClass().getName() +  " rozmiar = " + size + " czas = " + (timeAfter - timeBefore) + "ms");
		
		
	}
}
