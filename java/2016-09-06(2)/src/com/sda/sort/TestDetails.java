package com.sda.sort;

import java.util.Random;

public class TestDetails {
	long time;
	int testSize;
	
	public TestDetails(Sortable sort, int testSize) {
		this.testSize = testSize;
		
		int[] array = Sort.getRandomValueArray(testSize);		
		long timeBefore = System.currentTimeMillis();
		sort.sort(array);
		long timeAfter = System.currentTimeMillis();
		time = timeAfter - timeBefore;
	}

	public long getTime() {
		return time;
	}
	
	public static int[] getRandomValueArray(int size) {
        Random random = new Random();
        int[] result = new int[size];
        for (int i = 0; i < size; ++i) {
            result[i] = random.nextInt(10000);
        }
        return result;
    }
}
