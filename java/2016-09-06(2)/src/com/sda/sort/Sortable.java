package com.sda.sort;


public interface Sortable {
	public void sort(int[] array);
}
