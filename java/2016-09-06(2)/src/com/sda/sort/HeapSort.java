package com.sda.sort;


import java.util.PriorityQueue;
import java.util.Queue;
import java.util.stream.IntStream;

public class HeapSort implements Sortable{

	@Override
    public void sort(int[] array) {
        Queue<Integer> heap = new PriorityQueue<>();
        IntStream.of(array).forEach(heap::add);

        for (int i = 0; i < array.length; ++i) {
            array[i] = heap.poll();
        }
    }

}
