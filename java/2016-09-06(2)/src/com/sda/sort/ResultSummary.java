package com.sda.sort;

import java.util.ArrayList;
import java.util.List;

public class ResultSummary {
	List<TestDetails> results;
	String name;
	Sortable sort;
	int testSize;
	int numberOfTests;
	
	public ResultSummary(Sortable sort, String name , int testSize, int numberOfTests){
		this.name = name;
		this. sort = sort;
		results = new ArrayList<TestDetails>(numberOfTests);
		this.testSize = testSize;
		this.numberOfTests = numberOfTests;
		}
	
	public void runTests(){
		for(int i = 0; i < numberOfTests ; i++){
			results.add(new TestDetails(sort, testSize));
		}
	}
	
	public void printResults(){
		long totalTime = 0;
		for(TestDetails result : results){
			totalTime += result.getTime();
		}		
		long avgTime = totalTime / numberOfTests;
		System.out.println(name + " liczba elementow = " + testSize + " liczba testow = " + numberOfTests + " sredni czas = " + avgTime);
		
	}
	
	public static void main(String[] args){
		ResultSummary results = new ResultSummary(new CountSort(), "count sort", 1_000_000, 6);
		results.runTests();
		results.printResults();
		
		results = new ResultSummary(new CountSort(), "count sort", 10_000_000, 6);
		results.runTests();
		results.printResults();
		
		results = new ResultSummary(new CountSort(), "count sort", 25_000_000, 6);
		results.runTests();
		results.printResults();
		
		results = new ResultSummary(new QuickSort(), "quick sort", 1_000_000, 6);
		results.runTests();
		results.printResults();
		
		results = new ResultSummary(new QuickSort(), "quick sort", 10_000_000, 6);
		results.runTests();
		results.printResults();
		
		results = new ResultSummary(new QuickSort(), "quick sort", 25_000_000, 6);
		results.runTests();
		results.printResults();
		
		results = new ResultSummary(new ArraysSort(), "Arrays.sort", 1_000_000, 6);
		results.runTests();
		results.printResults();
		
		results = new ResultSummary(new ArraysSort(), "Arrays.sort", 10_000_000, 6);
		results.runTests();
		results.printResults();
		
		results = new ResultSummary(new ArraysSort(), "Arrays.sort", 25_000_000, 6);
		results.runTests();
		results.printResults();
	}
	
}
