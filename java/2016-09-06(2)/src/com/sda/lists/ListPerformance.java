package com.sda.lists;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ListPerformance {
	
	
	public static void main(String[] args){
		List<Integer> linkedList = new LinkedList<Integer>();
		List<Integer> arrayList = new ArrayList<Integer>();
		Random random = new Random();
		
		
		long timeBefore = System.currentTimeMillis();
		for(int i = 0; i < 1_000_000; i++){
			linkedList.add(random.nextInt(10000));
		}
		long timeAfter = System.currentTimeMillis();
		
		System.out.println("linked list add: " + (timeAfter - timeBefore));
		
		timeBefore = System.currentTimeMillis();
		for(int i = 0; i < 1_000_000; i++){
			arrayList.add(random.nextInt(10000));
		}
		timeAfter = System.currentTimeMillis();
		
		System.out.println("Array list add: " + (timeAfter - timeBefore));
		
		
		timeBefore = System.currentTimeMillis();
		for(int i = 0; i < 50_000; i++){
			linkedList.get(random.nextInt(100000));
		}
		timeAfter = System.currentTimeMillis();
		
		System.out.println("linked list get: " + (timeAfter - timeBefore));
		
		timeBefore = System.currentTimeMillis();
		for(int i = 0; i < 50_000; i++){
			arrayList.get(random.nextInt(100000));
		}
		timeAfter = System.currentTimeMillis();
		
		System.out.println("Array list get: " + (timeAfter - timeBefore));
		
		timeBefore = System.currentTimeMillis();
		for(int i = 0; i < 50_000; i++){
			linkedList.remove(random.nextInt(100000));
		}
		timeAfter = System.currentTimeMillis();
		
		System.out.println("linked list remove: " + (timeAfter - timeBefore));
		
		timeBefore = System.currentTimeMillis();
		for(int i = 0; i < 50_000; i++){
			arrayList.remove(random.nextInt(100000));
		}
		timeAfter = System.currentTimeMillis();
		
		System.out.println("Array list remove: " + (timeAfter - timeBefore));
		
		timeBefore = System.currentTimeMillis();
		for(int i = 0; i < 50_000; i++){
			linkedList.remove(0);
		}
		timeAfter = System.currentTimeMillis();
		
		System.out.println("linked list remove first: " + (timeAfter - timeBefore));
		
		timeBefore = System.currentTimeMillis();
		for(int i = 0; i < 50_000; i++){
			arrayList.remove(0);
		}
		timeAfter = System.currentTimeMillis();
		
		System.out.println("Array list remove first: " + (timeAfter - timeBefore));
	}
}
