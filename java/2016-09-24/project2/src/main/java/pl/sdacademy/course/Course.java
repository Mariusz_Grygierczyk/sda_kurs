package pl.sdacademy.course;

import java.util.List;

public class Course {
	private String name;
	private String duration;
	private List<Topic> topics;
	private List<Participant> participants;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public List<Topic> getTopics() {
		return topics;
	}
	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}
	public List<Participant> getParticipants() {
		return participants;
	}
	public void setParticipants(List<Participant> participants) {
		this.participants = participants;
	}
	public void addParticipant(Participant participant){
		participants.add(participant);
	}
	
	
	
}
