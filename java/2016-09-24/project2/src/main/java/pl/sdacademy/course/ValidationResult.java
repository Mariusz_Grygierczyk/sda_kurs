package pl.sdacademy.course;

import java.util.HashSet;
import java.util.Set;

import com.networknt.schema.ValidationMessage;

public class ValidationResult {
	private boolean valid;
	private Set<ValidationMessage> errors = new HashSet<ValidationMessage>();
	
	public boolean isValid() {
		return valid;
	}

	public Set<ValidationMessage> getErrors() {
		return errors;
	}

	public ValidationResult(boolean valid, Set<ValidationMessage> errors) {
		this.valid = valid;
		this.errors = new HashSet<ValidationMessage>(errors);
	}

	@Override
	public String toString() {
		return valid ? "json is valid" : "json has " + errors.size() + " errors";
	}
	
	
}
