package pl.sdacademy.course;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.ValidationMessage;

import java.io.IOException;
import java.util.Set;

public class Main {
	public static void main(String[] args) {
		ObjectMapper mapper = new ObjectMapper();		
		ClassLoader loader = Main.class.getClassLoader();
		Course course;

		try {
			course = mapper.readValue(loader.getResource("course.json"), Course.class);
			System.out.println(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(course));
		} catch (IOException e) {
			System.out.println("błąd przy przetwarzaniu pliku json");
			e.printStackTrace();
		}

		try {
			System.out.println(validate(getNodeFromFilename("course.json"), getSchemaFromFilename("course.schema")));
		} catch (IOException e) {
			System.out.println("Błąd przy wczytywaniu pliku");
		}
	}

	private static ValidationResult validate(JsonNode node, JsonSchema schema) {
		Set<ValidationMessage> errors;

		errors = schema.validate(node);
		return new ValidationResult(errors.isEmpty(), errors);

	}
	
	
	private static JsonNode getNodeFromFilename(String filename) throws IOException{
		ClassLoader loader = Main.class.getClassLoader();
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readTree(loader.getResource(filename));
	}
	

	private static JsonSchema getSchemaFromFilename(String filename){
		ClassLoader loader = Main.class.getClassLoader();		
		JsonSchemaFactory schemaFactory = new JsonSchemaFactory();
		return schemaFactory.getSchema(loader.getResource(filename));
	}
}
