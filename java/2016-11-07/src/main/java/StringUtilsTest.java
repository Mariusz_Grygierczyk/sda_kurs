import org.apache.commons.lang3.StringUtils;

public class StringUtilsTest {
    public static void main(String[] args){
        String string1 = "aaa";
        System.out.println(string1.contains("aaa"));
        System.out.println(StringUtils.contains(string1, "aaa"));
        System.out.println(StringUtils.contains(null, "aaa"));
        System.out.println(StringUtils.contains(string1, null));
    }
}
