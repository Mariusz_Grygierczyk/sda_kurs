package excercise4.part3;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;

public class WordCounter {
    public static void main(String[] args){
        HashMap<String, Integer> wordCounts = new HashMap<>();

        String input = "Guava is a set of core libraries that includes new collection types (such as multimap and multiset), immutable collections, a graph library, functional types, an in-memory cache, and APIs/utilities for concurrency, I/O, hashing, primitives, reflection, string processing, and much more!";
        String[] splitInput = input.split(" ");
        for (String word: splitInput){
            if (wordCounts.containsKey(word)) wordCounts.replace(word, wordCounts.get(word) + 1);
            else wordCounts.put(word, 1);
        }

        wordCounts.entrySet().forEach((entry) -> System.out.println(entry.getKey() + ": " + entry.getValue()));
    }
}