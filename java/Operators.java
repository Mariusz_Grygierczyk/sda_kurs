public class Operators{
	public static void main(String[] args){
		int a = 5;
		
		a += 6;
		System.out.println(a);
		a -= 7;
		System.out.println(a);
		a *= 8;
		System.out.println(a);
		
		int expression = 9 * 5 / 3 + 1;
		System.out.println(expression);
		
		System.out.println("modulo " + (10 % 4));
		
		int x = 0;
		
		System.out.println(x++);
		System.out.println(--x);
		
		boolean one = true;
		boolean other = false;
		
		// 0100
		int four = 4;
		// 0101
		int six = 6;
		
		int bitwiseResult = four & six;
		System.out.println("bitowy and " + bitwiseResult);
		
		boolean and = one & other;
		System.out.println("iloczyn " + and);
		boolean or = one | other;
		System.out.println("suma " + or);
		boolean xor = one ^ other;
		System.out.println("xor " + xor);
		System.out.println("negation " + !one);
		
		// && vs &
		if(other && one){
			System.out.println("raz");
		}
		
		String ternaryResult = one ? "true" : "false";
		System.out.println("ternary " + ternaryResult);
		
	}
	
	private static void neverPrint(){
		System.out.println("dwa");
	}

}