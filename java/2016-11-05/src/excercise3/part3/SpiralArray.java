package excercise3.part3;

public class SpiralArray {

    public void fillByLines(int[][] array){
        int count = 0;
        for (int i = 0; i < array.length; i++){
            for (int j = 0; j < array[i].length; j++){
                array[i][j] = count;
                count++;
            }
        }
    }

    public static void main(String[] args){
        int size = Integer.parseInt(System.getProperty("size"));
        SpiralArray arr = new SpiralArray();
        int[][] array = new int[size][size];
        arr.fillByLines(array);

        for (int i = 0; i < array.length; i++){
            for (int j = 0; j < array[i].length; j++){
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }

        
    }
}
