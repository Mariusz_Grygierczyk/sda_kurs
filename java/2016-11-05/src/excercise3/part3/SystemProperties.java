package excercise3.part3;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Properties;

public class SystemProperties {
    public static void main(String[] args) throws IOException, URISyntaxException {
        System.out.println("property:" + System.getProperty("ENV"));
        System.out.println("env:" + System.getenv("JAVA_HOME"));
        System.out.println("arg:" + args[0]);
        Properties properties = new Properties();
        ClassLoader loader = SystemProperties.class.getClassLoader();

        properties.load(new FileInputStream(new File(loader.getResource("config.properties").toURI())));
        properties.list(System.out);
    }
}
