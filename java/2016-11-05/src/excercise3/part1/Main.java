package excercise3.part1;



public class Main {
    public int recurentSum(int n){
        if (n == 1) return 1;
        return n + recurentSum(n - 1);
    }
}
