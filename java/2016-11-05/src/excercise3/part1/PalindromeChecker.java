package excercise3.part1;


public class PalindromeChecker {
    public static boolean isPalindrome(long number){
        String numberAsString = Long.toString(number);
        for (int i = 0; i < numberAsString.length() / 2; i++){
            if (numberAsString.charAt(i) != numberAsString.charAt(numberAsString.length() - i - 1)) return false;
        }
        return true;
    }

    public static void main(String[] args){
        System.out.println(isPalindrome(1121));
    }
}
