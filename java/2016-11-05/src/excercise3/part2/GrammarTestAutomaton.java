package excercise3.part2;


public class GrammarTestAutomaton {


    public static boolean s0(String input) {
        if (input.charAt(0) == 'a') return s1(input.substring(1));
        else return false;
    }

    public static boolean s1(String input) {
        if (input.charAt(0) == 'a') return sAcc(input.substring(1));
        else if (input.charAt(0) == 'b') return s2(input.substring(1));
        else return false;
    }

    public static boolean s2(String input) {
        if (input.charAt(0) == 'c') return s2(input.substring(1));
        else if (input.charAt(0) == 'd' || input.charAt(0) == 'e') return sAcc(input.substring(1));
        else return false;
    }

    public static boolean sAcc(String input) {
        if (input.isEmpty()) return true;
        else return false;
    }

    public static void main(String[] args){
        String input1 = "aaa";
        String input2 = "aa";
        String input3 = "abccd";
        String input4 = "abcdc";

        System.out.println(s0(input1));
        System.out.println(s0(input2));
        System.out.println(s0(input3));
        System.out.println(s0(input4));
    }

}
