/**
 * contains operations to calculate following operations
 * a # b = M + (a+b)*(2a-b)
 * a @ b = (a#b)^2  + sqrt(a^3)  / abs(b) + cos(b) *  - M
 * a $ b = M * (a#b) – M * (a@b)
 */
public class Calculator {
    /**
     * constant equal to 1.618033988749895
     */
    private static final double PHI = 1.618033988749895;

    /**
     * calculates  a # b = M + (a+b)*(2a-b)
     *
     * @param a value of a in equation
     * @param b value of b in equation
     * @param m value of c in equation
     * @return result of calculation
     */
    public static Double calculateHash(Integer a, Double b, short m){
        return m +(a + b) * (2 * a - b);
    }



    /**
     * calculates a @ b = (a#b)^2  + sqrt(a^3)  / abs(b) + cos(b) *  - M
     *
     * @param a value of a in equation
     * @param b value of b in equation
     * @param m value of c in equation
     * @return result of calculation
     */
    public static Double calculateAt(Integer a, Double b, short m){
        if (a <= 0) throw new IllegalArgumentException("a < 0 przy pierwiastku z a");
        if (b == 0) throw new IllegalArgumentException("b = 0 przy dzieleniu przez b");
        return (Math.pow(calculateHash(a, b, m), 2)) + Math.sqrt(a ^ 3) / Math.abs(b) * PHI - m;
    }

    /**
     * calculates a $ b = M * (a#b) – M * (a@b)
     *
     * @param a value of a in equation
     * @param b value of b in equation
     * @param m value of c in equation
     * @return result of calculation
     */
    public static Double calculateDollar(Integer a, Double b, short m){
       return m * (calculateHash(a, b, m)) - m * (calculateAt(a, b, m));
    }

    /**
     * calculates @ # and $ for command line arguments
     * first argument is a, second b, third argument m
     * third argument is optional if absent m = 1 is assumed
     */
    public static void main(String[] args){
        if (args.length < 2 || args.length > 3) {
            System.out.println("usage: calculator a b [m]");
            System.exit(1);
        }

        Integer a = Integer.parseInt(args[0]);
        Double b = Double.parseDouble(args[1]);
        Short m;
        if (args.length == 3) m = Short.parseShort(args[2]);
        else m = 1;

        System.out.println(a + " @ " + b + " = " + calculateAt(a, b, m));
        System.out.println(a + " # " + b + " = " + calculateHash(a, b, m));
        System.out.println(a + " $ " + b + " = " + calculateDollar(a, b, m));
    }
}

