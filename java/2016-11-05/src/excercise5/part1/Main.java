package excercise5.part1;


import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        System.out.println(Cat.class);
       Class catClass =  Class.forName("excercise5.part1.Cat");
        Animal animal =  (Animal) catClass.newInstance();
        animal.voice();
        for (Method m :catClass.getDeclaredMethods()
             ) {
            System.out.println(m);
        }
    }
}
