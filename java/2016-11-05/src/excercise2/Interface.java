package excercise2;


import java.util.Arrays;
import java.util.Scanner;

public class Interface {
    public static void main(String[] args){
        Logic logic = new Logic();
        Scanner input = new Scanner(System.in);

        System.out.println("input size of array");
        int size = input.nextInt();
        if(size <= 0) {
            System.out.println("array size must be > 0");
            return;
        }

        double[] array = logic.generateRandomArray(size);
        System.out.println("random array: ");
        logic.printArray(array);



        logic.modifyArray(array);
        System.out.println("modified array: ");
        logic.printArray(array);

        double sum = logic.calculateSum(array);
        System.out.printf("sum of values between -20 and 20: %.2f", sum);
    }
}
