package excercise2;


import java.util.Random;

public class Logic {
    public double[] generateRandomArray(int size) {
        double[] result = new double[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            result[i] = random.nextDouble() * 120 - 40;
        }
        return result;
    }

    public void modifyArray(double[] array) {
        for (int i = 3; i < array.length; i += 3) {
            array[i] = array[i] * (1 << i);
        }

        for (int i = 0; i < array.length; i += 10){
            array[i]++;
        }

        for (int i = 0; i < array.length; i += 3){
            array[i] /= -3;
        }
    }

    public double calculateSum(double[] array) {
        double result = 0;
        for (double value : array) {
            if (value < 20 && value > -20) result += value;
        }
        return result;
    }

    public void printArray(double[] array){
        for (double value: array) {
            System.out.printf("%.2f ", value);
        }
        System.out.println();
    }
}


