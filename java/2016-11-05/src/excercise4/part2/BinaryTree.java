package excercise4.part2;


import java.util.function.Consumer;

public class BinaryTree {
    private class Node {
        int value;
        Node leftChild;
        Node rightChild;

        private Node(int value) {
            this.value = value;
        }
    }

    private Node root;
    private int maxDepth;

    public BinaryTree(int maxDepth) {
        this.maxDepth = maxDepth;
    }

    public void insert(int value) {
        Node newNode = new Node(value);
        if (root == null) root = newNode;
        else {
            for (int i = 1; i <= maxDepth; i++) {
                boolean inserted = insertUnder(root, newNode, i);
                if (inserted) return;
            }
            throw new IllegalStateException("maximum depth exceeded");
        }
    }

    private boolean insertUnder(Node parentNode, Node newNode, int depth) {
        if (depth > 0) {
            if (parentNode.leftChild == null) {
                parentNode.leftChild = newNode;
                return true;
            } else if (parentNode.rightChild == null) {
                parentNode.rightChild = newNode;
                return true;
            } else {
                if (insertUnder(parentNode.leftChild, newNode, depth - 1)) return true;
                else if (insertUnder(parentNode.rightChild, newNode, depth - 1)) return true;
            }
        }
        return false;
    }

    public void traversePreOrder(Consumer<Integer> action) {
        traversePreOrder(action, root);
    }

    private void traversePreOrder(Consumer<Integer> action, Node currentNode) {
        if (currentNode == null) return;
        action.accept(currentNode.value);
        traversePreOrder(action, currentNode.leftChild);
        traversePreOrder(action, currentNode.rightChild);
    }

    public void traverseInOrder(Consumer<Integer> action) {
        traverseInOrder(action, root);
    }

    private void traverseInOrder(Consumer<Integer> action, Node currentNode) {
        if (currentNode == null) return;
        traverseInOrder(action, currentNode.leftChild);
        action.accept(currentNode.value);
        traverseInOrder(action, currentNode.rightChild);
    }

    public void traversePostOrder(Consumer<Integer> action) {
        traversePostOrder(action, root);
    }

    private void traversePostOrder(Consumer<Integer> action, Node currentNode) {
        if (currentNode == null) return;
        traversePostOrder(action, currentNode.leftChild);
        traversePostOrder(action, currentNode.rightChild);
        action.accept(currentNode.value);
    }



}
