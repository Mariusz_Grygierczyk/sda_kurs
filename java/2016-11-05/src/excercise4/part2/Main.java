package excercise4.part2;

public class Main {
    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree(3);
        for (int i = 1; i < 8; i++) {
            tree.insert(i);
        }
        System.out.println("pre order:");
        tree.traversePreOrder(System.out::println);
        System.out.println("in order:");
        tree.traverseInOrder(System.out::println);
        System.out.println("post order:");
        tree.traversePostOrder(System.out::println);
    }
}
