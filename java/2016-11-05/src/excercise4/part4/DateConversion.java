package excercise4.part4;

import java.text.DateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class DateConversion {
    public static void main(String[] args){
       ZonedDateTime now = ZonedDateTime.now();
        ZonedDateTime tomorrow = now.plusDays(1);
        System.out.println(now);
        System.out.println(tomorrow);
        System.out.println(now.compareTo(tomorrow));
        LocalDate dateFromString = LocalDate.parse("11.11.2016",DateTimeFormatter.ofPattern("dd.MM.uuuu"));
        System.out.println(dateFromString);


    }
}
