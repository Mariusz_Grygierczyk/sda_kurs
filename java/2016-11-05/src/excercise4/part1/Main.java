package excercise4.part1;

import java.text.Collator;
import java.util.Locale;

public class Main {
    public static void main(String[] args) {
        Person[] array = new Person[5];
        array[0] = new Person("Ąa", 5);
        array[1] = new Person("aA", 5);
        array[2] = new Person("a", 3);
        array[3] = new Person("b", 6);
        array[4] = new Person("b", 2);
        PeopleSorter sorter = new PeopleSorter();
        sorter.insertionSortComparable(array);
        for (Person person : array) {
            System.out.println(person.getName() + " " + person.getAge());
        }
//        for (Locale l : Locale.getAvailableLocales()) {
//            System.out.println(l);
//        }
        System.out.println(Locale.getAvailableLocales().length);
        System.out.println(Locale.getDefault());
        Locale polishLocale = new Locale("pl_PL");


    }
}
