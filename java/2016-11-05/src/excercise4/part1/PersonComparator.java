package excercise4.part1;

import java.util.Comparator;

public class PersonComparator implements Comparator<Person> {
    @Override
    public int compare(Person p1, Person p2) {
        int nameComparisionResult = p1.getName().compareTo(p2.getName());
        if (nameComparisionResult != 0) return nameComparisionResult;
        else return p2.getAge() - p1.getAge();
    }
}
