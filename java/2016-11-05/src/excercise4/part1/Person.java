package excercise4.part1;


import java.text.Collator;
import java.util.Locale;

public class Person implements Comparable<Person>{
    private String name;
    private int age;

    public Person(String name, int age){
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public int compareTo(Person other) {
        Collator collator = Collator.getInstance(new Locale("pl_PL"));
        int nameComparisionResult = collator.compare(this.name , other.name);
        if (nameComparisionResult != 0) return nameComparisionResult;
        else return other.age - this.age;
    }
}
