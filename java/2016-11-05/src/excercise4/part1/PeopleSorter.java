package excercise4.part1;


import java.util.Comparator;

public class PeopleSorter {
    public void insertionSortComparable(Person[] arrayToSort){
        for (int i = 1; i < arrayToSort.length; i++) {
            int j = i;
            while (j > 0 && arrayToSort[j-1].compareTo(arrayToSort[j]) > 0){
                swap(arrayToSort, j-1, j);
                j--;
            }
        }
    }

    public void insertionSortComparator(Person[] arrayToSort, Comparator<Person> comparator){
        for (int i = 1; i < arrayToSort.length; i++) {
            int j = i;
            while (j > 0 && comparator.compare(arrayToSort[j-1], arrayToSort[j]) > 0){
                swap(arrayToSort, j-1, j);
                j--;
            }
        }
    }

    private void swap(Person[] array, int firstIndex, int secondIndex){
        Person temp = array[firstIndex];
        array[firstIndex] = array[secondIndex];
        array[secondIndex] = temp;
    }
}
