package fileop;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CountLines {
	//zad 11
	public static void main(String[] args){
		int size;
		
		for(int i = 0; i < args.length; i++){
			
			try {
				size = Files.readAllLines(Paths.get(args[i])).size();
				System.out.println("plik: " + args[i] + " ma " + size + " linii");
				
			} catch (IOException e) {
				System.out.println("wyj�tek przy otczycie pliku: " + args[i]);
				e.printStackTrace();
			}
		}
		
	}
	
	
	
	public static int countLines(String path){
		
		try {
			return Files.readAllLines(Paths.get(path)).size();
		} catch (IOException e) {
			System.out.println(e.getClass().getName());
			e.printStackTrace();
		}
		return -1;
	}
}
