package fileop;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

public class CountLetters {
	//zad12
	public static void main(String[] args){
		if(args.length == 0){
			System.out.println("nie podano argumentu");
			return;
		}
		
		
		long timeBefore = System.currentTimeMillis();
		HashMap<Character, Integer> result = countLetters(args[0]);
		long timeAfter = System.currentTimeMillis();
		
		for(Entry<Character, Integer> e : result.entrySet()){
			System.out.println(e.getKey() + ": " + e.getValue());
		}
		System.out.println("czas: " + (timeAfter-timeBefore) + " ms");
	}
	
	public static HashMap<Character, Integer> countLetters(String path){
		File inFile = new File(path);
		HashMap<Character, Integer> result = new HashMap<Character, Integer>();
		char ch;
		
		try(FileReader reader = new FileReader(inFile);) {
			while(reader.ready()){
				ch = (char)reader.read();
				if(result.containsKey(ch)) result.put(ch, result.get(ch) + 1);
				else result.put(ch, 1);
			}		
			
		} catch  (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
}