package fileop;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileOperations {

	
public static void main(String[] args) throws IOException {
		
		replaceWithStar("data\\inputFile.txt", "data\\output6.txt");
		toUpperCase("data\\inputFile.txt", "data\\output7.txt");
		System.out.println(fileExists("data\\file1.txt"));
		createFile("data\\file2");
		createFile("data\\file1");
		copyWithStreams("data\\inputFile.txt", "data\\output3.txt");
		copyWithReader("data\\inputFile.txt", "data\\output4.txt");
		copyWithBufferedReader("data\\inputFile.txt", "data\\output5.txt");
		writeFizzBuzz("data\\output8.txt");
		System.out.println(countLines("data\\wordList.txt"));
		sortLines("data\\wordList.txt", "data\\wordListSorted.txt");
		
		
	}




//zad 10
public static void sortLines(String inputPath, String outputPath){
	List<String> input = new ArrayList<String>();
	
	try {
		input = Files.readAllLines(Paths.get(inputPath));
	} catch (IOException e) {
		System.out.println(e.getClass().getName());
		e.printStackTrace();
	}
	
	if(!input.isEmpty()){
		
		input.sort(null);
		
		File outFile = new File(outputPath);
		
		try(FileWriter writer = new FileWriter(outFile)){
			
			for(String s : input){
				writer.write(s + "\n");
			}
			
		} catch (IOException e) {
			System.out.println(e.getClass().getName());
			e.printStackTrace();
		}
		
	}
	
	
}

//zad 9
public static int countLines(String path){
	
	try {
		return Files.readAllLines(Paths.get(path)).size();
	} catch (IOException e) {
		System.out.println(e.getClass().getName());
		e.printStackTrace();
	}
	return -1;
}


//zad 8
public static void writeFizzBuzz(String path){
	File outFile = new File(path);
	
	try (FileWriter writer = new FileWriter(outFile)){
		for(int i = 1; i <= 50; i++){
			if (i % 5 == 0 && i % 3 == 0){
				writer.write("FizzBuzz ");
			} else {
				if (i % 5 == 0 ) writer.write("Buzz ");
				else if (i % 3 == 0 ) writer.write("Fizz ");
				else writer.write(i + " ");
			}
		}
		
		
	} catch (IOException e) {
		System.out.println(e.getClass().getName());
		e.printStackTrace();
	} 
	
	
}


	//zad 5
public static void copyWithBufferedReader(String inputPath, String outputPath) throws IOException{
	File inFile = new File(inputPath);
	File outFile = new File(outputPath);
	FileReader reader = null;
	FileWriter writer = null;
	BufferedReader bufferedReader = null;
	BufferedWriter bufferedWriter = null;
	String buf;
	try {
		reader = new FileReader(inFile);
		writer = new FileWriter(outFile);
		bufferedReader = new BufferedReader(reader);
		bufferedWriter = new BufferedWriter(writer);
		while(bufferedReader.ready()){
			buf = bufferedReader.readLine();
			bufferedWriter.write(buf);
			bufferedWriter.flush();
		}		
		
	} catch (IOException e) {
		System.out.println(e.getClass().getName());
		e.printStackTrace();
	} finally {
		if(reader != null) reader.close();
		if(writer != null) writer.close();
	}
	
	
	
}

	//zad 4 
	public static void copyWithReader(String inputPath, String outputPath) throws IOException{
		File inFile = new File(inputPath);
		File outFile = new File(outputPath);
		FileReader reader = null;
		FileWriter writer = null;
		int buffer;
		
		try {
			reader = new FileReader(inFile);
			writer = new FileWriter(outFile);
			while(reader.ready()){
				buffer = reader.read();				
				writer.write(buffer);
			}		
			
		}  finally {
			if(reader != null) reader.close();
			if(writer != null) writer.close();
		}
	}

	//zad 3 
	public static void copyWithStreams(String inputPath, String outputPath) throws IOException{
		File inFile = new File(inputPath);
		File outFile = new File(outputPath);
		FileInputStream inStream = null;
		FileOutputStream outStream = null;
			
		int buffer;
		try {
			inStream = new FileInputStream(inFile);
			outStream = new FileOutputStream(outFile);
			
			while(inStream.available() > 0){
				buffer =  inStream.read();
				outStream.write(buffer);
			}
			inStream.close();
			outStream.close();
		
		} catch (IOException e) {
			System.out.println(e.getClass().getName());
			e.printStackTrace();
		} finally {
			if(inStream != null) inStream.close();
			if(outStream != null) outStream.close();
		}
		
	}

	//zad 2
	public static void createFile(String path){
		File file = new File(path);
		try {
			if(file.createNewFile()) System.out.println("plik: " + file.getName() + " stworzony w: " + file.getAbsolutePath());
			else System.out.println("plik juz istnieje");
		} catch (IOException e) {
			System.out.println(e.getClass().getName());
			e.printStackTrace();
		}
	}

	//zad 1
	public static boolean fileExists(String path){
		File file = new File(path);
		try {
			file.createNewFile();
		} catch (IOException e) {
			System.out.println(e.getClass().getName());
			e.printStackTrace();
		}
		return file.exists();
	}


	//zad 6
	public static void replaceWithStar(String inputPath, String outputPath){
		char input;
		
		File inFile = new File(inputPath);
		File outFile = new File(outputPath);
		
		try {
			FileReader reader = new FileReader(inFile);
			FileWriter writer = new FileWriter(outFile);
			while(reader.ready()){
				input = (char) reader.read();
				if(input == ' ') input = '*';
				writer.write(input);
			}
			reader.close();
			writer.close();
			
		} catch ( IOException e) {
			System.out.println(e.getClass().getName());
			e.printStackTrace();
		}
		
	}
	
	//zad 7
	public static void toUpperCase(String inputPath, String outputPath){
		char input;
		
		File inFile = new File(inputPath);
		File outFile = new File(outputPath);
		
		try {
			FileReader reader = new FileReader(inFile);
			FileWriter writer = new FileWriter(outFile);
			while(reader.ready()){
				input = (char) reader.read();
				if(Character.isLowerCase(input)) input = Character.toUpperCase(input);
				writer.write(input);
			}
			reader.close();
			writer.close();
			
		} catch ( IOException e) {
			System.out.println(e.getClass().getName());
			e.printStackTrace();
		}
	
	}
	
}
