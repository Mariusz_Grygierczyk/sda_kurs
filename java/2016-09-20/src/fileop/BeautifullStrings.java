package fileop;

public class BeautifullStrings {
	public static void main(String[] args){
		boolean result;
		
		for(int i = 0; i <= 1000; i++){
			result = isBeautifull(i);
			if (result){
				System.out.println(i +" " + Integer.toBinaryString(i) + " " + result);
			} else {
				System.out.println(i +" " + Integer.toBinaryString(i) + " " + result + " " + replace(i));
			}
			
		}
	}
	
	public static boolean isBeautifull(int input){
		String binary = Integer.toBinaryString(input);
		
		for(int i = 0; i < binary.length() - 2; i++){
			if(binary.charAt(i) == '0' && binary.charAt(i+1) == '1' && binary.charAt(i+2) == '0') return false;
		}
		return true;
	}
	
	public static String replace(int input){
		String binary = Integer.toBinaryString(input);		
		char[] bytes = new char[binary.length()];
		
		
		
		for(int i = 0; i < bytes.length; i++) bytes[i] = binary.charAt(i);
		
		for(int i = 0; i < binary.length() - 2; i++){
			if(bytes[i] == '0' && bytes[i+1]  == '1' && bytes[i+2] == '0'){
				bytes[i] = '1';
				bytes[i+1] = '0';
				bytes[i+2] = '1';
				i++;
			}
		}
		return new String(bytes);
		
	}
}
