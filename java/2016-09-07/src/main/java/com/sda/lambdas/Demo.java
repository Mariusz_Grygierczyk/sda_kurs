package com.sda.lambdas;

public class Demo {

	public static void main(String[] args) {
		
		MathOperation<Integer, Integer> addition = (a, b) -> a + b;
		MathOperation<Integer, Integer> substraction = (a, b) -> a - b;
		MathOperation<Integer, Integer> division = (a, b) -> a / b;
		MathOperation<Integer, Integer> multiplication = (a, b) -> a * b;
		
		MathOperation<String, Integer> stringAddition = (a, b) -> a.length() + b.length();
		
		System.out.println(addition.operation(2, 3));
		System.out.println(substraction.operation(2, 3));
		System.out.println(division.operation(2, 3));
		System.out.println(multiplication.operation(2, 3));
	
		
	}

}
