package com.sda.lambdas;


public interface MathOperation <T, R>{

    R operation(T a, T b);
}
