package com.sda.people;

import java.util.HashSet;
import java.util.Set;

public class SetTest {
	String name;
	int age;
	
	public SetTest(String name, int age) {
		this.name = name;
		this.age = age;
	}
	
	@Override
	public String toString(){
		return "[ name: "+ name + " age: " + age + " hash: " + this.hashCode() +" ]";
	}
	
	@Override 
	public boolean equals(Object o){
		if (!(o instanceof SetTest)) return false;
		else {
			SetTest other = (SetTest)o;
			if((name == other.name) && (age == other.age)) return true;
			else return false;
		}
	}
	
	@Override
	public int hashCode(){
		return name.hashCode() + age;
	}
	
	
	public static void main(String[] args){
		Set<SetTest> set = new HashSet<SetTest>();
		
		SetTest obj1 = new SetTest("aaa",2);
		System.out.println(obj1.equals(new SetTest("aaa",2)));
		
		set.add(new SetTest("aaa",2));
		set.add(new SetTest("aaa",2));
		
		set.forEach(System.out::println);		
		
	}
}
