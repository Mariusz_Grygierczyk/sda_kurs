package com.sda.people;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class SoftwareDeveloper {
	String name;
	int age;
	Set<String> knownLanguages;
	
	public SoftwareDeveloper(String name, int age, Set<String> knownLanguages) {
		this.name = name;
		this.age = age;
		this.knownLanguages = knownLanguages;
	}
	
	public static void main(String[] args){
		List<SoftwareDeveloper> developers = new ArrayList<SoftwareDeveloper>();
		
//		a)Andrzej,37 lat, zna: Java,C#,Java script,c++
		developers.add(new SoftwareDeveloper("Andrzej", 37, new HashSet<String>(Arrays.asList("Java", "C#", "Java script", "C++"))));		
//		b)Tomek,24 lat, zna: Scala,Haskell,html
		developers.add(new SoftwareDeveloper("Tomek", 24, new HashSet<String>(Arrays.asList("Scala", "Haskell", "html"))));
//		c)Bartek,29 lat, zna: Groovy,Css,Java sript,html
		developers.add(new SoftwareDeveloper("Bartek", 29, new HashSet<String>(Arrays.asList("Groovy", "Css", "Java script", "html"))));
//		d)Dorota,33 lat, zna: Java,sql
		developers.add(new SoftwareDeveloper("Dorota", 33, new HashSet<String>(Arrays.asList("Java", "sql"))));
//		e)Patryk, 38 lat, zna: Java script,html,css, jquery
		developers.add(new SoftwareDeveloper("Patryk", 38, new HashSet<String>(Arrays.asList("Java script", "html", "css", "jquery"))));
//		f)Iwona, 28 lat, zna: Java script, css, html
		developers.add(new SoftwareDeveloper("Iwona", 28, new HashSet<String>(Arrays.asList("Java script", "html", "css"))));
		
		List<List<String>> allLanguages = new ArrayList<List<String>>();
		for(SoftwareDeveloper d : developers){
			allLanguages.add(new ArrayList<String>(d.knownLanguages));
		}
		Set<String> allLanguagesNoReps = allLanguages.stream().flatMap(List::stream).collect(Collectors.toSet());
		allLanguagesNoReps.forEach(System.out::println);
		
		System.out.println();
		allLanguagesNoReps = allLanguagesNoReps.stream().filter(s -> s.contains("c")).collect(Collectors.toSet());
		for(String l : allLanguagesNoReps){
			System.out.print(l + " # ");
		}
		
		System.out.println();
		List<SoftwareDeveloper> devsKnowingAtLeast4Languages = developers.stream().filter(d -> d.knownLanguages.size() >= 4).collect(Collectors.toList());
		for(SoftwareDeveloper d : devsKnowingAtLeast4Languages){
			System.out.print(d.name + " # ");
		}
		System.out.println();
		
		System.out.println(developers.stream().mapToInt(d -> d.age).average());
		
		
	}
}
