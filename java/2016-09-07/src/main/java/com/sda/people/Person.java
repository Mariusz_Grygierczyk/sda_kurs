package com.sda.people;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Person {
	private String fName;
	private String lName;
	private int age;
	
	public String getFName() {
		return fName;
	}
	public String getLName() {
		return lName;
	}	
	public int getAge() {
		return age;
	}


	public Person(String fName, String lName, int age) {
		this.fName = fName;
		this.lName = lName;
		this.age = age;
	}
	
	public static void main(String[] args){
		List<Person> people = new ArrayList<Person>();
		
		people.add(new Person("aa", "bb", 12));
		people.add(new Person("jan", "nowak", 22));
		people.add(new Person("maria", "nowak", 13));
		people.add(new Person("jan", "kowalski", 44));
		
		Comparator<Person> compareAge = (p1, p2) -> p1.getAge() - p2.getAge();
		
		Collections.sort(people, compareAge);
		
		for(Person p : people){
			System.out.println(p.getFName() + " " + p.getLName());
		}
		
		Collections.sort(people, Collections.reverseOrder(compareAge));
		for(Person p : people){
			System.out.println(p.getFName() + " " + p.getLName());
		}
	}
	
	
}
