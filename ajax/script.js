(function () {
    const menuPosts = $(".menu-posts");
    const postsTab = $(".posts-tab");
    const postsContainer = $(".posts-container");
    const showAllButton = $(".show-all");
    const showFiveButton =$(".show-first-five");
    const showEvenButton = $(".show-even");

    const menuEdit = $(".menu-edit");
    const editTab = $(".edit-tab");
    const editIdInput = $(".edit-tab .message-id");
    const editTitleInput = $(".edit-tab .message-title");
    const editBodyInput = $(".edit-tab .message-body");
    const editSaveButton = $(".edit-tab .save-button");

    const menuCreate = $(".menu-create");
    const createTab = $(".create-tab");
    const createTitleInput = $(".create-tab .message-title");
    const createBodyInput = $(".create-tab .message-body");
    const createSaveButton = $(".create-tab .save-button");

    const root = "http://jsonplaceholder.typicode.com";


    const makeDivFromPost = function(post){
        return `<div> id: ${post.id}<br>  Title: ${post.title}<br>  Body:${post.body}</div>`;
    };
    const makeStringFromPost = function(post){
      return  `id: ${post.id}\n  Title: ${post.title}\n  Body:${post.body}`;
    };

    const showAllPosts = function(){
        postsContainer.empty();
        $.ajax({
            url:root + "/posts",
            method: "GET"
        }).done((response) => {
           response.forEach((post) => {
               //postsContainer.append(`<div> id: ${post.id}<br>  Title: ${post.title}<br>  Body:${post.body}`);
               postsContainer.append(makeDivFromPost(post));
           })
        }).fail(function () {
            alert("error");
        })
    };
    showAllButton.click(showAllPosts);

    const showFivePosts = function(){
        postsContainer.empty();
        $.ajax({
            url:root + "/posts",
            method: "GET"
        }).done((response) =>  {
           for(let i = 0; i <= 4; i++) {
               //postsContainer.append(`<div> id: ${response[i].id}<br>  Title: ${response[i].title}<br>  Body:${response[i].body}`);
               postsContainer.append(makeDivFromPost(response[i]));
            }
        }).fail(function () {
            alert("error");
        })
    };
    showFiveButton.click(showFivePosts);

    const showEvenPosts = function(){
        postsContainer.empty();
        $.ajax({
            url:root + "/posts",
            method: "GET"
        }).done((response) => {
            for(let i = 1; i < response.length; i += 2) {
                //postsContainer.append(`<div> id: ${response[i].id}<br>  Title: ${response[i].title}<br>  Body:${response[i].body}`);
                postsContainer.append(makeDivFromPost(response[i]));
            }
        }).fail(function () {
            alert("error");
        })
    };
    showEvenButton.click(showEvenPosts);

    const editPost = function(){
        $.ajax({
            url:root + "/posts/" + editIdInput.val(),
            method: "PUT",
            data: {
                id: editIdInput.val(),
                title: editTitleInput.val(),
                body: editBodyInput.val()
            }
        }).done(function(response) {
            //alert(`edit successful:\n id: ${response.id}\n  Title: ${response.title}\n  Body:${response.body}`);
            alert(`edit successful:\n ${makeStringFromPost(response)}`);
        }).fail(function () {
            alert("error");
        })
    };
    editSaveButton.click(editPost);

    const addPost = function(){
        $.ajax({
            url:root + "/posts",
            method: "POST",
            data: {
                title: createTitleInput.val(),
                body: createBodyInput.val()
            }
        }).done(function(response) {
            //alert(`post created:\n id: ${response.id}\n  Title: ${response.title}\n  Body:${response.body}`);
            alert(`post created:\n ${makeStringFromPost(response)}`)
        }).fail(function () {
            alert("error");
        })
    };
    createSaveButton.click(addPost);


    const hideAll = function () {
        postsTab.hide();
        editTab.hide();
        createTab.hide();
    };
    menuPosts.click(function(){
        hideAll();
        showAllPosts();
        postsTab.show();
    });
    menuEdit.click(function(){
        hideAll();
        editTab.show();
    });
    menuCreate.click(function(){
        hideAll();
        createTab.show();
    });

})();